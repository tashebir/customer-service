<%-- 
    Document   : editProject
    Created on : May 11, 2011, 4:38:41 PM
    Author     : endashaw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="javax.portlet.*"%>
<%@page import="com.example.service.service.customerprojectLocalServiceUtil" %>
<%@page import="com.example.service.service.*" %>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="javax.portlet.*"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.util.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 <script>
     function displaycancel()
     {
         
         document.getElementById('canc').value='canc';
         document.forms['editform'].submit();
     }
      function checkUpdate()
    {

                var h= confirm("Are U Sure U want To Update?");
                if(h==true)
               {
                       document.getElementById('supdate').value='supdate';
                       document.forms['editform'].submit();
               }
   }

 </script>
<%
if(request.getAttribute("msg")!=null)
 {
        out.println(request.getAttribute("msg"));

}
 if(request.getAttribute("mg")!=null)
    {
        out.println("<p style=\"color:red;padding-left:50px;\">"+request.getAttribute("mg")+"</p>");
       }
%>

<portlet:defineObjects />
<%PortletPreferences prefs = renderRequest.getPreferences();%>

 <link href="/INSAOnlineCustomerService/css/styles.css" type="text/css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="/INSAproject/css/sur.css"/>

 <div id="holder">
    <input type="hidden" id="val" name="vval">
    <div id="head" align="center" >Header</div>
     <div id="left">
        <form name="linkform" method="post" action="<portlet:actionURL/>">
         <%
                     ThemeDisplay xxxxx = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);

                                        String uid= Long.toString(xxxxx.getUserId());
                    /*out.println("userId" + uid);*/
                    String CustServiceUser="CustomerService";
                    String ProManager="ProjectManager";
                    //String NormalCustomer="Customer";
                    String jobbTitle="";


              %>
        <table>
            <tbody style="border:1px"><tr>
                    <td>
                        <div id="listview">
                           <ul>
                               <%


                               for(int z=0;z<user_LocalServiceUtil.getuser_sCount();z++)
                                   {
                                        if(uid.equalsIgnoreCase(Long.toString(user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(z).getUserId())))
                                        {
                                            jobbTitle= user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(z).getJobTitle();
                                         }
                                     }

                                if(jobbTitle.equalsIgnoreCase(CustServiceUser))
                                {

                                    for (int i = 0; i < customerprojectLocalServiceUtil.getcustomerprojectsCount(); i++) {


                                             %>
                                             <li class="LHSViewHeader"><a  href="#"  id="<%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectId()%>" onclick="javascript:document.getElementById('pid').value=this.id;document.forms['linkform'].submit()"><b><%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectName()%></b></a></li>
                                                 <input type="hidden" id="pid" name="resh">
                                 <%}}
                                  else if(jobbTitle.equalsIgnoreCase(ProManager))
                                  {
                                      for (int i = 0; i < customerprojectLocalServiceUtil.getcustomerprojectsCount(); i++) {
                                              if(uid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectManagerId())){

                                             %>
                                              <li class="LHSViewHeader"><a  href="#"  id="<%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectId()%>" onclick="javascript:document.getElementById('pid').value=this.id;document.forms['linkform'].submit()"><b><%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectName()%></b></a></li>
                                                   <input type="hidden" id="pid" name="resh">
                                  <%}}}
                                  else
                                      {
                                    for (int i = 0; i < customerprojectLocalServiceUtil.getcustomerprojectsCount(); i++) {
                                              if(uid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getUserIId())){

                                             %>
                                              <li class="LHSViewHeader"><a  href="#"  id="<%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectId()%>" onclick="javascript:document.getElementById('pid').value=this.id;document.forms['linkform'].submit()"><b><%=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectName()%></b></a></li>
                                  <%}}}%>
                                 <input type="hidden" id="pid" name="resh">

                            </ul>
                     </div>
                    </td>

                </tr>


        </tbody></table>
</form>

</div>
  <div id="center">
        <form name="editform" method="post" action="<portlet:actionURL/>" >

            <FIELDSET>
                <LEGEND>Project Information</LEGEND>
                      <table>
                               <tr><td id="subtd2"style="width:100px;">Project Manager</td><td><input type="text" name="projMng" value="<%=request.getAttribute("proman") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Project Id</td><td><input type="text" name="projId" value="<%=request.getAttribute("proid") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Project Name</td><td><input type="text" name="projName" value="<%=request.getAttribute("proname") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Start Date</td><td><input type="text" name="projSd" value="<%=request.getAttribute("prosd") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">End Date</td><td><input type="text" name="projEd" value="<%=request.getAttribute("proed") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;"> Responsible Project Follower</td><td><input type="text" name="projUserid" value="<%=request.getAttribute("profollower") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;"> ID </td><td><input type="text" name="pid" value="<%=request.getAttribute("pid") %>"></td></tr>
                       </table>
           </FIELDSET>
         <fieldset>
              <legend>Sub Project Information</legend>
               <table>
                               <tr><td id="subtd2"style="width:100px;">Sub Id</td><td><input type="text" name="suid" value="<%=request.getAttribute("suid") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Sub Project Id</td><td><input type="text" name="subproject_id" value="<%=request.getAttribute("subproid") %>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Sub Project Name</td><td><input type="text" name="subproname" value="<%=request.getAttribute("subproname")%>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Sub Project Start Date</td><td><input type="text" name="subsd" value="<%=request.getAttribute("subsd")%>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">Sub Project End Date</td><td><input type="text" name="subed" value="<%=request.getAttribute("subed")%>"></td></tr>
                               <tr><td id="subtd2"style="width:100px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
                  
              </table>
              <table>
                   <%
                   if(String.valueOf(request.getAttribute("updatedisplay")).equalsIgnoreCase("displayUpdate"))
                                 {
                   %>
                                            <tr><td id="subtd2"style="width:100px;"><input type="submit" name="subupdate" value="Update" onclick=" document.getElementById('disupdate').value='disupdate';document.forms['editform'].submit();"></td><td><input type="submit" name="subcancel" value="Cancel" onclick="displaycancel();" ></td></tr>
                                             <input type="hidden" id="disupdate" name="disupdate">
                                             <input type="hidden" id="discanc" name="discanc">

                   <%
                                 }
                                 else
                                 {
                   %>
                                             <tr><td id="subtd2"style="width:100px;"><input type="submit" name="subupdate" value="UpdateW" onclick="checkUpdate();"></td><td><input type="submit" name="subcancel" value="Cancel" onclick="displaycancel();" ></td></tr>
                                             <input type="hidden" id="supdate" name="supdate">
                                             <input type="hidden" id="canc" name="canc">
                                  <%}%>
              </table>
         </fieldset>
      </form>
     </div>
    

 </div>

   