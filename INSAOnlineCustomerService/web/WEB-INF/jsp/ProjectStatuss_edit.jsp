<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%-- Uncomment below lines to add portlet taglibs to jsp--%>
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="com.example.service.service.*"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="com.liferay.portal.kernel.util.*"%>
<%@ page import="com.liferay.portal.kernel.dao.search.*"%>
<%@ page import="java.util.List, java.util.ArrayList,java.sql.SQLException,javax.swing.JOptionPane;" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="ui"%>
<portlet:defineObjects />
<%PortletPreferences prefs = renderRequest.getPreferences();%> 

<script>
    function checkextension(fileid)
    {
        document.getElementById('upf').value=fileid.value.split('.')[1];
        var ext=document.getElementById('upf').value;
        if(ext!='doc'&& ext!='docx'&& ext!='pdf'&& ext!='odt'&& ext!='rtf'&& ext!='txt'&& ext!='sxw')
        {
            alert("U r Only Allowed To Enter Files With PDF, DOC, DOCX Files");
          
            document.getElementById('subbmit').style.visibility='hidden';
       }
       else
       {
            document.getElementById('subbmit').style.visibility='visible';
       }
    }
</script>
<%
if(request.getAttribute("msg")!=null)
 {
     %>
       <div class="portlet-msg-success">  <% out.println(request.getAttribute("msg"));%>
       </div>
   <%
}
 if(request.getAttribute("mg")!=null)
    {
        out.println("<p style=\"color:red;padding-left:50px;\">"+request.getAttribute("mg")+"</p>");
       }
%>
<link href="/INSAOnlineCustomerService/css/styles.css" type="text/css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="/INSAproject/css/sur.css"/>
<div id="holder">
    <div id="head">Header</div>
    <div id="left">
        Project Status
    </div>
    <div id="center">

<form  name="prostatusform" method="post" action="<portlet:actionURL/>" enctype="multipart/form-data">
 <%
                     ThemeDisplay xxxxx = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
                     String uid= Long.toString(xxxxx.getUserId());
                    
                   
 %>

        <FIELDSET >
            <LEGEND>Project Information</LEGEND>
                   <table id="tabless">
                          <tr><td id="subtd">Project Name </td><td>
                              <select name="selectedproname" onchange="document.getElementById('proid').value=this.options[selectedIndex].id;document.forms['prostatusform'].submit()">
                                <option>---- Select ----</option>

                                        <%
                                            for(int z=0;z<customerprojectLocalServiceUtil.getcustomerprojectsCount();z++)
                                            {
                                                    if(uid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectManagerId()))
                                                    {
                                                        out.println("<option id=\""+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId()+"\""+">"+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName() + "</option>");
                                                    }
                                            }

                                        if(request.getAttribute("projectName")!=null){
                                        %>
                                        
<option selected><%= request.getAttribute("projectName") %></option> <% } %>

                                </select> <input type="hidden" id="proid" name="projectIId"</td>
                          </tr>
                          <tr><td id="subtd">Sub Project Name </td><td>        
                             <select name="selectedat" onchange="javascript:document.getElementById('subproid').value=this.options[selectedIndex].id">
                                  <option selected>---- Select ----</option>
                                        <%
                                               
                                          
                                           //selectedpro=xx.toString();                  

                                            /*    out.println(request.getAttribute("proidreturn"));
                                             for(int z=0;z<customerprojectLocalServiceUtil.getcustomerprojectsCount();z++)
                                            {
                                                    if(uid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectManagerId()))
                                                    {
                                                            ProjectIId=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId();
                                                          */
                                        if(request.getAttribute("projectNa")!=null){
                                                            for(int x=0;x<subprojectLocalServiceUtil.getsubprojectsCount();x++)
                                                            {
                                                               if(request.getAttribute("projectNa").toString().equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(x).getProjectId()))
                                                                {
                                                                    out.println("<option id=\""+ subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(x).getSubproject_id()+"\""+">"+ subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(x).getSubproject_name() + "</option>");


                                                                                                                                       }
                                                            }

                                                            }
                     else{out.println("<option> No value</option>");}
                                                  // }
                                          // }
                                      %>
                              </select><b style="color:red">&nbsp;&nbsp;&nbsp;*</b> <input type="hidden" id="subproid" name="projectId"></td>
                              </tr>

                        <tr><td id="subtd">Activity Type </td><td><select name="selectedat" onchange="javascript:document.getElementById('patid').value=this.options[selectedIndex].id">
                                     <option selected>---- Select ----</option>
                                         <%
                                             for(int f=0; f<projectactivityLocalServiceUtil.getprojectactivitiesCount();f++)
                                             {                                                                                         
                                                      out.println("<option id=\""+ projectactivityLocalServiceUtil.getprojectactivities(0, projectactivityLocalServiceUtil.getprojectactivitiesCount()).get(f).getId()+"\""+">"+ projectactivityLocalServiceUtil.getprojectactivities(0, projectactivityLocalServiceUtil.getprojectactivitiesCount()).get(f).getActivity() + "</option>");
                                              } %>
                                       
                                    </select><b style="color:red">&nbsp;&nbsp;&nbsp;*</b><input type="hidden" name="activitytype" id="patid"></td></tr>
                             <tr><td id="subtd">Percentage Completed</td><td><select name="selectedperco"onchange="javascript:document.getElementById('perccomp').value=this.options[selectedIndex].id">
                                       <option selected>---- Select ----</option>
                                       <option >25%</option>
                                       <option >50%</option>
                                       <option >75%</option>
                                       <option >100%</option>
                                 </select><b style="color:red">&nbsp;&nbsp;&nbsp;*</b><input  type="hidden" id="perccomp" name="percentcompleted"></td></tr>
                                                       
                             <tr><td>Upload File</td><td><input type="file" name="uplfile" onchange="javascript:checkextension(this);"></td></tr>
                             <input type="hidden" name="upf" id="upf">
                    </table>
            </FIELDSET>

        <table>

            <tr><td>&nbsp;</td></tr>
            <tr>
                <td id="subtd"> <input type="reset" name="reset" value="reset"></td>
                <td id="subtd"> <input type="submit" name="subbmit" value="submit" id="subbmit" onclick="document.getElementById('ps').value='ps'"></td>
                <input type="hidden" name="ps" id="ps">

            </tr>
        </table>


    </form>
    </div>
   
</div>
