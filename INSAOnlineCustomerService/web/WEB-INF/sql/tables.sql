
create table complainDetail (
	codetid INTEGER not null primary key,
	compid VARCHAR(75) null,
	complaintype VARCHAR(75) null,
	remark VARCHAR(75) null
);

create table complainReport (
	id INTEGER not null primary key,
	complainType VARCHAR(75) null,
	complainId VARCHAR(75) null,
	remark VARCHAR(75) null
);

create table complainSolution (
	solutionId INTEGER not null primary key,
	solutionSubject VARCHAR(75) null,
	solutionDetail VARCHAR(75) null,
	solutionDate VARCHAR(75) null,
	complainId VARCHAR(75) null,
	subprojectId VARCHAR(75) null
);

create table customer (
	id INTEGER not null primary key,
	organName VARCHAR(75) null,
	sector VARCHAR(75) null,
	email VARCHAR(75) null,
	fixedtele VARCHAR(75) null,
	mobiletele VARCHAR(75) null,
	fax VARCHAR(75) null,
	region VARCHAR(75) null,
	zone VARCHAR(75) null,
	woreda VARCHAR(75) null,
	kebele VARCHAR(75) null,
	houseno VARCHAR(75) null,
	userIId VARCHAR(75) null
);

create table customercomplain (
	id INTEGER not null primary key,
	complainSubject VARCHAR(75) null,
	complainDetail VARCHAR(75) null,
	subprojectId VARCHAR(75) null,
	status VARCHAR(75) null,
	complainDate VARCHAR(75) null,
	lastanswerDate VARCHAR(75) null,
	assignedTo VARCHAR(75) null,
	reIdOf INTEGER
);

create table customerfeedback (
	id INTEGER not null primary key,
	orgname VARCHAR(75) null,
	customerName VARCHAR(75) null,
	email VARCHAR(75) null,
	urfeedback VARCHAR(75) null,
	projectid VARCHAR(75) null
);

create table customerproject (
	id INTEGER not null primary key,
	projectId VARCHAR(75) null,
	projectName VARCHAR(75) null,
	startDate VARCHAR(75) null,
	endDate VARCHAR(75) null,
	userIId VARCHAR(75) null,
	projectManagerId VARCHAR(75) null,
	orgid INTEGER
);

create table departmentDetail (
	id INTEGER not null primary key,
	title VARCHAR(75) null,
	dname VARCHAR(75) null,
	description VARCHAR(75) null,
	imagepath VARCHAR(75) null
);

create table detailinfo (
	id INTEGER not null primary key,
	title VARCHAR(75) null,
	dname VARCHAR(75) null,
	description VARCHAR(75) null,
	imagepath VARCHAR(75) null
);

create table indepartment (
	id INTEGER not null primary key,
	title VARCHAR(75) null,
	descrptionn VARCHAR(75) null,
	storedimage VARCHAR(75) null
);

create table projectActivity (
	id INTEGER not null primary key,
	activity VARCHAR(75) null,
	description VARCHAR(75) null
);

create table projectactivity (
	id INTEGER not null primary key,
	activity VARCHAR(75) null,
	description VARCHAR(75) null
);

create table projectstatus (
	id INTEGER not null primary key,
	subprojectId VARCHAR(75) null,
	activityType VARCHAR(75) null,
	percentageCompleted VARCHAR(75) null,
	remaining VARCHAR(75) null,
	statusfilldate VARCHAR(75) null,
	uploaddd VARCHAR(75) null
);

create table subproject (
	id INTEGER not null primary key,
	subproject_id VARCHAR(75) null,
	subproject_name VARCHAR(75) null,
	start_date VARCHAR(75) null,
	end_date VARCHAR(75) null,
	duration VARCHAR(75) null,
	description VARCHAR(75) null,
	projectId VARCHAR(75) null
);

create table subprojectactivityplan (
	id INTEGER not null primary key,
	subprojectId VARCHAR(75) null,
	activityType VARCHAR(75) null,
	start_date VARCHAR(75) null,
	end_date VARCHAR(75) null,
	duration VARCHAR(75) null,
	description VARCHAR(75) null,
	more VARCHAR(75) null
);

create table user_ (
	userId INTEGER not null primary key,
	screenName VARCHAR(75) null,
	emailAddress VARCHAR(75) null,
	jobTitle VARCHAR(75) null,
	uuid_ VARCHAR(75) null
);
