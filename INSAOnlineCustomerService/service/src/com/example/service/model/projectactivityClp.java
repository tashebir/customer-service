package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class projectactivityClp extends BaseModelImpl<projectactivity>
    implements projectactivity {
    private int _id;
    private String _activity;
    private String _description;

    public projectactivityClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getActivity() {
        return _activity;
    }

    public void setActivity(String activity) {
        _activity = activity;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public projectactivity toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            projectactivity model = new projectactivityClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setActivity(HtmlUtil.escape(getActivity()));
            model.setDescription(HtmlUtil.escape(getDescription()));

            model = (projectactivity) Proxy.newProxyInstance(projectactivity.class.getClassLoader(),
                    new Class[] { projectactivity.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        projectactivityClp clone = new projectactivityClp();

        clone.setId(getId());
        clone.setActivity(getActivity());
        clone.setDescription(getDescription());

        return clone;
    }

    public int compareTo(projectactivity projectactivity) {
        int pk = projectactivity.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        projectactivityClp projectactivity = null;

        try {
            projectactivity = (projectactivityClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = projectactivity.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", activity=");
        sb.append(getActivity());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.projectactivity");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activity</column-name><column-value><![CDATA[");
        sb.append(getActivity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
