package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="complainReportModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>complainReport</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.complainReport
 * @see com.example.service.model.impl.complainReportImpl
 * @see com.example.service.model.impl.complainReportModelImpl
 *
 */
public interface complainReportModel extends BaseModel<complainReport> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getComplainType();

    public void setComplainType(String complainType);

    public String getComplainId();

    public void setComplainId(String complainId);

    public String getRemark();

    public void setRemark(String remark);

    public complainReport toEscapedModel();
}
