package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class complainReportClp extends BaseModelImpl<complainReport>
    implements complainReport {
    private int _id;
    private String _complainType;
    private String _complainId;
    private String _remark;

    public complainReportClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainType() {
        return _complainType;
    }

    public void setComplainType(String complainType) {
        _complainType = complainType;
    }

    public String getComplainId() {
        return _complainId;
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public complainReport toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            complainReport model = new complainReportClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setComplainType(HtmlUtil.escape(getComplainType()));
            model.setComplainId(HtmlUtil.escape(getComplainId()));
            model.setRemark(HtmlUtil.escape(getRemark()));

            model = (complainReport) Proxy.newProxyInstance(complainReport.class.getClassLoader(),
                    new Class[] { complainReport.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        complainReportClp clone = new complainReportClp();

        clone.setId(getId());
        clone.setComplainType(getComplainType());
        clone.setComplainId(getComplainId());
        clone.setRemark(getRemark());

        return clone;
    }

    public int compareTo(complainReport complainReport) {
        int pk = complainReport.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        complainReportClp complainReport = null;

        try {
            complainReport = (complainReportClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = complainReport.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", complainType=");
        sb.append(getComplainType());
        sb.append(", complainId=");
        sb.append(getComplainId());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.complainReport");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainType</column-name><column-value><![CDATA[");
        sb.append(getComplainType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainId</column-name><column-value><![CDATA[");
        sb.append(getComplainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
