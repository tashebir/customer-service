package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="detailinfoModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>detailinfo</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.detailinfo
 * @see com.example.service.model.impl.detailinfoImpl
 * @see com.example.service.model.impl.detailinfoModelImpl
 *
 */
public interface detailinfoModel extends BaseModel<detailinfo> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getTitle();

    public void setTitle(String title);

    public String getDname();

    public void setDname(String dname);

    public String getDescirption();

    public void setDescirption(String descirption);

    public String getImagepath();

    public void setImagepath(String imagepath);

    public detailinfo toEscapedModel();
}
