package com.example.service.model;


/**
 * <a href="projectactivity.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>projectactivity</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.projectactivityImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectactivityModel
 * @see com.example.service.model.impl.projectactivityImpl
 * @see com.example.service.model.impl.projectactivityModelImpl
 *
 */
public interface projectactivity extends projectactivityModel {
}
