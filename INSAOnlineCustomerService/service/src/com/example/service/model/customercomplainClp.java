package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class customercomplainClp extends BaseModelImpl<customercomplain>
    implements customercomplain {
    private int _id;
    private String _complainSubject;
    private String _complainDetail;
    private String _subprojectId;
    private String _status;
    private String _complainDate;
    private String _lastanswerDate;
    private String _assignedTo;
    private int _reIdOf;

    public customercomplainClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainSubject() {
        return _complainSubject;
    }

    public void setComplainSubject(String complainSubject) {
        _complainSubject = complainSubject;
    }

    public String getComplainDetail() {
        return _complainDetail;
    }

    public void setComplainDetail(String complainDetail) {
        _complainDetail = complainDetail;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getStatus() {
        return _status;
    }

    public void setStatus(String status) {
        _status = status;
    }

    public String getComplainDate() {
        return _complainDate;
    }

    public void setComplainDate(String complainDate) {
        _complainDate = complainDate;
    }

    public String getLastanswerDate() {
        return _lastanswerDate;
    }

    public void setLastanswerDate(String lastanswerDate) {
        _lastanswerDate = lastanswerDate;
    }

    public String getAssignedTo() {
        return _assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        _assignedTo = assignedTo;
    }

    public int getReIdOf() {
        return _reIdOf;
    }

    public void setReIdOf(int reIdOf) {
        _reIdOf = reIdOf;
    }

    public customercomplain toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            customercomplain model = new customercomplainClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setComplainSubject(HtmlUtil.escape(getComplainSubject()));
            model.setComplainDetail(HtmlUtil.escape(getComplainDetail()));
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setStatus(HtmlUtil.escape(getStatus()));
            model.setComplainDate(HtmlUtil.escape(getComplainDate()));
            model.setLastanswerDate(HtmlUtil.escape(getLastanswerDate()));
            model.setAssignedTo(HtmlUtil.escape(getAssignedTo()));
            model.setReIdOf(getReIdOf());

            model = (customercomplain) Proxy.newProxyInstance(customercomplain.class.getClassLoader(),
                    new Class[] { customercomplain.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customercomplainClp clone = new customercomplainClp();

        clone.setId(getId());
        clone.setComplainSubject(getComplainSubject());
        clone.setComplainDetail(getComplainDetail());
        clone.setSubprojectId(getSubprojectId());
        clone.setStatus(getStatus());
        clone.setComplainDate(getComplainDate());
        clone.setLastanswerDate(getLastanswerDate());
        clone.setAssignedTo(getAssignedTo());
        clone.setReIdOf(getReIdOf());

        return clone;
    }

    public int compareTo(customercomplain customercomplain) {
        int pk = customercomplain.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customercomplainClp customercomplain = null;

        try {
            customercomplain = (customercomplainClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customercomplain.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", complainSubject=");
        sb.append(getComplainSubject());
        sb.append(", complainDetail=");
        sb.append(getComplainDetail());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", status=");
        sb.append(getStatus());
        sb.append(", complainDate=");
        sb.append(getComplainDate());
        sb.append(", lastanswerDate=");
        sb.append(getLastanswerDate());
        sb.append(", assignedTo=");
        sb.append(getAssignedTo());
        sb.append(", reIdOf=");
        sb.append(getReIdOf());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customercomplain");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainSubject</column-name><column-value><![CDATA[");
        sb.append(getComplainSubject());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainDetail</column-name><column-value><![CDATA[");
        sb.append(getComplainDetail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>status</column-name><column-value><![CDATA[");
        sb.append(getStatus());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainDate</column-name><column-value><![CDATA[");
        sb.append(getComplainDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lastanswerDate</column-name><column-value><![CDATA[");
        sb.append(getLastanswerDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>assignedTo</column-name><column-value><![CDATA[");
        sb.append(getAssignedTo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>reIdOf</column-name><column-value><![CDATA[");
        sb.append(getReIdOf());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
