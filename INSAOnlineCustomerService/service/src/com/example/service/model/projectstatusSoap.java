package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="projectstatusSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.projectstatusServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.projectstatusServiceSoap
 *
 */
public class projectstatusSoap implements Serializable {
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _percentageCompleted;
    private String _remaining;
    private String _statusfilldate;
    private String _uploaddd;

    public projectstatusSoap() {
    }

    public static projectstatusSoap toSoapModel(projectstatus model) {
        projectstatusSoap soapModel = new projectstatusSoap();

        soapModel.setId(model.getId());
        soapModel.setSubprojectId(model.getSubprojectId());
        soapModel.setActivityType(model.getActivityType());
        soapModel.setPercentageCompleted(model.getPercentageCompleted());
        soapModel.setRemaining(model.getRemaining());
        soapModel.setStatusfilldate(model.getStatusfilldate());
        soapModel.setUploaddd(model.getUploaddd());

        return soapModel;
    }

    public static projectstatusSoap[] toSoapModels(projectstatus[] models) {
        projectstatusSoap[] soapModels = new projectstatusSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static projectstatusSoap[][] toSoapModels(projectstatus[][] models) {
        projectstatusSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new projectstatusSoap[models.length][models[0].length];
        } else {
            soapModels = new projectstatusSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static projectstatusSoap[] toSoapModels(List<projectstatus> models) {
        List<projectstatusSoap> soapModels = new ArrayList<projectstatusSoap>(models.size());

        for (projectstatus model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new projectstatusSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return _activityType;
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getPercentageCompleted() {
        return _percentageCompleted;
    }

    public void setPercentageCompleted(String percentageCompleted) {
        _percentageCompleted = percentageCompleted;
    }

    public String getRemaining() {
        return _remaining;
    }

    public void setRemaining(String remaining) {
        _remaining = remaining;
    }

    public String getStatusfilldate() {
        return _statusfilldate;
    }

    public void setStatusfilldate(String statusfilldate) {
        _statusfilldate = statusfilldate;
    }

    public String getUploaddd() {
        return _uploaddd;
    }

    public void setUploaddd(String uploaddd) {
        _uploaddd = uploaddd;
    }
}
