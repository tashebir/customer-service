package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customercomplainSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.customercomplainServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.customercomplainServiceSoap
 *
 */
public class customercomplainSoap implements Serializable {
    private int _id;
    private String _complainSubject;
    private String _complainDetail;
    private String _subprojectId;
    private String _status;
    private String _complainDate;
    private String _lastanswerDate;
    private String _assignedTo;
    private int _reIdOf;

    public customercomplainSoap() {
    }

    public static customercomplainSoap toSoapModel(customercomplain model) {
        customercomplainSoap soapModel = new customercomplainSoap();

        soapModel.setId(model.getId());
        soapModel.setComplainSubject(model.getComplainSubject());
        soapModel.setComplainDetail(model.getComplainDetail());
        soapModel.setSubprojectId(model.getSubprojectId());
        soapModel.setStatus(model.getStatus());
        soapModel.setComplainDate(model.getComplainDate());
        soapModel.setLastanswerDate(model.getLastanswerDate());
        soapModel.setAssignedTo(model.getAssignedTo());
        soapModel.setReIdOf(model.getReIdOf());

        return soapModel;
    }

    public static customercomplainSoap[] toSoapModels(customercomplain[] models) {
        customercomplainSoap[] soapModels = new customercomplainSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static customercomplainSoap[][] toSoapModels(
        customercomplain[][] models) {
        customercomplainSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new customercomplainSoap[models.length][models[0].length];
        } else {
            soapModels = new customercomplainSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static customercomplainSoap[] toSoapModels(
        List<customercomplain> models) {
        List<customercomplainSoap> soapModels = new ArrayList<customercomplainSoap>(models.size());

        for (customercomplain model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new customercomplainSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainSubject() {
        return _complainSubject;
    }

    public void setComplainSubject(String complainSubject) {
        _complainSubject = complainSubject;
    }

    public String getComplainDetail() {
        return _complainDetail;
    }

    public void setComplainDetail(String complainDetail) {
        _complainDetail = complainDetail;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getStatus() {
        return _status;
    }

    public void setStatus(String status) {
        _status = status;
    }

    public String getComplainDate() {
        return _complainDate;
    }

    public void setComplainDate(String complainDate) {
        _complainDate = complainDate;
    }

    public String getLastanswerDate() {
        return _lastanswerDate;
    }

    public void setLastanswerDate(String lastanswerDate) {
        _lastanswerDate = lastanswerDate;
    }

    public String getAssignedTo() {
        return _assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        _assignedTo = assignedTo;
    }

    public int getReIdOf() {
        return _reIdOf;
    }

    public void setReIdOf(int reIdOf) {
        _reIdOf = reIdOf;
    }
}
