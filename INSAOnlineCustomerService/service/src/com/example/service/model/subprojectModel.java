package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="subprojectModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>subproject</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.subproject
 * @see com.example.service.model.impl.subprojectImpl
 * @see com.example.service.model.impl.subprojectModelImpl
 *
 */
public interface subprojectModel extends BaseModel<subproject> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getSubproject_id();

    public void setSubproject_id(String subproject_id);

    public String getSubproject_name();

    public void setSubproject_name(String subproject_name);

    public String getStart_date();

    public void setStart_date(String start_date);

    public String getEnd_date();

    public void setEnd_date(String end_date);

    public String getDuration();

    public void setDuration(String duration);

    public String getDescription();

    public void setDescription(String description);

    public String getProjectId();

    public void setProjectId(String projectId);

    public subproject toEscapedModel();
}
