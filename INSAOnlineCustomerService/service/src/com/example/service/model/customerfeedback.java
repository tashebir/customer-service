package com.example.service.model;


/**
 * <a href="customerfeedback.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>customerfeedback</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.customerfeedbackImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customerfeedbackModel
 * @see com.example.service.model.impl.customerfeedbackImpl
 * @see com.example.service.model.impl.customerfeedbackModelImpl
 *
 */
public interface customerfeedback extends customerfeedbackModel {
}
