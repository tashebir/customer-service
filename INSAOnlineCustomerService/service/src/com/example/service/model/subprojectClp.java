package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class subprojectClp extends BaseModelImpl<subproject>
    implements subproject {
    private int _id;
    private String _subproject_id;
    private String _subproject_name;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _projectId;

    public subprojectClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubproject_id() {
        return _subproject_id;
    }

    public void setSubproject_id(String subproject_id) {
        _subproject_id = subproject_id;
    }

    public String getSubproject_name() {
        return _subproject_name;
    }

    public void setSubproject_name(String subproject_name) {
        _subproject_name = subproject_name;
    }

    public String getStart_date() {
        return _start_date;
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return _end_date;
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public subproject toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            subproject model = new subprojectClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubproject_id(HtmlUtil.escape(getSubproject_id()));
            model.setSubproject_name(HtmlUtil.escape(getSubproject_name()));
            model.setStart_date(HtmlUtil.escape(getStart_date()));
            model.setEnd_date(HtmlUtil.escape(getEnd_date()));
            model.setDuration(HtmlUtil.escape(getDuration()));
            model.setDescription(HtmlUtil.escape(getDescription()));
            model.setProjectId(HtmlUtil.escape(getProjectId()));

            model = (subproject) Proxy.newProxyInstance(subproject.class.getClassLoader(),
                    new Class[] { subproject.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        subprojectClp clone = new subprojectClp();

        clone.setId(getId());
        clone.setSubproject_id(getSubproject_id());
        clone.setSubproject_name(getSubproject_name());
        clone.setStart_date(getStart_date());
        clone.setEnd_date(getEnd_date());
        clone.setDuration(getDuration());
        clone.setDescription(getDescription());
        clone.setProjectId(getProjectId());

        return clone;
    }

    public int compareTo(subproject subproject) {
        int pk = subproject.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        subprojectClp subproject = null;

        try {
            subproject = (subprojectClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = subproject.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subproject_id=");
        sb.append(getSubproject_id());
        sb.append(", subproject_name=");
        sb.append(getSubproject_name());
        sb.append(", start_date=");
        sb.append(getStart_date());
        sb.append(", end_date=");
        sb.append(getEnd_date());
        sb.append(", duration=");
        sb.append(getDuration());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.subproject");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subproject_id</column-name><column-value><![CDATA[");
        sb.append(getSubproject_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subproject_name</column-name><column-value><![CDATA[");
        sb.append(getSubproject_name());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>start_date</column-name><column-value><![CDATA[");
        sb.append(getStart_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>end_date</column-name><column-value><![CDATA[");
        sb.append(getEnd_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>duration</column-name><column-value><![CDATA[");
        sb.append(getDuration());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
