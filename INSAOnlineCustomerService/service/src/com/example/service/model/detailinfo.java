package com.example.service.model;


/**
 * <a href="detailinfo.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>detailinfo</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.detailinfoImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.detailinfoModel
 * @see com.example.service.model.impl.detailinfoImpl
 * @see com.example.service.model.impl.detailinfoModelImpl
 *
 */
public interface detailinfo extends detailinfoModel {
}
