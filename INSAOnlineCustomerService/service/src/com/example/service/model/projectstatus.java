package com.example.service.model;


/**
 * <a href="projectstatus.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>projectstatus</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.projectstatusImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectstatusModel
 * @see com.example.service.model.impl.projectstatusImpl
 * @see com.example.service.model.impl.projectstatusModelImpl
 *
 */
public interface projectstatus extends projectstatusModel {
}
