package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class projectstatusClp extends BaseModelImpl<projectstatus>
    implements projectstatus {
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _percentageCompleted;
    private String _remaining;
    private String _statusfilldate;
    private String _uploaddd;

    public projectstatusClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return _activityType;
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getPercentageCompleted() {
        return _percentageCompleted;
    }

    public void setPercentageCompleted(String percentageCompleted) {
        _percentageCompleted = percentageCompleted;
    }

    public String getRemaining() {
        return _remaining;
    }

    public void setRemaining(String remaining) {
        _remaining = remaining;
    }

    public String getStatusfilldate() {
        return _statusfilldate;
    }

    public void setStatusfilldate(String statusfilldate) {
        _statusfilldate = statusfilldate;
    }

    public String getUploaddd() {
        return _uploaddd;
    }

    public void setUploaddd(String uploaddd) {
        _uploaddd = uploaddd;
    }

    public projectstatus toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            projectstatus model = new projectstatusClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setActivityType(HtmlUtil.escape(getActivityType()));
            model.setPercentageCompleted(HtmlUtil.escape(
                    getPercentageCompleted()));
            model.setRemaining(HtmlUtil.escape(getRemaining()));
            model.setStatusfilldate(HtmlUtil.escape(getStatusfilldate()));
            model.setUploaddd(HtmlUtil.escape(getUploaddd()));

            model = (projectstatus) Proxy.newProxyInstance(projectstatus.class.getClassLoader(),
                    new Class[] { projectstatus.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        projectstatusClp clone = new projectstatusClp();

        clone.setId(getId());
        clone.setSubprojectId(getSubprojectId());
        clone.setActivityType(getActivityType());
        clone.setPercentageCompleted(getPercentageCompleted());
        clone.setRemaining(getRemaining());
        clone.setStatusfilldate(getStatusfilldate());
        clone.setUploaddd(getUploaddd());

        return clone;
    }

    public int compareTo(projectstatus projectstatus) {
        int pk = projectstatus.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        projectstatusClp projectstatus = null;

        try {
            projectstatus = (projectstatusClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = projectstatus.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", activityType=");
        sb.append(getActivityType());
        sb.append(", percentageCompleted=");
        sb.append(getPercentageCompleted());
        sb.append(", remaining=");
        sb.append(getRemaining());
        sb.append(", statusfilldate=");
        sb.append(getStatusfilldate());
        sb.append(", uploaddd=");
        sb.append(getUploaddd());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.projectstatus");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activityType</column-name><column-value><![CDATA[");
        sb.append(getActivityType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>percentageCompleted</column-name><column-value><![CDATA[");
        sb.append(getPercentageCompleted());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remaining</column-name><column-value><![CDATA[");
        sb.append(getRemaining());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>statusfilldate</column-name><column-value><![CDATA[");
        sb.append(getStatusfilldate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>uploaddd</column-name><column-value><![CDATA[");
        sb.append(getUploaddd());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
