package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="subprojectactivityplanModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>subprojectactivityplan</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.subprojectactivityplan
 * @see com.example.service.model.impl.subprojectactivityplanImpl
 * @see com.example.service.model.impl.subprojectactivityplanModelImpl
 *
 */
public interface subprojectactivityplanModel extends BaseModel<subprojectactivityplan> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getSubprojectId();

    public void setSubprojectId(String subprojectId);

    public String getActivityType();

    public void setActivityType(String activityType);

    public String getStart_date();

    public void setStart_date(String start_date);

    public String getEnd_date();

    public void setEnd_date(String end_date);

    public String getDuration();

    public void setDuration(String duration);

    public String getDescription();

    public void setDescription(String description);

    public String getMore();

    public void setMore(String more);

    public subprojectactivityplan toEscapedModel();
}
