package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="customerprojectModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>customerproject</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customerproject
 * @see com.example.service.model.impl.customerprojectImpl
 * @see com.example.service.model.impl.customerprojectModelImpl
 *
 */
public interface customerprojectModel extends BaseModel<customerproject> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getProjectId();

    public void setProjectId(String projectId);

    public String getProjectName();

    public void setProjectName(String projectName);

    public String getStartDate();

    public void setStartDate(String startDate);

    public String getEndDate();

    public void setEndDate(String endDate);

    public String getUserIId();

    public void setUserIId(String userIId);

    public String getProjectManagerId();

    public void setProjectManagerId(String projectManagerId);

    public int getOrgid();

    public void setOrgid(int orgid);

    public customerproject toEscapedModel();
}
