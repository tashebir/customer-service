package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="projectactivitySoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.projectactivityServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.projectactivityServiceSoap
 *
 */
public class projectactivitySoap implements Serializable {
    private int _id;
    private String _activity;
    private String _description;

    public projectactivitySoap() {
    }

    public static projectactivitySoap toSoapModel(projectactivity model) {
        projectactivitySoap soapModel = new projectactivitySoap();

        soapModel.setId(model.getId());
        soapModel.setActivity(model.getActivity());
        soapModel.setDescription(model.getDescription());

        return soapModel;
    }

    public static projectactivitySoap[] toSoapModels(projectactivity[] models) {
        projectactivitySoap[] soapModels = new projectactivitySoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static projectactivitySoap[][] toSoapModels(
        projectactivity[][] models) {
        projectactivitySoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new projectactivitySoap[models.length][models[0].length];
        } else {
            soapModels = new projectactivitySoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static projectactivitySoap[] toSoapModels(
        List<projectactivity> models) {
        List<projectactivitySoap> soapModels = new ArrayList<projectactivitySoap>(models.size());

        for (projectactivity model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new projectactivitySoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getActivity() {
        return _activity;
    }

    public void setActivity(String activity) {
        _activity = activity;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }
}
