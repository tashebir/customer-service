package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="subprojectSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.subprojectServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.subprojectServiceSoap
 *
 */
public class subprojectSoap implements Serializable {
    private int _id;
    private String _subproject_id;
    private String _subproject_name;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _projectId;

    public subprojectSoap() {
    }

    public static subprojectSoap toSoapModel(subproject model) {
        subprojectSoap soapModel = new subprojectSoap();

        soapModel.setId(model.getId());
        soapModel.setSubproject_id(model.getSubproject_id());
        soapModel.setSubproject_name(model.getSubproject_name());
        soapModel.setStart_date(model.getStart_date());
        soapModel.setEnd_date(model.getEnd_date());
        soapModel.setDuration(model.getDuration());
        soapModel.setDescription(model.getDescription());
        soapModel.setProjectId(model.getProjectId());

        return soapModel;
    }

    public static subprojectSoap[] toSoapModels(subproject[] models) {
        subprojectSoap[] soapModels = new subprojectSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static subprojectSoap[][] toSoapModels(subproject[][] models) {
        subprojectSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new subprojectSoap[models.length][models[0].length];
        } else {
            soapModels = new subprojectSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static subprojectSoap[] toSoapModels(List<subproject> models) {
        List<subprojectSoap> soapModels = new ArrayList<subprojectSoap>(models.size());

        for (subproject model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new subprojectSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubproject_id() {
        return _subproject_id;
    }

    public void setSubproject_id(String subproject_id) {
        _subproject_id = subproject_id;
    }

    public String getSubproject_name() {
        return _subproject_name;
    }

    public void setSubproject_name(String subproject_name) {
        _subproject_name = subproject_name;
    }

    public String getStart_date() {
        return _start_date;
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return _end_date;
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }
}
