package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="projectstatusModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>projectstatus</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectstatus
 * @see com.example.service.model.impl.projectstatusImpl
 * @see com.example.service.model.impl.projectstatusModelImpl
 *
 */
public interface projectstatusModel extends BaseModel<projectstatus> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getSubprojectId();

    public void setSubprojectId(String subprojectId);

    public String getActivityType();

    public void setActivityType(String activityType);

    public String getPercentageCompleted();

    public void setPercentageCompleted(String percentageCompleted);

    public String getRemaining();

    public void setRemaining(String remaining);

    public String getStatusfilldate();

    public void setStatusfilldate(String statusfilldate);

    public String getUploaddd();

    public void setUploaddd(String uploaddd);

    public projectstatus toEscapedModel();
}
