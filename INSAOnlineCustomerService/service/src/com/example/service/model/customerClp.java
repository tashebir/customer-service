package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class customerClp extends BaseModelImpl<customer> implements customer {
    private int _id;
    private String _organName;
    private String _sector;
    private String _email;
    private String _fixedtele;
    private String _mobiletele;
    private String _fax;
    private String _region;
    private String _zone;
    private String _woreda;
    private String _kebele;
    private String _houseno;
    private String _userIId;

    public customerClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrganName() {
        return _organName;
    }

    public void setOrganName(String organName) {
        _organName = organName;
    }

    public String getSector() {
        return _sector;
    }

    public void setSector(String sector) {
        _sector = sector;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getFixedtele() {
        return _fixedtele;
    }

    public void setFixedtele(String fixedtele) {
        _fixedtele = fixedtele;
    }

    public String getMobiletele() {
        return _mobiletele;
    }

    public void setMobiletele(String mobiletele) {
        _mobiletele = mobiletele;
    }

    public String getFax() {
        return _fax;
    }

    public void setFax(String fax) {
        _fax = fax;
    }

    public String getRegion() {
        return _region;
    }

    public void setRegion(String region) {
        _region = region;
    }

    public String getZone() {
        return _zone;
    }

    public void setZone(String zone) {
        _zone = zone;
    }

    public String getWoreda() {
        return _woreda;
    }

    public void setWoreda(String woreda) {
        _woreda = woreda;
    }

    public String getKebele() {
        return _kebele;
    }

    public void setKebele(String kebele) {
        _kebele = kebele;
    }

    public String getHouseno() {
        return _houseno;
    }

    public void setHouseno(String houseno) {
        _houseno = houseno;
    }

    public String getUserIId() {
        return _userIId;
    }

    public void setUserIId(String userIId) {
        _userIId = userIId;
    }

    public customer toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            customer model = new customerClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setOrganName(HtmlUtil.escape(getOrganName()));
            model.setSector(HtmlUtil.escape(getSector()));
            model.setEmail(HtmlUtil.escape(getEmail()));
            model.setFixedtele(HtmlUtil.escape(getFixedtele()));
            model.setMobiletele(HtmlUtil.escape(getMobiletele()));
            model.setFax(HtmlUtil.escape(getFax()));
            model.setRegion(HtmlUtil.escape(getRegion()));
            model.setZone(HtmlUtil.escape(getZone()));
            model.setWoreda(HtmlUtil.escape(getWoreda()));
            model.setKebele(HtmlUtil.escape(getKebele()));
            model.setHouseno(HtmlUtil.escape(getHouseno()));
            model.setUserIId(HtmlUtil.escape(getUserIId()));

            model = (customer) Proxy.newProxyInstance(customer.class.getClassLoader(),
                    new Class[] { customer.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customerClp clone = new customerClp();

        clone.setId(getId());
        clone.setOrganName(getOrganName());
        clone.setSector(getSector());
        clone.setEmail(getEmail());
        clone.setFixedtele(getFixedtele());
        clone.setMobiletele(getMobiletele());
        clone.setFax(getFax());
        clone.setRegion(getRegion());
        clone.setZone(getZone());
        clone.setWoreda(getWoreda());
        clone.setKebele(getKebele());
        clone.setHouseno(getHouseno());
        clone.setUserIId(getUserIId());

        return clone;
    }

    public int compareTo(customer customer) {
        int pk = customer.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customerClp customer = null;

        try {
            customer = (customerClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customer.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", organName=");
        sb.append(getOrganName());
        sb.append(", sector=");
        sb.append(getSector());
        sb.append(", email=");
        sb.append(getEmail());
        sb.append(", fixedtele=");
        sb.append(getFixedtele());
        sb.append(", mobiletele=");
        sb.append(getMobiletele());
        sb.append(", fax=");
        sb.append(getFax());
        sb.append(", region=");
        sb.append(getRegion());
        sb.append(", zone=");
        sb.append(getZone());
        sb.append(", woreda=");
        sb.append(getWoreda());
        sb.append(", kebele=");
        sb.append(getKebele());
        sb.append(", houseno=");
        sb.append(getHouseno());
        sb.append(", userIId=");
        sb.append(getUserIId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customer");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organName</column-name><column-value><![CDATA[");
        sb.append(getOrganName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sector</column-name><column-value><![CDATA[");
        sb.append(getSector());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>email</column-name><column-value><![CDATA[");
        sb.append(getEmail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fixedtele</column-name><column-value><![CDATA[");
        sb.append(getFixedtele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>mobiletele</column-name><column-value><![CDATA[");
        sb.append(getMobiletele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fax</column-name><column-value><![CDATA[");
        sb.append(getFax());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>region</column-name><column-value><![CDATA[");
        sb.append(getRegion());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>zone</column-name><column-value><![CDATA[");
        sb.append(getZone());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>woreda</column-name><column-value><![CDATA[");
        sb.append(getWoreda());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>kebele</column-name><column-value><![CDATA[");
        sb.append(getKebele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>houseno</column-name><column-value><![CDATA[");
        sb.append(getHouseno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userIId</column-name><column-value><![CDATA[");
        sb.append(getUserIId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
