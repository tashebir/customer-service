package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerfeedbackSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.customerfeedbackServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.customerfeedbackServiceSoap
 *
 */
public class customerfeedbackSoap implements Serializable {
    private int _id;
    private String _orgname;
    private String _customerName;
    private String _email;
    private String _urfeedback;
    private String _projectid;

    public customerfeedbackSoap() {
    }

    public static customerfeedbackSoap toSoapModel(customerfeedback model) {
        customerfeedbackSoap soapModel = new customerfeedbackSoap();

        soapModel.setId(model.getId());
        soapModel.setOrgname(model.getOrgname());
        soapModel.setCustomerName(model.getCustomerName());
        soapModel.setEmail(model.getEmail());
        soapModel.setUrfeedback(model.getUrfeedback());
        soapModel.setProjectid(model.getProjectid());

        return soapModel;
    }

    public static customerfeedbackSoap[] toSoapModels(customerfeedback[] models) {
        customerfeedbackSoap[] soapModels = new customerfeedbackSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static customerfeedbackSoap[][] toSoapModels(
        customerfeedback[][] models) {
        customerfeedbackSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new customerfeedbackSoap[models.length][models[0].length];
        } else {
            soapModels = new customerfeedbackSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static customerfeedbackSoap[] toSoapModels(
        List<customerfeedback> models) {
        List<customerfeedbackSoap> soapModels = new ArrayList<customerfeedbackSoap>(models.size());

        for (customerfeedback model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new customerfeedbackSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrgname() {
        return _orgname;
    }

    public void setOrgname(String orgname) {
        _orgname = orgname;
    }

    public String getCustomerName() {
        return _customerName;
    }

    public void setCustomerName(String customerName) {
        _customerName = customerName;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getUrfeedback() {
        return _urfeedback;
    }

    public void setUrfeedback(String urfeedback) {
        _urfeedback = urfeedback;
    }

    public String getProjectid() {
        return _projectid;
    }

    public void setProjectid(String projectid) {
        _projectid = projectid;
    }
}
