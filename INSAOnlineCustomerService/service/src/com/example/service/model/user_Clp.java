package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class user_Clp extends BaseModelImpl<user_> implements user_ {
    private int _userId;
    private String _screenName;
    private String _emailAddress;
    private String _jobTitle;
    private String _uuid_;

    public user_Clp() {
    }

    public int getPrimaryKey() {
        return _userId;
    }

    public void setPrimaryKey(int pk) {
        setUserId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_userId);
    }

    public int getUserId() {
        return _userId;
    }

    public void setUserId(int userId) {
        _userId = userId;
    }

    public String getScreenName() {
        return _screenName;
    }

    public void setScreenName(String screenName) {
        _screenName = screenName;
    }

    public String getEmailAddress() {
        return _emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        _emailAddress = emailAddress;
    }

    public String getJobTitle() {
        return _jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        _jobTitle = jobTitle;
    }

    public String getUuid_() {
        return _uuid_;
    }

    public void setUuid_(String uuid_) {
        _uuid_ = uuid_;
    }

    public user_ toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            user_ model = new user_Clp();

            model.setEscapedModel(true);

            model.setUserId(getUserId());
            model.setScreenName(HtmlUtil.escape(getScreenName()));
            model.setEmailAddress(HtmlUtil.escape(getEmailAddress()));
            model.setJobTitle(HtmlUtil.escape(getJobTitle()));
            model.setUuid_(HtmlUtil.escape(getUuid_()));

            model = (user_) Proxy.newProxyInstance(user_.class.getClassLoader(),
                    new Class[] { user_.class }, new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        user_Clp clone = new user_Clp();

        clone.setUserId(getUserId());
        clone.setScreenName(getScreenName());
        clone.setEmailAddress(getEmailAddress());
        clone.setJobTitle(getJobTitle());
        clone.setUuid_(getUuid_());

        return clone;
    }

    public int compareTo(user_ user_) {
        int pk = user_.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        user_Clp user_ = null;

        try {
            user_ = (user_Clp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = user_.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{userId=");
        sb.append(getUserId());
        sb.append(", screenName=");
        sb.append(getScreenName());
        sb.append(", emailAddress=");
        sb.append(getEmailAddress());
        sb.append(", jobTitle=");
        sb.append(getJobTitle());
        sb.append(", uuid_=");
        sb.append(getUuid_());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.user_");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>screenName</column-name><column-value><![CDATA[");
        sb.append(getScreenName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>emailAddress</column-name><column-value><![CDATA[");
        sb.append(getEmailAddress());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>jobTitle</column-name><column-value><![CDATA[");
        sb.append(getJobTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>uuid_</column-name><column-value><![CDATA[");
        sb.append(getUuid_());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
