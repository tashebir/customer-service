package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerprojectSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.customerprojectServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.customerprojectServiceSoap
 *
 */
public class customerprojectSoap implements Serializable {
    private int _id;
    private String _projectId;
    private String _projectName;
    private String _startDate;
    private String _endDate;
    private String _userIId;
    private String _projectManagerId;
    private int _orgid;

    public customerprojectSoap() {
    }

    public static customerprojectSoap toSoapModel(customerproject model) {
        customerprojectSoap soapModel = new customerprojectSoap();

        soapModel.setId(model.getId());
        soapModel.setProjectId(model.getProjectId());
        soapModel.setProjectName(model.getProjectName());
        soapModel.setStartDate(model.getStartDate());
        soapModel.setEndDate(model.getEndDate());
        soapModel.setUserIId(model.getUserIId());
        soapModel.setProjectManagerId(model.getProjectManagerId());
        soapModel.setOrgid(model.getOrgid());

        return soapModel;
    }

    public static customerprojectSoap[] toSoapModels(customerproject[] models) {
        customerprojectSoap[] soapModels = new customerprojectSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static customerprojectSoap[][] toSoapModels(
        customerproject[][] models) {
        customerprojectSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new customerprojectSoap[models.length][models[0].length];
        } else {
            soapModels = new customerprojectSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static customerprojectSoap[] toSoapModels(
        List<customerproject> models) {
        List<customerprojectSoap> soapModels = new ArrayList<customerprojectSoap>(models.size());

        for (customerproject model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new customerprojectSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public String getProjectName() {
        return _projectName;
    }

    public void setProjectName(String projectName) {
        _projectName = projectName;
    }

    public String getStartDate() {
        return _startDate;
    }

    public void setStartDate(String startDate) {
        _startDate = startDate;
    }

    public String getEndDate() {
        return _endDate;
    }

    public void setEndDate(String endDate) {
        _endDate = endDate;
    }

    public String getUserIId() {
        return _userIId;
    }

    public void setUserIId(String userIId) {
        _userIId = userIId;
    }

    public String getProjectManagerId() {
        return _projectManagerId;
    }

    public void setProjectManagerId(String projectManagerId) {
        _projectManagerId = projectManagerId;
    }

    public int getOrgid() {
        return _orgid;
    }

    public void setOrgid(int orgid) {
        _orgid = orgid;
    }
}
