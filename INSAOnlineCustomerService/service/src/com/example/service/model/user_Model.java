package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="user_Model.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>user_</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.user_
 * @see com.example.service.model.impl.user_Impl
 * @see com.example.service.model.impl.user_ModelImpl
 *
 */
public interface user_Model extends BaseModel<user_> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getUserId();

    public void setUserId(int userId);

    public String getScreenName();

    public void setScreenName(String screenName);

    public String getEmailAddress();

    public void setEmailAddress(String emailAddress);

    public String getJobTitle();

    public void setJobTitle(String jobTitle);

    public String getUuid_();

    public void setUuid_(String uuid_);

    public user_ toEscapedModel();
}
