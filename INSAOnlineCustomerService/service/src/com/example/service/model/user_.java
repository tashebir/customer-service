package com.example.service.model;


/**
 * <a href="user_.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>user_</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.user_Impl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.user_Model
 * @see com.example.service.model.impl.user_Impl
 * @see com.example.service.model.impl.user_ModelImpl
 *
 */
public interface user_ extends user_Model {
}
