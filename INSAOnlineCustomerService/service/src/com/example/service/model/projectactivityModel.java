package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="projectactivityModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>projectactivity</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectactivity
 * @see com.example.service.model.impl.projectactivityImpl
 * @see com.example.service.model.impl.projectactivityModelImpl
 *
 */
public interface projectactivityModel extends BaseModel<projectactivity> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getActivity();

    public void setActivity(String activity);

    public String getDescription();

    public void setDescription(String description);

    public projectactivity toEscapedModel();
}
