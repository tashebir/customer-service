package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class complainSolutionClp extends BaseModelImpl<complainSolution>
    implements complainSolution {
    private int _solutionId;
    private String _solutionSubject;
    private String _solutionDetail;
    private String _solutionDate;
    private String _complainId;
    private String _subprojectId;

    public complainSolutionClp() {
    }

    public int getPrimaryKey() {
        return _solutionId;
    }

    public void setPrimaryKey(int pk) {
        setSolutionId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_solutionId);
    }

    public int getSolutionId() {
        return _solutionId;
    }

    public void setSolutionId(int solutionId) {
        _solutionId = solutionId;
    }

    public String getSolutionSubject() {
        return _solutionSubject;
    }

    public void setSolutionSubject(String solutionSubject) {
        _solutionSubject = solutionSubject;
    }

    public String getSolutionDetail() {
        return _solutionDetail;
    }

    public void setSolutionDetail(String solutionDetail) {
        _solutionDetail = solutionDetail;
    }

    public String getSolutionDate() {
        return _solutionDate;
    }

    public void setSolutionDate(String solutionDate) {
        _solutionDate = solutionDate;
    }

    public String getComplainId() {
        return _complainId;
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public complainSolution toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            complainSolution model = new complainSolutionClp();

            model.setEscapedModel(true);

            model.setSolutionId(getSolutionId());
            model.setSolutionSubject(HtmlUtil.escape(getSolutionSubject()));
            model.setSolutionDetail(HtmlUtil.escape(getSolutionDetail()));
            model.setSolutionDate(HtmlUtil.escape(getSolutionDate()));
            model.setComplainId(HtmlUtil.escape(getComplainId()));
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));

            model = (complainSolution) Proxy.newProxyInstance(complainSolution.class.getClassLoader(),
                    new Class[] { complainSolution.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        complainSolutionClp clone = new complainSolutionClp();

        clone.setSolutionId(getSolutionId());
        clone.setSolutionSubject(getSolutionSubject());
        clone.setSolutionDetail(getSolutionDetail());
        clone.setSolutionDate(getSolutionDate());
        clone.setComplainId(getComplainId());
        clone.setSubprojectId(getSubprojectId());

        return clone;
    }

    public int compareTo(complainSolution complainSolution) {
        int pk = complainSolution.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        complainSolutionClp complainSolution = null;

        try {
            complainSolution = (complainSolutionClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = complainSolution.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{solutionId=");
        sb.append(getSolutionId());
        sb.append(", solutionSubject=");
        sb.append(getSolutionSubject());
        sb.append(", solutionDetail=");
        sb.append(getSolutionDetail());
        sb.append(", solutionDate=");
        sb.append(getSolutionDate());
        sb.append(", complainId=");
        sb.append(getComplainId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.complainSolution");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>solutionId</column-name><column-value><![CDATA[");
        sb.append(getSolutionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionSubject</column-name><column-value><![CDATA[");
        sb.append(getSolutionSubject());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionDetail</column-name><column-value><![CDATA[");
        sb.append(getSolutionDetail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionDate</column-name><column-value><![CDATA[");
        sb.append(getSolutionDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainId</column-name><column-value><![CDATA[");
        sb.append(getComplainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
