package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="customerfeedbackModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>customerfeedback</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customerfeedback
 * @see com.example.service.model.impl.customerfeedbackImpl
 * @see com.example.service.model.impl.customerfeedbackModelImpl
 *
 */
public interface customerfeedbackModel extends BaseModel<customerfeedback> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getOrgname();

    public void setOrgname(String orgname);

    public String getCustomerName();

    public void setCustomerName(String customerName);

    public String getEmail();

    public void setEmail(String email);

    public String getUrfeedback();

    public void setUrfeedback(String urfeedback);

    public String getProjectid();

    public void setProjectid(String projectid);

    public customerfeedback toEscapedModel();
}
