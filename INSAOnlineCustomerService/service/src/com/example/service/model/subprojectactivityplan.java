package com.example.service.model;


/**
 * <a href="subprojectactivityplan.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>subprojectactivityplan</code> table
 * in the database.
 * </p>
 *
 * <p>
 * Customize <code>com.example.service.model.impl.subprojectactivityplanImpl</code>
 * and rerun the ServiceBuilder to generate the new methods.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.subprojectactivityplanModel
 * @see com.example.service.model.impl.subprojectactivityplanImpl
 * @see com.example.service.model.impl.subprojectactivityplanModelImpl
 *
 */
public interface subprojectactivityplan extends subprojectactivityplanModel {
}
