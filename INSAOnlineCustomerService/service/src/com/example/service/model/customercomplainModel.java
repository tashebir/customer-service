package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="customercomplainModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>customercomplain</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customercomplain
 * @see com.example.service.model.impl.customercomplainImpl
 * @see com.example.service.model.impl.customercomplainModelImpl
 *
 */
public interface customercomplainModel extends BaseModel<customercomplain> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getComplainSubject();

    public void setComplainSubject(String complainSubject);

    public String getComplainDetail();

    public void setComplainDetail(String complainDetail);

    public String getSubprojectId();

    public void setSubprojectId(String subprojectId);

    public String getStatus();

    public void setStatus(String status);

    public String getComplainDate();

    public void setComplainDate(String complainDate);

    public String getLastanswerDate();

    public void setLastanswerDate(String lastanswerDate);

    public String getAssignedTo();

    public void setAssignedTo(String assignedTo);

    public int getReIdOf();

    public void setReIdOf(int reIdOf);

    public customercomplain toEscapedModel();
}
