package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="subprojectactivityplanSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.subprojectactivityplanServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.subprojectactivityplanServiceSoap
 *
 */
public class subprojectactivityplanSoap implements Serializable {
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _more;

    public subprojectactivityplanSoap() {
    }

    public static subprojectactivityplanSoap toSoapModel(
        subprojectactivityplan model) {
        subprojectactivityplanSoap soapModel = new subprojectactivityplanSoap();

        soapModel.setId(model.getId());
        soapModel.setSubprojectId(model.getSubprojectId());
        soapModel.setActivityType(model.getActivityType());
        soapModel.setStart_date(model.getStart_date());
        soapModel.setEnd_date(model.getEnd_date());
        soapModel.setDuration(model.getDuration());
        soapModel.setDescription(model.getDescription());
        soapModel.setMore(model.getMore());

        return soapModel;
    }

    public static subprojectactivityplanSoap[] toSoapModels(
        subprojectactivityplan[] models) {
        subprojectactivityplanSoap[] soapModels = new subprojectactivityplanSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static subprojectactivityplanSoap[][] toSoapModels(
        subprojectactivityplan[][] models) {
        subprojectactivityplanSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new subprojectactivityplanSoap[models.length][models[0].length];
        } else {
            soapModels = new subprojectactivityplanSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static subprojectactivityplanSoap[] toSoapModels(
        List<subprojectactivityplan> models) {
        List<subprojectactivityplanSoap> soapModels = new ArrayList<subprojectactivityplanSoap>(models.size());

        for (subprojectactivityplan model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new subprojectactivityplanSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return _activityType;
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getStart_date() {
        return _start_date;
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return _end_date;
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getMore() {
        return _more;
    }

    public void setMore(String more) {
        _more = more;
    }
}
