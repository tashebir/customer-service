package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="complainReportSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.complainReportServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.complainReportServiceSoap
 *
 */
public class complainReportSoap implements Serializable {
    private int _id;
    private String _complainType;
    private String _complainId;
    private String _remark;

    public complainReportSoap() {
    }

    public static complainReportSoap toSoapModel(complainReport model) {
        complainReportSoap soapModel = new complainReportSoap();

        soapModel.setId(model.getId());
        soapModel.setComplainType(model.getComplainType());
        soapModel.setComplainId(model.getComplainId());
        soapModel.setRemark(model.getRemark());

        return soapModel;
    }

    public static complainReportSoap[] toSoapModels(complainReport[] models) {
        complainReportSoap[] soapModels = new complainReportSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static complainReportSoap[][] toSoapModels(complainReport[][] models) {
        complainReportSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new complainReportSoap[models.length][models[0].length];
        } else {
            soapModels = new complainReportSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static complainReportSoap[] toSoapModels(List<complainReport> models) {
        List<complainReportSoap> soapModels = new ArrayList<complainReportSoap>(models.size());

        for (complainReport model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new complainReportSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainType() {
        return _complainType;
    }

    public void setComplainType(String complainType) {
        _complainType = complainType;
    }

    public String getComplainId() {
        return _complainId;
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }
}
