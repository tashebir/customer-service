package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class subprojectactivityplanClp extends BaseModelImpl<subprojectactivityplan>
    implements subprojectactivityplan {
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _more;

    public subprojectactivityplanClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return _activityType;
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getStart_date() {
        return _start_date;
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return _end_date;
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return _duration;
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getMore() {
        return _more;
    }

    public void setMore(String more) {
        _more = more;
    }

    public subprojectactivityplan toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            subprojectactivityplan model = new subprojectactivityplanClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setActivityType(HtmlUtil.escape(getActivityType()));
            model.setStart_date(HtmlUtil.escape(getStart_date()));
            model.setEnd_date(HtmlUtil.escape(getEnd_date()));
            model.setDuration(HtmlUtil.escape(getDuration()));
            model.setDescription(HtmlUtil.escape(getDescription()));
            model.setMore(HtmlUtil.escape(getMore()));

            model = (subprojectactivityplan) Proxy.newProxyInstance(subprojectactivityplan.class.getClassLoader(),
                    new Class[] { subprojectactivityplan.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        subprojectactivityplanClp clone = new subprojectactivityplanClp();

        clone.setId(getId());
        clone.setSubprojectId(getSubprojectId());
        clone.setActivityType(getActivityType());
        clone.setStart_date(getStart_date());
        clone.setEnd_date(getEnd_date());
        clone.setDuration(getDuration());
        clone.setDescription(getDescription());
        clone.setMore(getMore());

        return clone;
    }

    public int compareTo(subprojectactivityplan subprojectactivityplan) {
        int pk = subprojectactivityplan.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        subprojectactivityplanClp subprojectactivityplan = null;

        try {
            subprojectactivityplan = (subprojectactivityplanClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = subprojectactivityplan.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", activityType=");
        sb.append(getActivityType());
        sb.append(", start_date=");
        sb.append(getStart_date());
        sb.append(", end_date=");
        sb.append(getEnd_date());
        sb.append(", duration=");
        sb.append(getDuration());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", more=");
        sb.append(getMore());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.subprojectactivityplan");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activityType</column-name><column-value><![CDATA[");
        sb.append(getActivityType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>start_date</column-name><column-value><![CDATA[");
        sb.append(getStart_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>end_date</column-name><column-value><![CDATA[");
        sb.append(getEnd_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>duration</column-name><column-value><![CDATA[");
        sb.append(getDuration());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>more</column-name><column-value><![CDATA[");
        sb.append(getMore());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
