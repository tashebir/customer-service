package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class detailinfoClp extends BaseModelImpl<detailinfo>
    implements detailinfo {
    private int _id;
    private String _title;
    private String _dname;
    private String _descirption;
    private String _imagepath;

    public detailinfoClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getDname() {
        return _dname;
    }

    public void setDname(String dname) {
        _dname = dname;
    }

    public String getDescirption() {
        return _descirption;
    }

    public void setDescirption(String descirption) {
        _descirption = descirption;
    }

    public String getImagepath() {
        return _imagepath;
    }

    public void setImagepath(String imagepath) {
        _imagepath = imagepath;
    }

    public detailinfo toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            detailinfo model = new detailinfoClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setTitle(HtmlUtil.escape(getTitle()));
            model.setDname(HtmlUtil.escape(getDname()));
            model.setDescirption(HtmlUtil.escape(getDescirption()));
            model.setImagepath(HtmlUtil.escape(getImagepath()));

            model = (detailinfo) Proxy.newProxyInstance(detailinfo.class.getClassLoader(),
                    new Class[] { detailinfo.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        detailinfoClp clone = new detailinfoClp();

        clone.setId(getId());
        clone.setTitle(getTitle());
        clone.setDname(getDname());
        clone.setDescirption(getDescirption());
        clone.setImagepath(getImagepath());

        return clone;
    }

    public int compareTo(detailinfo detailinfo) {
        int pk = detailinfo.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        detailinfoClp detailinfo = null;

        try {
            detailinfo = (detailinfoClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = detailinfo.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", dname=");
        sb.append(getDname());
        sb.append(", descirption=");
        sb.append(getDescirption());
        sb.append(", imagepath=");
        sb.append(getImagepath());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.detailinfo");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dname</column-name><column-value><![CDATA[");
        sb.append(getDname());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descirption</column-name><column-value><![CDATA[");
        sb.append(getDescirption());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>imagepath</column-name><column-value><![CDATA[");
        sb.append(getImagepath());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
