package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.customerServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.customerServiceSoap
 *
 */
public class customerSoap implements Serializable {
    private int _id;
    private String _organName;
    private String _sector;
    private String _email;
    private String _fixedtele;
    private String _mobiletele;
    private String _fax;
    private String _region;
    private String _zone;
    private String _woreda;
    private String _kebele;
    private String _houseno;
    private String _userIId;

    public customerSoap() {
    }

    public static customerSoap toSoapModel(customer model) {
        customerSoap soapModel = new customerSoap();

        soapModel.setId(model.getId());
        soapModel.setOrganName(model.getOrganName());
        soapModel.setSector(model.getSector());
        soapModel.setEmail(model.getEmail());
        soapModel.setFixedtele(model.getFixedtele());
        soapModel.setMobiletele(model.getMobiletele());
        soapModel.setFax(model.getFax());
        soapModel.setRegion(model.getRegion());
        soapModel.setZone(model.getZone());
        soapModel.setWoreda(model.getWoreda());
        soapModel.setKebele(model.getKebele());
        soapModel.setHouseno(model.getHouseno());
        soapModel.setUserIId(model.getUserIId());

        return soapModel;
    }

    public static customerSoap[] toSoapModels(customer[] models) {
        customerSoap[] soapModels = new customerSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static customerSoap[][] toSoapModels(customer[][] models) {
        customerSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new customerSoap[models.length][models[0].length];
        } else {
            soapModels = new customerSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static customerSoap[] toSoapModels(List<customer> models) {
        List<customerSoap> soapModels = new ArrayList<customerSoap>(models.size());

        for (customer model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new customerSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrganName() {
        return _organName;
    }

    public void setOrganName(String organName) {
        _organName = organName;
    }

    public String getSector() {
        return _sector;
    }

    public void setSector(String sector) {
        _sector = sector;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getFixedtele() {
        return _fixedtele;
    }

    public void setFixedtele(String fixedtele) {
        _fixedtele = fixedtele;
    }

    public String getMobiletele() {
        return _mobiletele;
    }

    public void setMobiletele(String mobiletele) {
        _mobiletele = mobiletele;
    }

    public String getFax() {
        return _fax;
    }

    public void setFax(String fax) {
        _fax = fax;
    }

    public String getRegion() {
        return _region;
    }

    public void setRegion(String region) {
        _region = region;
    }

    public String getZone() {
        return _zone;
    }

    public void setZone(String zone) {
        _zone = zone;
    }

    public String getWoreda() {
        return _woreda;
    }

    public void setWoreda(String woreda) {
        _woreda = woreda;
    }

    public String getKebele() {
        return _kebele;
    }

    public void setKebele(String kebele) {
        _kebele = kebele;
    }

    public String getHouseno() {
        return _houseno;
    }

    public void setHouseno(String houseno) {
        _houseno = houseno;
    }

    public String getUserIId() {
        return _userIId;
    }

    public void setUserIId(String userIId) {
        _userIId = userIId;
    }
}
