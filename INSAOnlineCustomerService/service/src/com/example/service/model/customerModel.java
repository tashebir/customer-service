package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="customerModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>customer</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customer
 * @see com.example.service.model.impl.customerImpl
 * @see com.example.service.model.impl.customerModelImpl
 *
 */
public interface customerModel extends BaseModel<customer> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getId();

    public void setId(int id);

    public String getOrganName();

    public void setOrganName(String organName);

    public String getSector();

    public void setSector(String sector);

    public String getEmail();

    public void setEmail(String email);

    public String getFixedtele();

    public void setFixedtele(String fixedtele);

    public String getMobiletele();

    public void setMobiletele(String mobiletele);

    public String getFax();

    public void setFax(String fax);

    public String getRegion();

    public void setRegion(String region);

    public String getZone();

    public void setZone(String zone);

    public String getWoreda();

    public void setWoreda(String woreda);

    public String getKebele();

    public void setKebele(String kebele);

    public String getHouseno();

    public void setHouseno(String houseno);

    public String getUserIId();

    public void setUserIId(String userIId);

    public customer toEscapedModel();
}
