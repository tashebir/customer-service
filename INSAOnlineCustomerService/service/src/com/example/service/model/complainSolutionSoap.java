package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="complainSolutionSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.complainSolutionServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.complainSolutionServiceSoap
 *
 */
public class complainSolutionSoap implements Serializable {
    private int _solutionId;
    private String _solutionSubject;
    private String _solutionDetail;
    private String _solutionDate;
    private String _complainId;
    private String _subprojectId;

    public complainSolutionSoap() {
    }

    public static complainSolutionSoap toSoapModel(complainSolution model) {
        complainSolutionSoap soapModel = new complainSolutionSoap();

        soapModel.setSolutionId(model.getSolutionId());
        soapModel.setSolutionSubject(model.getSolutionSubject());
        soapModel.setSolutionDetail(model.getSolutionDetail());
        soapModel.setSolutionDate(model.getSolutionDate());
        soapModel.setComplainId(model.getComplainId());
        soapModel.setSubprojectId(model.getSubprojectId());

        return soapModel;
    }

    public static complainSolutionSoap[] toSoapModels(complainSolution[] models) {
        complainSolutionSoap[] soapModels = new complainSolutionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static complainSolutionSoap[][] toSoapModels(
        complainSolution[][] models) {
        complainSolutionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new complainSolutionSoap[models.length][models[0].length];
        } else {
            soapModels = new complainSolutionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static complainSolutionSoap[] toSoapModels(
        List<complainSolution> models) {
        List<complainSolutionSoap> soapModels = new ArrayList<complainSolutionSoap>(models.size());

        for (complainSolution model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new complainSolutionSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _solutionId;
    }

    public void setPrimaryKey(int pk) {
        setSolutionId(pk);
    }

    public int getSolutionId() {
        return _solutionId;
    }

    public void setSolutionId(int solutionId) {
        _solutionId = solutionId;
    }

    public String getSolutionSubject() {
        return _solutionSubject;
    }

    public void setSolutionSubject(String solutionSubject) {
        _solutionSubject = solutionSubject;
    }

    public String getSolutionDetail() {
        return _solutionDetail;
    }

    public void setSolutionDetail(String solutionDetail) {
        _solutionDetail = solutionDetail;
    }

    public String getSolutionDate() {
        return _solutionDate;
    }

    public void setSolutionDate(String solutionDate) {
        _solutionDate = solutionDate;
    }

    public String getComplainId() {
        return _complainId;
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getSubprojectId() {
        return _subprojectId;
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }
}
