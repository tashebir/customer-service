package com.example.service.model;

import com.liferay.portal.model.BaseModel;


/**
 * <a href="complainSolutionModel.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface is a model that represents the <code>complainSolution</code>
 * table in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.complainSolution
 * @see com.example.service.model.impl.complainSolutionImpl
 * @see com.example.service.model.impl.complainSolutionModelImpl
 *
 */
public interface complainSolutionModel extends BaseModel<complainSolution> {
    public int getPrimaryKey();

    public void setPrimaryKey(int pk);

    public int getSolutionId();

    public void setSolutionId(int solutionId);

    public String getSolutionSubject();

    public void setSolutionSubject(String solutionSubject);

    public String getSolutionDetail();

    public void setSolutionDetail(String solutionDetail);

    public String getSolutionDate();

    public void setSolutionDate(String solutionDate);

    public String getComplainId();

    public void setComplainId(String complainId);

    public String getSubprojectId();

    public void setSubprojectId(String subprojectId);

    public complainSolution toEscapedModel();
}
