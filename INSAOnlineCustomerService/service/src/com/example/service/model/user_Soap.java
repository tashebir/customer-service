package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="user_Soap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.user_ServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.user_ServiceSoap
 *
 */
public class user_Soap implements Serializable {
    private int _userId;
    private String _screenName;
    private String _emailAddress;
    private String _jobTitle;
    private String _uuid_;

    public user_Soap() {
    }

    public static user_Soap toSoapModel(user_ model) {
        user_Soap soapModel = new user_Soap();

        soapModel.setUserId(model.getUserId());
        soapModel.setScreenName(model.getScreenName());
        soapModel.setEmailAddress(model.getEmailAddress());
        soapModel.setJobTitle(model.getJobTitle());
        soapModel.setUuid_(model.getUuid_());

        return soapModel;
    }

    public static user_Soap[] toSoapModels(user_[] models) {
        user_Soap[] soapModels = new user_Soap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static user_Soap[][] toSoapModels(user_[][] models) {
        user_Soap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new user_Soap[models.length][models[0].length];
        } else {
            soapModels = new user_Soap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static user_Soap[] toSoapModels(List<user_> models) {
        List<user_Soap> soapModels = new ArrayList<user_Soap>(models.size());

        for (user_ model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new user_Soap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _userId;
    }

    public void setPrimaryKey(int pk) {
        setUserId(pk);
    }

    public int getUserId() {
        return _userId;
    }

    public void setUserId(int userId) {
        _userId = userId;
    }

    public String getScreenName() {
        return _screenName;
    }

    public void setScreenName(String screenName) {
        _screenName = screenName;
    }

    public String getEmailAddress() {
        return _emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        _emailAddress = emailAddress;
    }

    public String getJobTitle() {
        return _jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        _jobTitle = jobTitle;
    }

    public String getUuid_() {
        return _uuid_;
    }

    public void setUuid_(String uuid_) {
        _uuid_ = uuid_;
    }
}
