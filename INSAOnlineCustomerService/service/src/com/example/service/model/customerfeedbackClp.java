package com.example.service.model;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;


public class customerfeedbackClp extends BaseModelImpl<customerfeedback>
    implements customerfeedback {
    private int _id;
    private String _orgname;
    private String _customerName;
    private String _email;
    private String _urfeedback;
    private String _projectid;

    public customerfeedbackClp() {
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrgname() {
        return _orgname;
    }

    public void setOrgname(String orgname) {
        _orgname = orgname;
    }

    public String getCustomerName() {
        return _customerName;
    }

    public void setCustomerName(String customerName) {
        _customerName = customerName;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getUrfeedback() {
        return _urfeedback;
    }

    public void setUrfeedback(String urfeedback) {
        _urfeedback = urfeedback;
    }

    public String getProjectid() {
        return _projectid;
    }

    public void setProjectid(String projectid) {
        _projectid = projectid;
    }

    public customerfeedback toEscapedModel() {
        if (isEscapedModel()) {
            return this;
        } else {
            customerfeedback model = new customerfeedbackClp();

            model.setEscapedModel(true);

            model.setId(getId());
            model.setOrgname(HtmlUtil.escape(getOrgname()));
            model.setCustomerName(HtmlUtil.escape(getCustomerName()));
            model.setEmail(HtmlUtil.escape(getEmail()));
            model.setUrfeedback(HtmlUtil.escape(getUrfeedback()));
            model.setProjectid(HtmlUtil.escape(getProjectid()));

            model = (customerfeedback) Proxy.newProxyInstance(customerfeedback.class.getClassLoader(),
                    new Class[] { customerfeedback.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customerfeedbackClp clone = new customerfeedbackClp();

        clone.setId(getId());
        clone.setOrgname(getOrgname());
        clone.setCustomerName(getCustomerName());
        clone.setEmail(getEmail());
        clone.setUrfeedback(getUrfeedback());
        clone.setProjectid(getProjectid());

        return clone;
    }

    public int compareTo(customerfeedback customerfeedback) {
        int pk = customerfeedback.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customerfeedbackClp customerfeedback = null;

        try {
            customerfeedback = (customerfeedbackClp) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customerfeedback.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", orgname=");
        sb.append(getOrgname());
        sb.append(", customerName=");
        sb.append(getCustomerName());
        sb.append(", email=");
        sb.append(getEmail());
        sb.append(", urfeedback=");
        sb.append(getUrfeedback());
        sb.append(", projectid=");
        sb.append(getProjectid());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customerfeedback");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>orgname</column-name><column-value><![CDATA[");
        sb.append(getOrgname());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>customerName</column-name><column-value><![CDATA[");
        sb.append(getCustomerName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>email</column-name><column-value><![CDATA[");
        sb.append(getEmail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>urfeedback</column-name><column-value><![CDATA[");
        sb.append(getUrfeedback());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectid</column-name><column-value><![CDATA[");
        sb.append(getProjectid());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
