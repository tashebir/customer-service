package com.example.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="detailinfoSoap.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.example.service.service.http.detailinfoServiceSoap</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.http.detailinfoServiceSoap
 *
 */
public class detailinfoSoap implements Serializable {
    private int _id;
    private String _title;
    private String _dname;
    private String _descirption;
    private String _imagepath;

    public detailinfoSoap() {
    }

    public static detailinfoSoap toSoapModel(detailinfo model) {
        detailinfoSoap soapModel = new detailinfoSoap();

        soapModel.setId(model.getId());
        soapModel.setTitle(model.getTitle());
        soapModel.setDname(model.getDname());
        soapModel.setDescirption(model.getDescirption());
        soapModel.setImagepath(model.getImagepath());

        return soapModel;
    }

    public static detailinfoSoap[] toSoapModels(detailinfo[] models) {
        detailinfoSoap[] soapModels = new detailinfoSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static detailinfoSoap[][] toSoapModels(detailinfo[][] models) {
        detailinfoSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new detailinfoSoap[models.length][models[0].length];
        } else {
            soapModels = new detailinfoSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static detailinfoSoap[] toSoapModels(List<detailinfo> models) {
        List<detailinfoSoap> soapModels = new ArrayList<detailinfoSoap>(models.size());

        for (detailinfo model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new detailinfoSoap[soapModels.size()]);
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getDname() {
        return _dname;
    }

    public void setDname(String dname) {
        _dname = dname;
    }

    public String getDescirption() {
        return _descirption;
    }

    public void setDescirption(String descirption) {
        _descirption = descirption;
    }

    public String getImagepath() {
        return _imagepath;
    }

    public void setImagepath(String imagepath) {
        _imagepath = imagepath;
    }
}
