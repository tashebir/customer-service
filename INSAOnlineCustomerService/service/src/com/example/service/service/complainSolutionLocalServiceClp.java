package com.example.service.service;

import com.liferay.portal.kernel.util.BooleanWrapper;
import com.liferay.portal.kernel.util.ClassLoaderProxy;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.NullWrapper;


public class complainSolutionLocalServiceClp
    implements complainSolutionLocalService {
    private ClassLoaderProxy _classLoaderProxy;

    public complainSolutionLocalServiceClp(ClassLoaderProxy classLoaderProxy) {
        _classLoaderProxy = classLoaderProxy;
    }

    public com.example.service.model.complainSolution addcomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(complainSolution);

        if (complainSolution == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.complainSolution");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("addcomplainSolution",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.complainSolution) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.complainSolution createcomplainSolution(
        int solutionId) {
        Object paramObj0 = new IntegerWrapper(solutionId);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("createcomplainSolution",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.complainSolution) ClpSerializer.translateOutput(returnObj);
    }

    public void deletecomplainSolution(int solutionId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(solutionId);

        try {
            _classLoaderProxy.invoke("deletecomplainSolution",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public void deletecomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(complainSolution);

        if (complainSolution == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.complainSolution");
        }

        try {
            _classLoaderProxy.invoke("deletecomplainSolution",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object paramObj1 = new IntegerWrapper(start);

        Object paramObj2 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0, paramObj1, paramObj2 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.complainSolution getcomplainSolution(
        int solutionId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(solutionId);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getcomplainSolution",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.complainSolution) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.example.service.model.complainSolution> getcomplainSolutions(
        int start, int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(start);

        Object paramObj1 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getcomplainSolutions",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.example.service.model.complainSolution>) ClpSerializer.translateOutput(returnObj);
    }

    public int getcomplainSolutionsCount()
        throws com.liferay.portal.SystemException {
        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getcomplainSolutionsCount",
                    new Object[0]);
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
    }

    public com.example.service.model.complainSolution updatecomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(complainSolution);

        if (complainSolution == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.complainSolution");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatecomplainSolution",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.complainSolution) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.complainSolution updatecomplainSolution(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(complainSolution);

        if (complainSolution == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.complainSolution");
        }

        Object paramObj1 = new BooleanWrapper(merge);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatecomplainSolution",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.complainSolution) ClpSerializer.translateOutput(returnObj);
    }
}
