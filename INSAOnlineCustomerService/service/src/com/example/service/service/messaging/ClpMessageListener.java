package com.example.service.service.messaging;

import com.example.service.service.ClpSerializer;
import com.example.service.service.complainReportLocalServiceUtil;
import com.example.service.service.complainReportServiceUtil;
import com.example.service.service.complainSolutionLocalServiceUtil;
import com.example.service.service.complainSolutionServiceUtil;
import com.example.service.service.customerLocalServiceUtil;
import com.example.service.service.customerServiceUtil;
import com.example.service.service.customercomplainLocalServiceUtil;
import com.example.service.service.customercomplainServiceUtil;
import com.example.service.service.customerfeedbackLocalServiceUtil;
import com.example.service.service.customerfeedbackServiceUtil;
import com.example.service.service.customerprojectLocalServiceUtil;
import com.example.service.service.customerprojectServiceUtil;
import com.example.service.service.detailinfoLocalServiceUtil;
import com.example.service.service.detailinfoServiceUtil;
import com.example.service.service.projectactivityLocalServiceUtil;
import com.example.service.service.projectactivityServiceUtil;
import com.example.service.service.projectstatusLocalServiceUtil;
import com.example.service.service.projectstatusServiceUtil;
import com.example.service.service.subprojectLocalServiceUtil;
import com.example.service.service.subprojectServiceUtil;
import com.example.service.service.subprojectactivityplanLocalServiceUtil;
import com.example.service.service.subprojectactivityplanServiceUtil;
import com.example.service.service.user_LocalServiceUtil;
import com.example.service.service.user_ServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;


public class ClpMessageListener implements MessageListener {
    public static final String SERVLET_CONTEXT_NAME = ClpSerializer.SERVLET_CONTEXT_NAME;
    private static Log _log = LogFactoryUtil.getLog(ClpMessageListener.class);

    public void receive(Message message) {
        try {
            doReceive(message);
        } catch (Exception e) {
            _log.error("Unable to process message " + message, e);
        }
    }

    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(SERVLET_CONTEXT_NAME)) {
            customerprojectLocalServiceUtil.clearService();

            customerprojectServiceUtil.clearService();
            customerLocalServiceUtil.clearService();

            customerServiceUtil.clearService();
            subprojectactivityplanLocalServiceUtil.clearService();

            subprojectactivityplanServiceUtil.clearService();
            user_LocalServiceUtil.clearService();

            user_ServiceUtil.clearService();
            subprojectLocalServiceUtil.clearService();

            subprojectServiceUtil.clearService();
            customercomplainLocalServiceUtil.clearService();

            customercomplainServiceUtil.clearService();
            projectstatusLocalServiceUtil.clearService();

            projectstatusServiceUtil.clearService();
            projectactivityLocalServiceUtil.clearService();

            projectactivityServiceUtil.clearService();
            complainSolutionLocalServiceUtil.clearService();

            complainSolutionServiceUtil.clearService();
            customerfeedbackLocalServiceUtil.clearService();

            customerfeedbackServiceUtil.clearService();
            detailinfoLocalServiceUtil.clearService();

            detailinfoServiceUtil.clearService();
            complainReportLocalServiceUtil.clearService();

            complainReportServiceUtil.clearService();
        }
    }
}
