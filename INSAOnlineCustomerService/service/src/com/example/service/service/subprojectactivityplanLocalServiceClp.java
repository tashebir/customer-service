package com.example.service.service;

import com.liferay.portal.kernel.util.BooleanWrapper;
import com.liferay.portal.kernel.util.ClassLoaderProxy;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.NullWrapper;


public class subprojectactivityplanLocalServiceClp
    implements subprojectactivityplanLocalService {
    private ClassLoaderProxy _classLoaderProxy;

    public subprojectactivityplanLocalServiceClp(
        ClassLoaderProxy classLoaderProxy) {
        _classLoaderProxy = classLoaderProxy;
    }

    public com.example.service.model.subprojectactivityplan addsubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subprojectactivityplan);

        if (subprojectactivityplan == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.subprojectactivityplan");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("addsubprojectactivityplan",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subprojectactivityplan) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subprojectactivityplan createsubprojectactivityplan(
        int id) {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("createsubprojectactivityplan",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subprojectactivityplan) ClpSerializer.translateOutput(returnObj);
    }

    public void deletesubprojectactivityplan(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        try {
            _classLoaderProxy.invoke("deletesubprojectactivityplan",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public void deletesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subprojectactivityplan);

        if (subprojectactivityplan == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.subprojectactivityplan");
        }

        try {
            _classLoaderProxy.invoke("deletesubprojectactivityplan",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object paramObj1 = new IntegerWrapper(start);

        Object paramObj2 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0, paramObj1, paramObj2 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subprojectactivityplan getsubprojectactivityplan(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubprojectactivityplan",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subprojectactivityplan) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.example.service.model.subprojectactivityplan> getsubprojectactivityplans(
        int start, int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(start);

        Object paramObj1 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubprojectactivityplans",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.example.service.model.subprojectactivityplan>) ClpSerializer.translateOutput(returnObj);
    }

    public int getsubprojectactivityplansCount()
        throws com.liferay.portal.SystemException {
        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubprojectactivityplansCount",
                    new Object[0]);
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
    }

    public com.example.service.model.subprojectactivityplan updatesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subprojectactivityplan);

        if (subprojectactivityplan == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.subprojectactivityplan");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatesubprojectactivityplan",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subprojectactivityplan) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subprojectactivityplan updatesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan,
        boolean merge) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subprojectactivityplan);

        if (subprojectactivityplan == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.subprojectactivityplan");
        }

        Object paramObj1 = new BooleanWrapper(merge);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatesubprojectactivityplan",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subprojectactivityplan) ClpSerializer.translateOutput(returnObj);
    }
}
