package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="customercomplainLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.customercomplainLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.customercomplainLocalService
 *
 */
public class customercomplainLocalServiceUtil {
    private static customercomplainLocalService _service;

    public static com.example.service.model.customercomplain addcustomercomplain(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException {
        return getService().addcustomercomplain(customercomplain);
    }

    public static com.example.service.model.customercomplain createcustomercomplain(
        int id) {
        return getService().createcustomercomplain(id);
    }

    public static void deletecustomercomplain(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecustomercomplain(id);
    }

    public static void deletecustomercomplain(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException {
        getService().deletecustomercomplain(customercomplain);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.customercomplain getcustomercomplain(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcustomercomplain(id);
    }

    public static java.util.List<com.example.service.model.customercomplain> getcustomercomplains(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcustomercomplains(start, end);
    }

    public static int getcustomercomplainsCount()
        throws com.liferay.portal.SystemException {
        return getService().getcustomercomplainsCount();
    }

    public static com.example.service.model.customercomplain updatecustomercomplain(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomercomplain(customercomplain);
    }

    public static com.example.service.model.customercomplain updatecustomercomplain(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws com.liferay.portal.SystemException {
        return getService().updatecustomercomplain(customercomplain, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static customercomplainLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    customercomplainLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new customercomplainLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(customercomplainLocalService service) {
        _service = service;
    }
}
