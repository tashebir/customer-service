package com.example.service.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="subprojectLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>com.example.service.service.impl.subprojectLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.subprojectLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface subprojectLocalService {
    public com.example.service.model.subproject addsubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.subproject createsubproject(int id);

    public void deletesubproject(int id)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deletesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.example.service.model.subproject getsubproject(int id)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.example.service.model.subproject> getsubprojects(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getsubprojectsCount() throws com.liferay.portal.SystemException;

    public com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject, boolean merge)
        throws com.liferay.portal.SystemException;
}
