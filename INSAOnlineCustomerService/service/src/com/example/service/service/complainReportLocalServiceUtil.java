package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="complainReportLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.complainReportLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.complainReportLocalService
 *
 */
public class complainReportLocalServiceUtil {
    private static complainReportLocalService _service;

    public static com.example.service.model.complainReport addcomplainReport(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException {
        return getService().addcomplainReport(complainReport);
    }

    public static com.example.service.model.complainReport createcomplainReport(
        int id) {
        return getService().createcomplainReport(id);
    }

    public static void deletecomplainReport(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecomplainReport(id);
    }

    public static void deletecomplainReport(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException {
        getService().deletecomplainReport(complainReport);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.complainReport getcomplainReport(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcomplainReport(id);
    }

    public static java.util.List<com.example.service.model.complainReport> getcomplainReports(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcomplainReports(start, end);
    }

    public static int getcomplainReportsCount()
        throws com.liferay.portal.SystemException {
        return getService().getcomplainReportsCount();
    }

    public static com.example.service.model.complainReport updatecomplainReport(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException {
        return getService().updatecomplainReport(complainReport);
    }

    public static com.example.service.model.complainReport updatecomplainReport(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatecomplainReport(complainReport, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static complainReportLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    complainReportLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new complainReportLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(complainReportLocalService service) {
        _service = service;
    }
}
