package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="user_LocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.user_LocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.user_LocalService
 *
 */
public class user_LocalServiceUtil {
    private static user_LocalService _service;

    public static com.example.service.model.user_ adduser_(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException {
        return getService().adduser_(user_);
    }

    public static com.example.service.model.user_ createuser_(int userId) {
        return getService().createuser_(userId);
    }

    public static void deleteuser_(int userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteuser_(userId);
    }

    public static void deleteuser_(com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException {
        getService().deleteuser_(user_);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.user_ getuser_(int userId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getuser_(userId);
    }

    public static java.util.List<com.example.service.model.user_> getuser_s(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getuser_s(start, end);
    }

    public static int getuser_sCount()
        throws com.liferay.portal.SystemException {
        return getService().getuser_sCount();
    }

    public static com.example.service.model.user_ updateuser_(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException {
        return getService().updateuser_(user_);
    }

    public static com.example.service.model.user_ updateuser_(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateuser_(user_, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static user_LocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    user_LocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new user_LocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(user_LocalService service) {
        _service = service;
    }
}
