package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="detailinfoLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.detailinfoLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.detailinfoLocalService
 *
 */
public class detailinfoLocalServiceUtil {
    private static detailinfoLocalService _service;

    public static com.example.service.model.detailinfo adddetailinfo(
        com.example.service.model.detailinfo detailinfo)
        throws com.liferay.portal.SystemException {
        return getService().adddetailinfo(detailinfo);
    }

    public static com.example.service.model.detailinfo createdetailinfo(int id) {
        return getService().createdetailinfo(id);
    }

    public static void deletedetailinfo(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletedetailinfo(id);
    }

    public static void deletedetailinfo(
        com.example.service.model.detailinfo detailinfo)
        throws com.liferay.portal.SystemException {
        getService().deletedetailinfo(detailinfo);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.detailinfo getdetailinfo(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getdetailinfo(id);
    }

    public static java.util.List<com.example.service.model.detailinfo> getdetailinfos(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getdetailinfos(start, end);
    }

    public static int getdetailinfosCount()
        throws com.liferay.portal.SystemException {
        return getService().getdetailinfosCount();
    }

    public static com.example.service.model.detailinfo updatedetailinfo(
        com.example.service.model.detailinfo detailinfo)
        throws com.liferay.portal.SystemException {
        return getService().updatedetailinfo(detailinfo);
    }

    public static com.example.service.model.detailinfo updatedetailinfo(
        com.example.service.model.detailinfo detailinfo, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatedetailinfo(detailinfo, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static detailinfoLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    detailinfoLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new detailinfoLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(detailinfoLocalService service) {
        _service = service;
    }
}
