package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="complainSolutionLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.complainSolutionLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.complainSolutionLocalService
 *
 */
public class complainSolutionLocalServiceUtil {
    private static complainSolutionLocalService _service;

    public static com.example.service.model.complainSolution addcomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        return getService().addcomplainSolution(complainSolution);
    }

    public static com.example.service.model.complainSolution createcomplainSolution(
        int solutionId) {
        return getService().createcomplainSolution(solutionId);
    }

    public static void deletecomplainSolution(int solutionId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecomplainSolution(solutionId);
    }

    public static void deletecomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        getService().deletecomplainSolution(complainSolution);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.complainSolution getcomplainSolution(
        int solutionId)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcomplainSolution(solutionId);
    }

    public static java.util.List<com.example.service.model.complainSolution> getcomplainSolutions(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcomplainSolutions(start, end);
    }

    public static int getcomplainSolutionsCount()
        throws com.liferay.portal.SystemException {
        return getService().getcomplainSolutionsCount();
    }

    public static com.example.service.model.complainSolution updatecomplainSolution(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        return getService().updatecomplainSolution(complainSolution);
    }

    public static com.example.service.model.complainSolution updatecomplainSolution(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException {
        return getService().updatecomplainSolution(complainSolution, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static complainSolutionLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    complainSolutionLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new complainSolutionLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(complainSolutionLocalService service) {
        _service = service;
    }
}
