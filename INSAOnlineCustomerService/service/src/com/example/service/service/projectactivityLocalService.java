package com.example.service.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="projectactivityLocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>com.example.service.service.impl.projectactivityLocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.projectactivityLocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface projectactivityLocalService {
    public com.example.service.model.projectactivity addprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.projectactivity createprojectactivity(
        int id);

    public void deleteprojectactivity(int id)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deleteprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.example.service.model.projectactivity getprojectactivity(int id)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.example.service.model.projectactivity> getprojectactivities(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getprojectactivitiesCount()
        throws com.liferay.portal.SystemException;

    public com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws com.liferay.portal.SystemException;
}
