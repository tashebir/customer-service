package com.example.service.service;

import com.liferay.portal.kernel.util.BooleanWrapper;
import com.liferay.portal.kernel.util.ClassLoaderProxy;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.NullWrapper;


public class subprojectLocalServiceClp implements subprojectLocalService {
    private ClassLoaderProxy _classLoaderProxy;

    public subprojectLocalServiceClp(ClassLoaderProxy classLoaderProxy) {
        _classLoaderProxy = classLoaderProxy;
    }

    public com.example.service.model.subproject addsubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subproject);

        if (subproject == null) {
            paramObj0 = new NullWrapper("com.example.service.model.subproject");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("addsubproject",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subproject) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subproject createsubproject(int id) {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("createsubproject",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subproject) ClpSerializer.translateOutput(returnObj);
    }

    public void deletesubproject(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        try {
            _classLoaderProxy.invoke("deletesubproject",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public void deletesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subproject);

        if (subproject == null) {
            paramObj0 = new NullWrapper("com.example.service.model.subproject");
        }

        try {
            _classLoaderProxy.invoke("deletesubproject",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object paramObj1 = new IntegerWrapper(start);

        Object paramObj2 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0, paramObj1, paramObj2 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subproject getsubproject(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubproject",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subproject) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.example.service.model.subproject> getsubprojects(
        int start, int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(start);

        Object paramObj1 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubprojects",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.example.service.model.subproject>) ClpSerializer.translateOutput(returnObj);
    }

    public int getsubprojectsCount() throws com.liferay.portal.SystemException {
        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getsubprojectsCount",
                    new Object[0]);
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
    }

    public com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subproject);

        if (subproject == null) {
            paramObj0 = new NullWrapper("com.example.service.model.subproject");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatesubproject",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subproject) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject, boolean merge)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(subproject);

        if (subproject == null) {
            paramObj0 = new NullWrapper("com.example.service.model.subproject");
        }

        Object paramObj1 = new BooleanWrapper(merge);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updatesubproject",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.subproject) ClpSerializer.translateOutput(returnObj);
    }
}
