package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="projectactivityLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.projectactivityLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.projectactivityLocalService
 *
 */
public class projectactivityLocalServiceUtil {
    private static projectactivityLocalService _service;

    public static com.example.service.model.projectactivity addprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        return getService().addprojectactivity(projectactivity);
    }

    public static com.example.service.model.projectactivity createprojectactivity(
        int id) {
        return getService().createprojectactivity(id);
    }

    public static void deleteprojectactivity(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteprojectactivity(id);
    }

    public static void deleteprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        getService().deleteprojectactivity(projectactivity);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.projectactivity getprojectactivity(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getprojectactivity(id);
    }

    public static java.util.List<com.example.service.model.projectactivity> getprojectactivities(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getprojectactivities(start, end);
    }

    public static int getprojectactivitiesCount()
        throws com.liferay.portal.SystemException {
        return getService().getprojectactivitiesCount();
    }

    public static com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        return getService().updateprojectactivity(projectactivity);
    }

    public static com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateprojectactivity(projectactivity, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static projectactivityLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    projectactivityLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new projectactivityLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(projectactivityLocalService service) {
        _service = service;
    }
}
