package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="subprojectactivityplanLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.subprojectactivityplanLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.subprojectactivityplanLocalService
 *
 */
public class subprojectactivityplanLocalServiceUtil {
    private static subprojectactivityplanLocalService _service;

    public static com.example.service.model.subprojectactivityplan addsubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        return getService().addsubprojectactivityplan(subprojectactivityplan);
    }

    public static com.example.service.model.subprojectactivityplan createsubprojectactivityplan(
        int id) {
        return getService().createsubprojectactivityplan(id);
    }

    public static void deletesubprojectactivityplan(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletesubprojectactivityplan(id);
    }

    public static void deletesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        getService().deletesubprojectactivityplan(subprojectactivityplan);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.subprojectactivityplan getsubprojectactivityplan(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getsubprojectactivityplan(id);
    }

    public static java.util.List<com.example.service.model.subprojectactivityplan> getsubprojectactivityplans(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getsubprojectactivityplans(start, end);
    }

    public static int getsubprojectactivityplansCount()
        throws com.liferay.portal.SystemException {
        return getService().getsubprojectactivityplansCount();
    }

    public static com.example.service.model.subprojectactivityplan updatesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        return getService().updatesubprojectactivityplan(subprojectactivityplan);
    }

    public static com.example.service.model.subprojectactivityplan updatesubprojectactivityplan(
        com.example.service.model.subprojectactivityplan subprojectactivityplan,
        boolean merge) throws com.liferay.portal.SystemException {
        return getService()
                   .updatesubprojectactivityplan(subprojectactivityplan, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static subprojectactivityplanLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    subprojectactivityplanLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new subprojectactivityplanLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(subprojectactivityplanLocalService service) {
        _service = service;
    }
}
