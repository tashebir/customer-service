package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="customerfeedbackLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.customerfeedbackLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.customerfeedbackLocalService
 *
 */
public class customerfeedbackLocalServiceUtil {
    private static customerfeedbackLocalService _service;

    public static com.example.service.model.customerfeedback addcustomerfeedback(
        com.example.service.model.customerfeedback customerfeedback)
        throws com.liferay.portal.SystemException {
        return getService().addcustomerfeedback(customerfeedback);
    }

    public static com.example.service.model.customerfeedback createcustomerfeedback(
        int id) {
        return getService().createcustomerfeedback(id);
    }

    public static void deletecustomerfeedback(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecustomerfeedback(id);
    }

    public static void deletecustomerfeedback(
        com.example.service.model.customerfeedback customerfeedback)
        throws com.liferay.portal.SystemException {
        getService().deletecustomerfeedback(customerfeedback);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.customerfeedback getcustomerfeedback(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcustomerfeedback(id);
    }

    public static java.util.List<com.example.service.model.customerfeedback> getcustomerfeedbacks(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcustomerfeedbacks(start, end);
    }

    public static int getcustomerfeedbacksCount()
        throws com.liferay.portal.SystemException {
        return getService().getcustomerfeedbacksCount();
    }

    public static com.example.service.model.customerfeedback updatecustomerfeedback(
        com.example.service.model.customerfeedback customerfeedback)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomerfeedback(customerfeedback);
    }

    public static com.example.service.model.customerfeedback updatecustomerfeedback(
        com.example.service.model.customerfeedback customerfeedback,
        boolean merge) throws com.liferay.portal.SystemException {
        return getService().updatecustomerfeedback(customerfeedback, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static customerfeedbackLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    customerfeedbackLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new customerfeedbackLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(customerfeedbackLocalService service) {
        _service = service;
    }
}
