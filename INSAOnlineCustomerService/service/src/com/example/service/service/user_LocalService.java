package com.example.service.service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;


/**
 * <a href="user_LocalService.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This interface defines the service. The default implementation is
 * <code>com.example.service.service.impl.user_LocalServiceImpl</code>.
 * Modify methods in that class and rerun ServiceBuilder to populate this class
 * and all other generated classes.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.user_LocalServiceUtil
 *
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface user_LocalService {
    public com.example.service.model.user_ adduser_(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.user_ createuser_(int userId);

    public void deleteuser_(int userId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    public void deleteuser_(com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.example.service.model.user_ getuser_(int userId)
        throws com.liferay.portal.SystemException,
            com.liferay.portal.PortalException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.example.service.model.user_> getuser_s(
        int start, int end) throws com.liferay.portal.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getuser_sCount() throws com.liferay.portal.SystemException;

    public com.example.service.model.user_ updateuser_(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.user_ updateuser_(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException;
}
