package com.example.service.service;

import com.example.service.model.complainReportClp;
import com.example.service.model.complainSolutionClp;
import com.example.service.model.customerClp;
import com.example.service.model.customercomplainClp;
import com.example.service.model.customerfeedbackClp;
import com.example.service.model.customerprojectClp;
import com.example.service.model.detailinfoClp;
import com.example.service.model.projectactivityClp;
import com.example.service.model.projectstatusClp;
import com.example.service.model.subprojectClp;
import com.example.service.model.subprojectactivityplanClp;
import com.example.service.model.user_Clp;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModel;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;


public class ClpSerializer {
    public static final String SERVLET_CONTEXT_NAME = "INSAOnlineCustomerService";
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static ClassLoader _classLoader;

    public static void setClassLoader(ClassLoader classLoader) {
        _classLoader = classLoader;
    }

    public static Object translateInput(BaseModel oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(customerprojectClp.class.getName())) {
            customerprojectClp oldCplModel = (customerprojectClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.customerprojectImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setProjectId",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getProjectId();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setProjectName",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getProjectName();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setStartDate",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getStartDate();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setEndDate",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getEndDate();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setUserIId",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getUserIId();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setProjectManagerId",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getProjectManagerId();

                    method6.invoke(newModel, value6);

                    Method method7 = newModelClass.getMethod("setOrgid",
                            new Class[] { Integer.TYPE });

                    Integer value7 = new Integer(oldCplModel.getOrgid());

                    method7.invoke(newModel, value7);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(customerClp.class.getName())) {
            customerClp oldCplModel = (customerClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.customerImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setOrganName",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getOrganName();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setSector",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getSector();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setEmail",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getEmail();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setFixedtele",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getFixedtele();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setMobiletele",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getMobiletele();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setFax",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getFax();

                    method6.invoke(newModel, value6);

                    Method method7 = newModelClass.getMethod("setRegion",
                            new Class[] { String.class });

                    String value7 = oldCplModel.getRegion();

                    method7.invoke(newModel, value7);

                    Method method8 = newModelClass.getMethod("setZone",
                            new Class[] { String.class });

                    String value8 = oldCplModel.getZone();

                    method8.invoke(newModel, value8);

                    Method method9 = newModelClass.getMethod("setWoreda",
                            new Class[] { String.class });

                    String value9 = oldCplModel.getWoreda();

                    method9.invoke(newModel, value9);

                    Method method10 = newModelClass.getMethod("setKebele",
                            new Class[] { String.class });

                    String value10 = oldCplModel.getKebele();

                    method10.invoke(newModel, value10);

                    Method method11 = newModelClass.getMethod("setHouseno",
                            new Class[] { String.class });

                    String value11 = oldCplModel.getHouseno();

                    method11.invoke(newModel, value11);

                    Method method12 = newModelClass.getMethod("setUserIId",
                            new Class[] { String.class });

                    String value12 = oldCplModel.getUserIId();

                    method12.invoke(newModel, value12);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(subprojectactivityplanClp.class.getName())) {
            subprojectactivityplanClp oldCplModel = (subprojectactivityplanClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.subprojectactivityplanImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setSubprojectId",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getSubprojectId();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setActivityType",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getActivityType();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setStart_date",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getStart_date();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setEnd_date",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getEnd_date();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setDuration",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getDuration();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setDescription",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getDescription();

                    method6.invoke(newModel, value6);

                    Method method7 = newModelClass.getMethod("setMore",
                            new Class[] { String.class });

                    String value7 = oldCplModel.getMore();

                    method7.invoke(newModel, value7);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(user_Clp.class.getName())) {
            user_Clp oldCplModel = (user_Clp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.user_Impl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setUserId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getUserId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setScreenName",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getScreenName();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setEmailAddress",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getEmailAddress();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setJobTitle",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getJobTitle();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setUuid_",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getUuid_();

                    method4.invoke(newModel, value4);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(subprojectClp.class.getName())) {
            subprojectClp oldCplModel = (subprojectClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.subprojectImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setSubproject_id",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getSubproject_id();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setSubproject_name",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getSubproject_name();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setStart_date",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getStart_date();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setEnd_date",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getEnd_date();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setDuration",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getDuration();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setDescription",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getDescription();

                    method6.invoke(newModel, value6);

                    Method method7 = newModelClass.getMethod("setProjectId",
                            new Class[] { String.class });

                    String value7 = oldCplModel.getProjectId();

                    method7.invoke(newModel, value7);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(customercomplainClp.class.getName())) {
            customercomplainClp oldCplModel = (customercomplainClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.customercomplainImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setComplainSubject",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getComplainSubject();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setComplainDetail",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getComplainDetail();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setSubprojectId",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getSubprojectId();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setStatus",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getStatus();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setComplainDate",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getComplainDate();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setLastanswerDate",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getLastanswerDate();

                    method6.invoke(newModel, value6);

                    Method method7 = newModelClass.getMethod("setAssignedTo",
                            new Class[] { String.class });

                    String value7 = oldCplModel.getAssignedTo();

                    method7.invoke(newModel, value7);

                    Method method8 = newModelClass.getMethod("setReIdOf",
                            new Class[] { Integer.TYPE });

                    Integer value8 = new Integer(oldCplModel.getReIdOf());

                    method8.invoke(newModel, value8);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(projectstatusClp.class.getName())) {
            projectstatusClp oldCplModel = (projectstatusClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.projectstatusImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setSubprojectId",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getSubprojectId();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setActivityType",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getActivityType();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setPercentageCompleted",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getPercentageCompleted();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setRemaining",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getRemaining();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setStatusfilldate",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getStatusfilldate();

                    method5.invoke(newModel, value5);

                    Method method6 = newModelClass.getMethod("setUploaddd",
                            new Class[] { String.class });

                    String value6 = oldCplModel.getUploaddd();

                    method6.invoke(newModel, value6);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(projectactivityClp.class.getName())) {
            projectactivityClp oldCplModel = (projectactivityClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.projectactivityImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setActivity",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getActivity();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setDescription",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getDescription();

                    method2.invoke(newModel, value2);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(complainSolutionClp.class.getName())) {
            complainSolutionClp oldCplModel = (complainSolutionClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.complainSolutionImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setSolutionId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getSolutionId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setSolutionSubject",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getSolutionSubject();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setSolutionDetail",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getSolutionDetail();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setSolutionDate",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getSolutionDate();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setComplainId",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getComplainId();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setSubprojectId",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getSubprojectId();

                    method5.invoke(newModel, value5);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(customerfeedbackClp.class.getName())) {
            customerfeedbackClp oldCplModel = (customerfeedbackClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.customerfeedbackImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setOrgname",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getOrgname();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setCustomerName",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getCustomerName();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setEmail",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getEmail();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setUrfeedback",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getUrfeedback();

                    method4.invoke(newModel, value4);

                    Method method5 = newModelClass.getMethod("setProjectid",
                            new Class[] { String.class });

                    String value5 = oldCplModel.getProjectid();

                    method5.invoke(newModel, value5);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(detailinfoClp.class.getName())) {
            detailinfoClp oldCplModel = (detailinfoClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.detailinfoImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setTitle",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getTitle();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setDname",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getDname();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setDescirption",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getDescirption();

                    method3.invoke(newModel, value3);

                    Method method4 = newModelClass.getMethod("setImagepath",
                            new Class[] { String.class });

                    String value4 = oldCplModel.getImagepath();

                    method4.invoke(newModel, value4);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(complainReportClp.class.getName())) {
            complainReportClp oldCplModel = (complainReportClp) oldModel;

            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    Class<?> newModelClass = Class.forName("com.example.service.model.impl.complainReportImpl",
                            true, _classLoader);

                    Object newModel = newModelClass.newInstance();

                    Method method0 = newModelClass.getMethod("setId",
                            new Class[] { Integer.TYPE });

                    Integer value0 = new Integer(oldCplModel.getId());

                    method0.invoke(newModel, value0);

                    Method method1 = newModelClass.getMethod("setComplainType",
                            new Class[] { String.class });

                    String value1 = oldCplModel.getComplainType();

                    method1.invoke(newModel, value1);

                    Method method2 = newModelClass.getMethod("setComplainId",
                            new Class[] { String.class });

                    String value2 = oldCplModel.getComplainId();

                    method2.invoke(newModel, value2);

                    Method method3 = newModelClass.getMethod("setRemark",
                            new Class[] { String.class });

                    String value3 = oldCplModel.getRemark();

                    method3.invoke(newModel, value3);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel) {
            return translateInput((BaseModel) obj);
        } else if (obj instanceof List) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.customerprojectImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    customerprojectClp newModel = new customerprojectClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getProjectId");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setProjectId(value1);

                    Method method2 = oldModelClass.getMethod("getProjectName");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setProjectName(value2);

                    Method method3 = oldModelClass.getMethod("getStartDate");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setStartDate(value3);

                    Method method4 = oldModelClass.getMethod("getEndDate");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEndDate(value4);

                    Method method5 = oldModelClass.getMethod("getUserIId");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUserIId(value5);

                    Method method6 = oldModelClass.getMethod(
                            "getProjectManagerId");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setProjectManagerId(value6);

                    Method method7 = oldModelClass.getMethod("getOrgid");

                    Integer value7 = (Integer) method7.invoke(oldModel,
                            (Object[]) null);

                    newModel.setOrgid(value7.intValue());

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.customerImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    customerClp newModel = new customerClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getOrganName");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setOrganName(value1);

                    Method method2 = oldModelClass.getMethod("getSector");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSector(value2);

                    Method method3 = oldModelClass.getMethod("getEmail");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEmail(value3);

                    Method method4 = oldModelClass.getMethod("getFixedtele");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setFixedtele(value4);

                    Method method5 = oldModelClass.getMethod("getMobiletele");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setMobiletele(value5);

                    Method method6 = oldModelClass.getMethod("getFax");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setFax(value6);

                    Method method7 = oldModelClass.getMethod("getRegion");

                    String value7 = (String) method7.invoke(oldModel,
                            (Object[]) null);

                    newModel.setRegion(value7);

                    Method method8 = oldModelClass.getMethod("getZone");

                    String value8 = (String) method8.invoke(oldModel,
                            (Object[]) null);

                    newModel.setZone(value8);

                    Method method9 = oldModelClass.getMethod("getWoreda");

                    String value9 = (String) method9.invoke(oldModel,
                            (Object[]) null);

                    newModel.setWoreda(value9);

                    Method method10 = oldModelClass.getMethod("getKebele");

                    String value10 = (String) method10.invoke(oldModel,
                            (Object[]) null);

                    newModel.setKebele(value10);

                    Method method11 = oldModelClass.getMethod("getHouseno");

                    String value11 = (String) method11.invoke(oldModel,
                            (Object[]) null);

                    newModel.setHouseno(value11);

                    Method method12 = oldModelClass.getMethod("getUserIId");

                    String value12 = (String) method12.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUserIId(value12);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.subprojectactivityplanImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    subprojectactivityplanClp newModel = new subprojectactivityplanClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getSubprojectId");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubprojectId(value1);

                    Method method2 = oldModelClass.getMethod("getActivityType");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setActivityType(value2);

                    Method method3 = oldModelClass.getMethod("getStart_date");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setStart_date(value3);

                    Method method4 = oldModelClass.getMethod("getEnd_date");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEnd_date(value4);

                    Method method5 = oldModelClass.getMethod("getDuration");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDuration(value5);

                    Method method6 = oldModelClass.getMethod("getDescription");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDescription(value6);

                    Method method7 = oldModelClass.getMethod("getMore");

                    String value7 = (String) method7.invoke(oldModel,
                            (Object[]) null);

                    newModel.setMore(value7);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals("com.example.service.model.impl.user_Impl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    user_Clp newModel = new user_Clp();

                    Method method0 = oldModelClass.getMethod("getUserId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUserId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getScreenName");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setScreenName(value1);

                    Method method2 = oldModelClass.getMethod("getEmailAddress");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEmailAddress(value2);

                    Method method3 = oldModelClass.getMethod("getJobTitle");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setJobTitle(value3);

                    Method method4 = oldModelClass.getMethod("getUuid_");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUuid_(value4);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.subprojectImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    subprojectClp newModel = new subprojectClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getSubproject_id");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubproject_id(value1);

                    Method method2 = oldModelClass.getMethod(
                            "getSubproject_name");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubproject_name(value2);

                    Method method3 = oldModelClass.getMethod("getStart_date");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setStart_date(value3);

                    Method method4 = oldModelClass.getMethod("getEnd_date");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEnd_date(value4);

                    Method method5 = oldModelClass.getMethod("getDuration");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDuration(value5);

                    Method method6 = oldModelClass.getMethod("getDescription");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDescription(value6);

                    Method method7 = oldModelClass.getMethod("getProjectId");

                    String value7 = (String) method7.invoke(oldModel,
                            (Object[]) null);

                    newModel.setProjectId(value7);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.customercomplainImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    customercomplainClp newModel = new customercomplainClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod(
                            "getComplainSubject");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainSubject(value1);

                    Method method2 = oldModelClass.getMethod(
                            "getComplainDetail");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainDetail(value2);

                    Method method3 = oldModelClass.getMethod("getSubprojectId");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubprojectId(value3);

                    Method method4 = oldModelClass.getMethod("getStatus");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setStatus(value4);

                    Method method5 = oldModelClass.getMethod("getComplainDate");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainDate(value5);

                    Method method6 = oldModelClass.getMethod(
                            "getLastanswerDate");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setLastanswerDate(value6);

                    Method method7 = oldModelClass.getMethod("getAssignedTo");

                    String value7 = (String) method7.invoke(oldModel,
                            (Object[]) null);

                    newModel.setAssignedTo(value7);

                    Method method8 = oldModelClass.getMethod("getReIdOf");

                    Integer value8 = (Integer) method8.invoke(oldModel,
                            (Object[]) null);

                    newModel.setReIdOf(value8.intValue());

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.projectstatusImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    projectstatusClp newModel = new projectstatusClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getSubprojectId");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubprojectId(value1);

                    Method method2 = oldModelClass.getMethod("getActivityType");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setActivityType(value2);

                    Method method3 = oldModelClass.getMethod(
                            "getPercentageCompleted");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setPercentageCompleted(value3);

                    Method method4 = oldModelClass.getMethod("getRemaining");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setRemaining(value4);

                    Method method5 = oldModelClass.getMethod(
                            "getStatusfilldate");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setStatusfilldate(value5);

                    Method method6 = oldModelClass.getMethod("getUploaddd");

                    String value6 = (String) method6.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUploaddd(value6);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.projectactivityImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    projectactivityClp newModel = new projectactivityClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getActivity");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setActivity(value1);

                    Method method2 = oldModelClass.getMethod("getDescription");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDescription(value2);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.complainSolutionImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    complainSolutionClp newModel = new complainSolutionClp();

                    Method method0 = oldModelClass.getMethod("getSolutionId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSolutionId(value0.intValue());

                    Method method1 = oldModelClass.getMethod(
                            "getSolutionSubject");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSolutionSubject(value1);

                    Method method2 = oldModelClass.getMethod(
                            "getSolutionDetail");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSolutionDetail(value2);

                    Method method3 = oldModelClass.getMethod("getSolutionDate");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSolutionDate(value3);

                    Method method4 = oldModelClass.getMethod("getComplainId");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainId(value4);

                    Method method5 = oldModelClass.getMethod("getSubprojectId");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setSubprojectId(value5);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.customerfeedbackImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    customerfeedbackClp newModel = new customerfeedbackClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getOrgname");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setOrgname(value1);

                    Method method2 = oldModelClass.getMethod("getCustomerName");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setCustomerName(value2);

                    Method method3 = oldModelClass.getMethod("getEmail");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setEmail(value3);

                    Method method4 = oldModelClass.getMethod("getUrfeedback");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setUrfeedback(value4);

                    Method method5 = oldModelClass.getMethod("getProjectid");

                    String value5 = (String) method5.invoke(oldModel,
                            (Object[]) null);

                    newModel.setProjectid(value5);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.detailinfoImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    detailinfoClp newModel = new detailinfoClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getTitle");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setTitle(value1);

                    Method method2 = oldModelClass.getMethod("getDname");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDname(value2);

                    Method method3 = oldModelClass.getMethod("getDescirption");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setDescirption(value3);

                    Method method4 = oldModelClass.getMethod("getImagepath");

                    String value4 = (String) method4.invoke(oldModel,
                            (Object[]) null);

                    newModel.setImagepath(value4);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        if (oldModelClassName.equals(
                    "com.example.service.model.impl.complainReportImpl")) {
            ClassLoader contextClassLoader = Thread.currentThread()
                                                   .getContextClassLoader();

            try {
                Thread.currentThread().setContextClassLoader(_classLoader);

                try {
                    complainReportClp newModel = new complainReportClp();

                    Method method0 = oldModelClass.getMethod("getId");

                    Integer value0 = (Integer) method0.invoke(oldModel,
                            (Object[]) null);

                    newModel.setId(value0.intValue());

                    Method method1 = oldModelClass.getMethod("getComplainType");

                    String value1 = (String) method1.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainType(value1);

                    Method method2 = oldModelClass.getMethod("getComplainId");

                    String value2 = (String) method2.invoke(oldModel,
                            (Object[]) null);

                    newModel.setComplainId(value2);

                    Method method3 = oldModelClass.getMethod("getRemark");

                    String value3 = (String) method3.invoke(oldModel,
                            (Object[]) null);

                    newModel.setRemark(value3);

                    return newModel;
                } catch (Exception e) {
                    _log.error(e, e);
                }
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }

        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel) {
            return translateOutput((BaseModel) obj);
        } else if (obj instanceof List) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }
}
