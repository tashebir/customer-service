package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="customerprojectLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.customerprojectLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.customerprojectLocalService
 *
 */
public class customerprojectLocalServiceUtil {
    private static customerprojectLocalService _service;

    public static com.example.service.model.customerproject addcustomerproject(
        com.example.service.model.customerproject customerproject)
        throws com.liferay.portal.SystemException {
        return getService().addcustomerproject(customerproject);
    }

    public static com.example.service.model.customerproject createcustomerproject(
        int id) {
        return getService().createcustomerproject(id);
    }

    public static void deletecustomerproject(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecustomerproject(id);
    }

    public static void deletecustomerproject(
        com.example.service.model.customerproject customerproject)
        throws com.liferay.portal.SystemException {
        getService().deletecustomerproject(customerproject);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.customerproject getcustomerproject(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcustomerproject(id);
    }

    public static java.util.List<com.example.service.model.customerproject> getcustomerprojects(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcustomerprojects(start, end);
    }

    public static int getcustomerprojectsCount()
        throws com.liferay.portal.SystemException {
        return getService().getcustomerprojectsCount();
    }

    public static com.example.service.model.customerproject updatecustomerproject(
        com.example.service.model.customerproject customerproject)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomerproject(customerproject);
    }

    public static com.example.service.model.customerproject updatecustomerproject(
        com.example.service.model.customerproject customerproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomerproject(customerproject, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static customerprojectLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    customerprojectLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new customerprojectLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(customerprojectLocalService service) {
        _service = service;
    }
}
