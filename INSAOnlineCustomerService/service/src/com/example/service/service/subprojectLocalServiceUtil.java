package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="subprojectLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.subprojectLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.subprojectLocalService
 *
 */
public class subprojectLocalServiceUtil {
    private static subprojectLocalService _service;

    public static com.example.service.model.subproject addsubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        return getService().addsubproject(subproject);
    }

    public static com.example.service.model.subproject createsubproject(int id) {
        return getService().createsubproject(id);
    }

    public static void deletesubproject(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletesubproject(id);
    }

    public static void deletesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        getService().deletesubproject(subproject);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.subproject getsubproject(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getsubproject(id);
    }

    public static java.util.List<com.example.service.model.subproject> getsubprojects(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getsubprojects(start, end);
    }

    public static int getsubprojectsCount()
        throws com.liferay.portal.SystemException {
        return getService().getsubprojectsCount();
    }

    public static com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        return getService().updatesubproject(subproject);
    }

    public static com.example.service.model.subproject updatesubproject(
        com.example.service.model.subproject subproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatesubproject(subproject, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static subprojectLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    subprojectLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new subprojectLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(subprojectLocalService service) {
        _service = service;
    }
}
