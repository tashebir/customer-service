package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="customerLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.customerLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.customerLocalService
 *
 */
public class customerLocalServiceUtil {
    private static customerLocalService _service;

    public static com.example.service.model.customer addcustomer(
        com.example.service.model.customer customer)
        throws com.liferay.portal.SystemException {
        return getService().addcustomer(customer);
    }

    public static com.example.service.model.customer createcustomer(int id) {
        return getService().createcustomer(id);
    }

    public static void deletecustomer(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deletecustomer(id);
    }

    public static void deletecustomer(
        com.example.service.model.customer customer)
        throws com.liferay.portal.SystemException {
        getService().deletecustomer(customer);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.customer getcustomer(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getcustomer(id);
    }

    public static java.util.List<com.example.service.model.customer> getcustomers(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getcustomers(start, end);
    }

    public static int getcustomersCount()
        throws com.liferay.portal.SystemException {
        return getService().getcustomersCount();
    }

    public static com.example.service.model.customer updatecustomer(
        com.example.service.model.customer customer)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomer(customer);
    }

    public static com.example.service.model.customer updatecustomer(
        com.example.service.model.customer customer, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updatecustomer(customer, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static customerLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    customerLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new customerLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(customerLocalService service) {
        _service = service;
    }
}
