package com.example.service.service;

import com.liferay.portal.kernel.util.BooleanWrapper;
import com.liferay.portal.kernel.util.ClassLoaderProxy;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.NullWrapper;


public class projectactivityLocalServiceClp
    implements projectactivityLocalService {
    private ClassLoaderProxy _classLoaderProxy;

    public projectactivityLocalServiceClp(ClassLoaderProxy classLoaderProxy) {
        _classLoaderProxy = classLoaderProxy;
    }

    public com.example.service.model.projectactivity addprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(projectactivity);

        if (projectactivity == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.projectactivity");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("addprojectactivity",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.projectactivity) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.projectactivity createprojectactivity(
        int id) {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("createprojectactivity",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.projectactivity) ClpSerializer.translateOutput(returnObj);
    }

    public void deleteprojectactivity(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        try {
            _classLoaderProxy.invoke("deleteprojectactivity",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public void deleteprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(projectactivity);

        if (projectactivity == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.projectactivity");
        }

        try {
            _classLoaderProxy.invoke("deleteprojectactivity",
                new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(dynamicQuery);

        if (dynamicQuery == null) {
            paramObj0 = new NullWrapper(
                    "com.liferay.portal.kernel.dao.orm.DynamicQuery");
        }

        Object paramObj1 = new IntegerWrapper(start);

        Object paramObj2 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("dynamicQuery",
                    new Object[] { paramObj0, paramObj1, paramObj2 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<Object>) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.projectactivity getprojectactivity(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(id);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getprojectactivity",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.PortalException) {
                throw (com.liferay.portal.PortalException) t;
            }

            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.projectactivity) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.example.service.model.projectactivity> getprojectactivities(
        int start, int end) throws com.liferay.portal.SystemException {
        Object paramObj0 = new IntegerWrapper(start);

        Object paramObj1 = new IntegerWrapper(end);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getprojectactivities",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.example.service.model.projectactivity>) ClpSerializer.translateOutput(returnObj);
    }

    public int getprojectactivitiesCount()
        throws com.liferay.portal.SystemException {
        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("getprojectactivitiesCount",
                    new Object[0]);
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
    }

    public com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(projectactivity);

        if (projectactivity == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.projectactivity");
        }

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updateprojectactivity",
                    new Object[] { paramObj0 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.projectactivity) ClpSerializer.translateOutput(returnObj);
    }

    public com.example.service.model.projectactivity updateprojectactivity(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws com.liferay.portal.SystemException {
        Object paramObj0 = ClpSerializer.translateInput(projectactivity);

        if (projectactivity == null) {
            paramObj0 = new NullWrapper(
                    "com.example.service.model.projectactivity");
        }

        Object paramObj1 = new BooleanWrapper(merge);

        Object returnObj = null;

        try {
            returnObj = _classLoaderProxy.invoke("updateprojectactivity",
                    new Object[] { paramObj0, paramObj1 });
        } catch (Throwable t) {
            if (t instanceof com.liferay.portal.SystemException) {
                throw (com.liferay.portal.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.example.service.model.projectactivity) ClpSerializer.translateOutput(returnObj);
    }
}
