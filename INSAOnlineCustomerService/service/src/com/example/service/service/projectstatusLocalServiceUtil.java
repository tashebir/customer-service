package com.example.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;


/**
 * <a href="projectstatusLocalServiceUtil.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class provides static methods for the
 * <code>com.example.service.service.projectstatusLocalService</code>
 * bean. The static methods of this class calls the same methods of the bean
 * instance. It's convenient to be able to just write one line to call a method
 * on a bean instead of writing a lookup call and a method call.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.service.projectstatusLocalService
 *
 */
public class projectstatusLocalServiceUtil {
    private static projectstatusLocalService _service;

    public static com.example.service.model.projectstatus addprojectstatus(
        com.example.service.model.projectstatus projectstatus)
        throws com.liferay.portal.SystemException {
        return getService().addprojectstatus(projectstatus);
    }

    public static com.example.service.model.projectstatus createprojectstatus(
        int id) {
        return getService().createprojectstatus(id);
    }

    public static void deleteprojectstatus(int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        getService().deleteprojectstatus(id);
    }

    public static void deleteprojectstatus(
        com.example.service.model.projectstatus projectstatus)
        throws com.liferay.portal.SystemException {
        getService().deleteprojectstatus(projectstatus);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    public static com.example.service.model.projectstatus getprojectstatus(
        int id)
        throws com.liferay.portal.PortalException,
            com.liferay.portal.SystemException {
        return getService().getprojectstatus(id);
    }

    public static java.util.List<com.example.service.model.projectstatus> getprojectstatuses(
        int start, int end) throws com.liferay.portal.SystemException {
        return getService().getprojectstatuses(start, end);
    }

    public static int getprojectstatusesCount()
        throws com.liferay.portal.SystemException {
        return getService().getprojectstatusesCount();
    }

    public static com.example.service.model.projectstatus updateprojectstatus(
        com.example.service.model.projectstatus projectstatus)
        throws com.liferay.portal.SystemException {
        return getService().updateprojectstatus(projectstatus);
    }

    public static com.example.service.model.projectstatus updateprojectstatus(
        com.example.service.model.projectstatus projectstatus, boolean merge)
        throws com.liferay.portal.SystemException {
        return getService().updateprojectstatus(projectstatus, merge);
    }

    public static void clearService() {
        _service = null;
    }

    public static projectstatusLocalService getService() {
        if (_service == null) {
            Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    projectstatusLocalServiceUtil.class.getName());
            ClassLoader portletClassLoader = (ClassLoader) PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
                    "portletClassLoader");

            ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
                    portletClassLoader);

            _service = new projectstatusLocalServiceClp(classLoaderProxy);

            ClpSerializer.setClassLoader(portletClassLoader);
        }

        return _service;
    }

    public void setService(projectstatusLocalService service) {
        _service = service;
    }
}
