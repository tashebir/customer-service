package com.example.service.service.persistence;

public class customercomplainUtil {
    private static customercomplainPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.customercomplain customercomplain) {
        getPersistence().cacheResult(customercomplain);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.customercomplain> customercomplains) {
        getPersistence().cacheResult(customercomplains);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.customercomplain create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.customercomplain remove(int id)
        throws com.example.service.NoSuchcustomercomplainException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.customercomplain remove(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(customercomplain);
    }

    /**
     * @deprecated Use <code>update(customercomplain customercomplain, boolean merge)</code>.
     */
    public static com.example.service.model.customercomplain update(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(customercomplain);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customercomplain the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customercomplain is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.customercomplain update(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().update(customercomplain, merge);
    }

    public static com.example.service.model.customercomplain updateImpl(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(customercomplain, merge);
    }

    public static com.example.service.model.customercomplain findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchcustomercomplainException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.customercomplain fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.customercomplain> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.customercomplain> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.customercomplain> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static customercomplainPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(customercomplainPersistence persistence) {
        _persistence = persistence;
    }
}
