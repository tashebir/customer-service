package com.example.service.service.persistence;

public class customerprojectUtil {
    private static customerprojectPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.customerproject customerproject) {
        getPersistence().cacheResult(customerproject);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.customerproject> customerprojects) {
        getPersistence().cacheResult(customerprojects);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.customerproject create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.customerproject remove(int id)
        throws com.example.service.NoSuchcustomerprojectException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.customerproject remove(
        com.example.service.model.customerproject customerproject)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(customerproject);
    }

    /**
     * @deprecated Use <code>update(customerproject customerproject, boolean merge)</code>.
     */
    public static com.example.service.model.customerproject update(
        com.example.service.model.customerproject customerproject)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(customerproject);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customerproject the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customerproject is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.customerproject update(
        com.example.service.model.customerproject customerproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(customerproject, merge);
    }

    public static com.example.service.model.customerproject updateImpl(
        com.example.service.model.customerproject customerproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(customerproject, merge);
    }

    public static com.example.service.model.customerproject findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchcustomerprojectException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.customerproject fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.customerproject> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.customerproject> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.customerproject> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static customerprojectPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(customerprojectPersistence persistence) {
        _persistence = persistence;
    }
}
