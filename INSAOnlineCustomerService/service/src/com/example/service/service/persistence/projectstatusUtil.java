package com.example.service.service.persistence;

public class projectstatusUtil {
    private static projectstatusPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.projectstatus projectstatus) {
        getPersistence().cacheResult(projectstatus);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.projectstatus> projectstatuses) {
        getPersistence().cacheResult(projectstatuses);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.projectstatus create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.projectstatus remove(int id)
        throws com.example.service.NoSuchprojectstatusException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.projectstatus remove(
        com.example.service.model.projectstatus projectstatus)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(projectstatus);
    }

    /**
     * @deprecated Use <code>update(projectstatus projectstatus, boolean merge)</code>.
     */
    public static com.example.service.model.projectstatus update(
        com.example.service.model.projectstatus projectstatus)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(projectstatus);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                projectstatus the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when projectstatus is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.projectstatus update(
        com.example.service.model.projectstatus projectstatus, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(projectstatus, merge);
    }

    public static com.example.service.model.projectstatus updateImpl(
        com.example.service.model.projectstatus projectstatus, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(projectstatus, merge);
    }

    public static com.example.service.model.projectstatus findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchprojectstatusException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.projectstatus fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.projectstatus> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.projectstatus> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.projectstatus> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static projectstatusPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(projectstatusPersistence persistence) {
        _persistence = persistence;
    }
}
