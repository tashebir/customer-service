package com.example.service.service.persistence;

public class complainSolutionUtil {
    private static complainSolutionPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.complainSolution complainSolution) {
        getPersistence().cacheResult(complainSolution);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.complainSolution> complainSolutions) {
        getPersistence().cacheResult(complainSolutions);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.complainSolution create(
        int solutionId) {
        return getPersistence().create(solutionId);
    }

    public static com.example.service.model.complainSolution remove(
        int solutionId)
        throws com.example.service.NoSuchcomplainSolutionException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(solutionId);
    }

    public static com.example.service.model.complainSolution remove(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(complainSolution);
    }

    /**
     * @deprecated Use <code>update(complainSolution complainSolution, boolean merge)</code>.
     */
    public static com.example.service.model.complainSolution update(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(complainSolution);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainSolution the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainSolution is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.complainSolution update(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().update(complainSolution, merge);
    }

    public static com.example.service.model.complainSolution updateImpl(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(complainSolution, merge);
    }

    public static com.example.service.model.complainSolution findByPrimaryKey(
        int solutionId)
        throws com.example.service.NoSuchcomplainSolutionException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(solutionId);
    }

    public static com.example.service.model.complainSolution fetchByPrimaryKey(
        int solutionId) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(solutionId);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.complainSolution> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.complainSolution> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.complainSolution> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static complainSolutionPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(complainSolutionPersistence persistence) {
        _persistence = persistence;
    }
}
