package com.example.service.service.persistence;

public class complainReportUtil {
    private static complainReportPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.complainReport complainReport) {
        getPersistence().cacheResult(complainReport);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.complainReport> complainReports) {
        getPersistence().cacheResult(complainReports);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.complainReport create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.complainReport remove(int id)
        throws com.example.service.NoSuchcomplainReportException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.complainReport remove(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(complainReport);
    }

    /**
     * @deprecated Use <code>update(complainReport complainReport, boolean merge)</code>.
     */
    public static com.example.service.model.complainReport update(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(complainReport);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainReport the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainReport is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.complainReport update(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(complainReport, merge);
    }

    public static com.example.service.model.complainReport updateImpl(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(complainReport, merge);
    }

    public static com.example.service.model.complainReport findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchcomplainReportException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.complainReport fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.complainReport> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.complainReport> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.complainReport> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static complainReportPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(complainReportPersistence persistence) {
        _persistence = persistence;
    }
}
