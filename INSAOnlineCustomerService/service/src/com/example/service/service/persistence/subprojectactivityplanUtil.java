package com.example.service.service.persistence;

public class subprojectactivityplanUtil {
    private static subprojectactivityplanPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.subprojectactivityplan subprojectactivityplan) {
        getPersistence().cacheResult(subprojectactivityplan);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.subprojectactivityplan> subprojectactivityplans) {
        getPersistence().cacheResult(subprojectactivityplans);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.subprojectactivityplan create(
        int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.subprojectactivityplan remove(
        int id)
        throws com.example.service.NoSuchsubprojectactivityplanException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.subprojectactivityplan remove(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(subprojectactivityplan);
    }

    /**
     * @deprecated Use <code>update(subprojectactivityplan subprojectactivityplan, boolean merge)</code>.
     */
    public static com.example.service.model.subprojectactivityplan update(
        com.example.service.model.subprojectactivityplan subprojectactivityplan)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(subprojectactivityplan);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                subprojectactivityplan the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when subprojectactivityplan is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.subprojectactivityplan update(
        com.example.service.model.subprojectactivityplan subprojectactivityplan,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().update(subprojectactivityplan, merge);
    }

    public static com.example.service.model.subprojectactivityplan updateImpl(
        com.example.service.model.subprojectactivityplan subprojectactivityplan,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(subprojectactivityplan, merge);
    }

    public static com.example.service.model.subprojectactivityplan findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchsubprojectactivityplanException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.subprojectactivityplan fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.subprojectactivityplan> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.subprojectactivityplan> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.subprojectactivityplan> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static subprojectactivityplanPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(subprojectactivityplanPersistence persistence) {
        _persistence = persistence;
    }
}
