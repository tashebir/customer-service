package com.example.service.service.persistence;

public class projectactivityUtil {
    private static projectactivityPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.projectactivity projectactivity) {
        getPersistence().cacheResult(projectactivity);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.projectactivity> projectactivities) {
        getPersistence().cacheResult(projectactivities);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.projectactivity create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.projectactivity remove(int id)
        throws com.example.service.NoSuchprojectactivityException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.projectactivity remove(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(projectactivity);
    }

    /**
     * @deprecated Use <code>update(projectactivity projectactivity, boolean merge)</code>.
     */
    public static com.example.service.model.projectactivity update(
        com.example.service.model.projectactivity projectactivity)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(projectactivity);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                projectactivity the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when projectactivity is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.projectactivity update(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(projectactivity, merge);
    }

    public static com.example.service.model.projectactivity updateImpl(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(projectactivity, merge);
    }

    public static com.example.service.model.projectactivity findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchprojectactivityException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.projectactivity fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.projectactivity> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.projectactivity> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.projectactivity> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static projectactivityPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(projectactivityPersistence persistence) {
        _persistence = persistence;
    }
}
