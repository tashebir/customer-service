package com.example.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface complainReportPersistence extends BasePersistence {
    public void cacheResult(
        com.example.service.model.complainReport complainReport);

    public void cacheResult(
        java.util.List<com.example.service.model.complainReport> complainReports);

    public void clearCache();

    public com.example.service.model.complainReport create(int id);

    public com.example.service.model.complainReport remove(int id)
        throws com.example.service.NoSuchcomplainReportException,
            com.liferay.portal.SystemException;

    public com.example.service.model.complainReport remove(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(complainReport complainReport, boolean merge)</code>.
     */
    public com.example.service.model.complainReport update(
        com.example.service.model.complainReport complainReport)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainReport the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainReport is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public com.example.service.model.complainReport update(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.complainReport updateImpl(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.complainReport findByPrimaryKey(int id)
        throws com.example.service.NoSuchcomplainReportException,
            com.liferay.portal.SystemException;

    public com.example.service.model.complainReport fetchByPrimaryKey(int id)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainReport> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainReport> findAll(
        int start, int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainReport> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
