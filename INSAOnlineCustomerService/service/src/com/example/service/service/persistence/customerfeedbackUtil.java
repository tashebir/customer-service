package com.example.service.service.persistence;

public class customerfeedbackUtil {
    private static customerfeedbackPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.customerfeedback customerfeedback) {
        getPersistence().cacheResult(customerfeedback);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.customerfeedback> customerfeedbacks) {
        getPersistence().cacheResult(customerfeedbacks);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.customerfeedback create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.customerfeedback remove(int id)
        throws com.example.service.NoSuchcustomerfeedbackException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.customerfeedback remove(
        com.example.service.model.customerfeedback customerfeedback)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(customerfeedback);
    }

    /**
     * @deprecated Use <code>update(customerfeedback customerfeedback, boolean merge)</code>.
     */
    public static com.example.service.model.customerfeedback update(
        com.example.service.model.customerfeedback customerfeedback)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(customerfeedback);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customerfeedback the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customerfeedback is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.customerfeedback update(
        com.example.service.model.customerfeedback customerfeedback,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().update(customerfeedback, merge);
    }

    public static com.example.service.model.customerfeedback updateImpl(
        com.example.service.model.customerfeedback customerfeedback,
        boolean merge) throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(customerfeedback, merge);
    }

    public static com.example.service.model.customerfeedback findByPrimaryKey(
        int id)
        throws com.example.service.NoSuchcustomerfeedbackException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.customerfeedback fetchByPrimaryKey(
        int id) throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.customerfeedback> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.customerfeedback> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.customerfeedback> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static customerfeedbackPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(customerfeedbackPersistence persistence) {
        _persistence = persistence;
    }
}
