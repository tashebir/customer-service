package com.example.service.service.persistence;

public class subprojectUtil {
    private static subprojectPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.subproject subproject) {
        getPersistence().cacheResult(subproject);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.subproject> subprojects) {
        getPersistence().cacheResult(subprojects);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.subproject create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.subproject remove(int id)
        throws com.example.service.NoSuchsubprojectException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.subproject remove(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(subproject);
    }

    /**
     * @deprecated Use <code>update(subproject subproject, boolean merge)</code>.
     */
    public static com.example.service.model.subproject update(
        com.example.service.model.subproject subproject)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(subproject);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                subproject the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when subproject is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.subproject update(
        com.example.service.model.subproject subproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(subproject, merge);
    }

    public static com.example.service.model.subproject updateImpl(
        com.example.service.model.subproject subproject, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(subproject, merge);
    }

    public static com.example.service.model.subproject findByPrimaryKey(int id)
        throws com.example.service.NoSuchsubprojectException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.subproject fetchByPrimaryKey(int id)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.subproject> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.subproject> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.subproject> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static subprojectPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(subprojectPersistence persistence) {
        _persistence = persistence;
    }
}
