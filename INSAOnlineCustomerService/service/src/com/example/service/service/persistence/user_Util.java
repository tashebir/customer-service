package com.example.service.service.persistence;

public class user_Util {
    private static user_Persistence _persistence;

    public static void cacheResult(com.example.service.model.user_ user_) {
        getPersistence().cacheResult(user_);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.user_> user_s) {
        getPersistence().cacheResult(user_s);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.user_ create(int userId) {
        return getPersistence().create(userId);
    }

    public static com.example.service.model.user_ remove(int userId)
        throws com.example.service.NoSuchuser_Exception,
            com.liferay.portal.SystemException {
        return getPersistence().remove(userId);
    }

    public static com.example.service.model.user_ remove(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(user_);
    }

    /**
     * @deprecated Use <code>update(user_ user_, boolean merge)</code>.
     */
    public static com.example.service.model.user_ update(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(user_);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                user_ the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when user_ is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.user_ update(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(user_, merge);
    }

    public static com.example.service.model.user_ updateImpl(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(user_, merge);
    }

    public static com.example.service.model.user_ findByPrimaryKey(int userId)
        throws com.example.service.NoSuchuser_Exception,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(userId);
    }

    public static com.example.service.model.user_ fetchByPrimaryKey(int userId)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(userId);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.user_> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.user_> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.user_> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static user_Persistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(user_Persistence persistence) {
        _persistence = persistence;
    }
}
