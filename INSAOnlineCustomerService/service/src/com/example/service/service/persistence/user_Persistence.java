package com.example.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface user_Persistence extends BasePersistence {
    public void cacheResult(com.example.service.model.user_ user_);

    public void cacheResult(
        java.util.List<com.example.service.model.user_> user_s);

    public void clearCache();

    public com.example.service.model.user_ create(int userId);

    public com.example.service.model.user_ remove(int userId)
        throws com.example.service.NoSuchuser_Exception,
            com.liferay.portal.SystemException;

    public com.example.service.model.user_ remove(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(user_ user_, boolean merge)</code>.
     */
    public com.example.service.model.user_ update(
        com.example.service.model.user_ user_)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                user_ the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when user_ is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public com.example.service.model.user_ update(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.user_ updateImpl(
        com.example.service.model.user_ user_, boolean merge)
        throws com.liferay.portal.SystemException;

    public com.example.service.model.user_ findByPrimaryKey(int userId)
        throws com.example.service.NoSuchuser_Exception,
            com.liferay.portal.SystemException;

    public com.example.service.model.user_ fetchByPrimaryKey(int userId)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.user_> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.user_> findAll(int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.user_> findAll(int start,
        int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
