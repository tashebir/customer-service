package com.example.service.service.persistence;

public class detailinfoUtil {
    private static detailinfoPersistence _persistence;

    public static void cacheResult(
        com.example.service.model.detailinfo detailinfo) {
        getPersistence().cacheResult(detailinfo);
    }

    public static void cacheResult(
        java.util.List<com.example.service.model.detailinfo> detailinfos) {
        getPersistence().cacheResult(detailinfos);
    }

    public static void clearCache() {
        getPersistence().clearCache();
    }

    public static com.example.service.model.detailinfo create(int id) {
        return getPersistence().create(id);
    }

    public static com.example.service.model.detailinfo remove(int id)
        throws com.example.service.NoSuchdetailinfoException,
            com.liferay.portal.SystemException {
        return getPersistence().remove(id);
    }

    public static com.example.service.model.detailinfo remove(
        com.example.service.model.detailinfo detailinfo)
        throws com.liferay.portal.SystemException {
        return getPersistence().remove(detailinfo);
    }

    /**
     * @deprecated Use <code>update(detailinfo detailinfo, boolean merge)</code>.
     */
    public static com.example.service.model.detailinfo update(
        com.example.service.model.detailinfo detailinfo)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(detailinfo);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                detailinfo the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when detailinfo is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public static com.example.service.model.detailinfo update(
        com.example.service.model.detailinfo detailinfo, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().update(detailinfo, merge);
    }

    public static com.example.service.model.detailinfo updateImpl(
        com.example.service.model.detailinfo detailinfo, boolean merge)
        throws com.liferay.portal.SystemException {
        return getPersistence().updateImpl(detailinfo, merge);
    }

    public static com.example.service.model.detailinfo findByPrimaryKey(int id)
        throws com.example.service.NoSuchdetailinfoException,
            com.liferay.portal.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    public static com.example.service.model.detailinfo fetchByPrimaryKey(int id)
        throws com.liferay.portal.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    public static java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    public static java.util.List<com.example.service.model.detailinfo> findAll()
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll();
    }

    public static java.util.List<com.example.service.model.detailinfo> findAll(
        int start, int end) throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end);
    }

    public static java.util.List<com.example.service.model.detailinfo> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException {
        return getPersistence().findAll(start, end, obc);
    }

    public static void removeAll() throws com.liferay.portal.SystemException {
        getPersistence().removeAll();
    }

    public static int countAll() throws com.liferay.portal.SystemException {
        return getPersistence().countAll();
    }

    public static detailinfoPersistence getPersistence() {
        return _persistence;
    }

    public void setPersistence(detailinfoPersistence persistence) {
        _persistence = persistence;
    }
}
