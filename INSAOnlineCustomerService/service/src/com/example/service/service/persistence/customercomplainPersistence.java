package com.example.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface customercomplainPersistence extends BasePersistence {
    public void cacheResult(
        com.example.service.model.customercomplain customercomplain);

    public void cacheResult(
        java.util.List<com.example.service.model.customercomplain> customercomplains);

    public void clearCache();

    public com.example.service.model.customercomplain create(int id);

    public com.example.service.model.customercomplain remove(int id)
        throws com.example.service.NoSuchcustomercomplainException,
            com.liferay.portal.SystemException;

    public com.example.service.model.customercomplain remove(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(customercomplain customercomplain, boolean merge)</code>.
     */
    public com.example.service.model.customercomplain update(
        com.example.service.model.customercomplain customercomplain)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customercomplain the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customercomplain is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public com.example.service.model.customercomplain update(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws com.liferay.portal.SystemException;

    public com.example.service.model.customercomplain updateImpl(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws com.liferay.portal.SystemException;

    public com.example.service.model.customercomplain findByPrimaryKey(int id)
        throws com.example.service.NoSuchcustomercomplainException,
            com.liferay.portal.SystemException;

    public com.example.service.model.customercomplain fetchByPrimaryKey(int id)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.customercomplain> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.customercomplain> findAll(
        int start, int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.customercomplain> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
