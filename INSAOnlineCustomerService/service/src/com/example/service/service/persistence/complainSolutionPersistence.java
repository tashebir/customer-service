package com.example.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;


public interface complainSolutionPersistence extends BasePersistence {
    public void cacheResult(
        com.example.service.model.complainSolution complainSolution);

    public void cacheResult(
        java.util.List<com.example.service.model.complainSolution> complainSolutions);

    public void clearCache();

    public com.example.service.model.complainSolution create(int solutionId);

    public com.example.service.model.complainSolution remove(int solutionId)
        throws com.example.service.NoSuchcomplainSolutionException,
            com.liferay.portal.SystemException;

    public com.example.service.model.complainSolution remove(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException;

    /**
     * @deprecated Use <code>update(complainSolution complainSolution, boolean merge)</code>.
     */
    public com.example.service.model.complainSolution update(
        com.example.service.model.complainSolution complainSolution)
        throws com.liferay.portal.SystemException;

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainSolution the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainSolution is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public com.example.service.model.complainSolution update(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException;

    public com.example.service.model.complainSolution updateImpl(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws com.liferay.portal.SystemException;

    public com.example.service.model.complainSolution findByPrimaryKey(
        int solutionId)
        throws com.example.service.NoSuchcomplainSolutionException,
            com.liferay.portal.SystemException;

    public com.example.service.model.complainSolution fetchByPrimaryKey(
        int solutionId) throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.SystemException;

    public java.util.List<Object> findWithDynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainSolution> findAll()
        throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainSolution> findAll(
        int start, int end) throws com.liferay.portal.SystemException;

    public java.util.List<com.example.service.model.complainSolution> findAll(
        int start, int end, com.liferay.portal.kernel.util.OrderByComparator obc)
        throws com.liferay.portal.SystemException;

    public void removeAll() throws com.liferay.portal.SystemException;

    public int countAll() throws com.liferay.portal.SystemException;
}
