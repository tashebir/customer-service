<%-- 
    Document   : left
    Created on : Mar 31, 2011, 3:49:02 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<link type="text/css" rel="stylesheet" href="/INSAproject/css/sur.css"/>
<link type="text/css" rel="stylesheet" href="/INSAproject/css/styles.css"/>
<script> 
    function displayclicked(res)
    {

    if(res.id=="sp"){


        document.getElementById('spdiv').style.display='block';
        document.getElementById('p25div').style.display='none';
        document.getElementById('p50div').style.display='none';
        document.getElementById('p75div').style.display='none';
        document.getElementById('cpdiv').style.display='none';
        document.getElementById('suspdiv').style.display='none';


    }else if(res.id=="p25")
    {
        document.getElementById('p25div').style.display='block';
        document.getElementById('spdiv').style.display='none';
        document.getElementById('p50div').style.display='none';
        document.getElementById('p75div').style.display='none';
        document.getElementById('cpdiv').style.display='none';
        document.getElementById('suspdiv').style.display='none';

    }
    else if(res.id=="p50")
    {
         document.getElementById('p50div').style.display='block';
         document.getElementById('p25div').style.display='none';
         document.getElementById('spdiv').style.display='none';
         document.getElementById('p75div').style.display='none';
         document.getElementById('cpdiv').style.display='none';
         document.getElementById('suspdiv').style.display='none';

    }
     else if(res.id=="p75")
    {
         document.getElementById('p75div').style.display='block';
         document.getElementById('p25div').style.display='none';
          document.getElementById('spdiv').style.display='none';
         document.getElementById('p50div').style.display='none';
         document.getElementById('cpdiv').style.display='none';
         document.getElementById('suspdiv').style.display='none';

    }
     else if(res.id=="cp")
    {
            document.getElementById('cpdiv').style.display='block';
            document.getElementById('p25div').style.display='none';
            document.getElementById('spdiv').style.display='none';
            document.getElementById('p50div').style.display='none';
            document.getElementById('p75div').style.display='none';
            document.getElementById('suspdiv').style.display='none';


    }
     else if(res.id=="susp")
    {
            document.getElementById('suspdiv').style.display='block';
            document.getElementById('p25div').style.display='none';
            document.getElementById('spdiv').style.display='none';
            document.getElementById('p50div').style.display='none';
            document.getElementById('p75div').style.display='none';
            document.getElementById('cppdiv').style.display='none';

    }
    }
    </script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

    </head>
    <body>
        
           
      <table class="Left-Section" style="background: rgb(236, 243, 248) none repeat scroll 0%; float: left; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; display: block;" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td>
<div id="listview">
					<ul class="ViewsList">

					<li class="LHSViewHeader"><a href="=zz.jsp"><b>List of Projects</b></a></li>

                            		<li><a href="#" id="sp" onclick="displayclicked(this)">Started Projects</a></li>

                            		<li><a href="#" id="p25" onclick="displayclicked(this)">Projects completed above 25% </a></li>


                            		<li><a href="#" id="p50" onclick="displayclicked(this)">Projects completed above 50%</a></li>

                            		<li><a href="#" id="p75" onclick="displayclicked(this)">Projects completed above 75%</a></li>

                            		<li><a href="#" id="cp" onclick="displayclicked(this)">Completed projects</a></li>

                    			    <li><a href="#"id="susp"onclick="displayclicked(this)">Suspended Projects</a></li>

                            	



                 	</ul>
</div>
</td>

</tr>
<tr>
 <td align="left" valign="bottom"><img src="/INSAproject/images/leftblock-bl-curve.gif"></td>

<td class="leftBlockbdrb"><img src="/INSAproject/images/spacer.gif"></td>
<td align="right" valign="bottom"><img src="/INSAproject/images/leftblock-br-curve.gif"></td>
</tr>

</tbody></table>


    </body>
</html>
