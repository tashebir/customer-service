<%-- 
    Document   : projectActivity
    Created on : May 20, 2011, 11:03:34 PM
    Author     : endashaw
--%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="ui"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />
<%PortletPreferences prefs = renderRequest.getPreferences();%>
<link href="/INSAOnlineCustomerService/css/styles.css" type="text/css" rel="stylesheet">
<link href="/INSAOnlineCustomerService/css/sur.css" type="text/css" rel="stylesheet">
<%
if(request.getAttribute("msg")!=null)
    {
  
   // out.println("<p style=\"color:red;padding-left:50px;\">"+request.getAttribute("msg")+"</p>");
    out.println(request.getAttribute("msg"));
    out.println(request.getAttribute("vv"));
      out.println(request.getAttribute("hh"));

    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
 <body>
      <div id="holder">
           <div id="head" align="center" >Header</div>
     <div id="left" align="center">
         wow
     </div>
     <div id="center">
          <form method="post" action="<portlet:actionURL/>">
             <fieldset>
               
                <h2 align="center">Project Activities</h2>
                 <table style="border:1px; border-color:#CCCCCC">
                            
                             <tr><td>&nbsp;&nbsp;&nbsp;</td></tr>
                             <tr><td>Project Name</td><td><input type="text" name="proactname" value="<%=request.getAttribute("projecttranName") %>"</td></tr>
                             <tr><td>Project Id</td><td><input type="text" name="proactid" value="<%=request.getAttribute("projecttranid") %>"</td></tr>
                             <tr><th style="text-align:center;">Activities</th><th style="text-align:center;">Start Date</th><th>&nbsp;&nbsp;&nbsp;</th><th style="text-align:center;">End Date</th></tr>
                             <tr><td style="width:100px;">Project Plan & review</td><td id="subtd"><ui:input-date  dayParam="dspp" monthParam="mspp" yearParam="yspp" yearRangeStart="2011" yearRangeEnd="2030"/></td><th>&nbsp;&nbsp;&nbsp;</th><td id="subtd"><ui:input-date  dayParam="depp" monthParam="mepp" yearParam="yepp" yearRangeStart="2011" yearRangeEnd="2030"/></td></tr>
                             <tr><td style="width:100px;">RAD </td><td  id="subtd"><ui:input-date  dayParam="dsrp" monthParam="msrp" yearParam="ysrp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td id="subtd"><ui:input-date  dayParam="derp" monthParam="merp" yearParam="yerp" yearRangeStart="2011" yearRangeEnd="2030"/></tr>
                             <tr><td style="width:100px;">SDD </td ><td id="subtd"><ui:input-date  dayParam="dssp" monthParam="mssp" yearParam="yssp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td  id="subtd"><ui:input-date  dayParam="desp" monthParam="mesp" yearParam="yesp" yearRangeStart="2011" yearRangeEnd="2030"/></td></tr>
                             <tr><td style="width:100px;">Implementation </td><td id="subtd"><ui:input-date  dayParam="dsip" monthParam="msip" yearParam="ysip" yearRangeStart="2006" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td  id="subtd"><ui:input-date  dayParam="deip" monthParam="meip" yearParam="yeip" yearRangeStart="2006" yearRangeEnd="2030"/></td></tr>
                             <tr><td style="width:100px;">Testing </td><td id="subtd"><ui:input-date  dayParam="dstp" monthParam="mstp" yearParam="ystp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td id="subtd"><ui:input-date  dayParam="detp" monthParam="metp" yearParam="yetp" yearRangeStart="2011" yearRangeEnd="2030"/></tr>
                             <tr><td style="width:100px;">Deployment </td><td  id="subtd"><ui:input-date  dayParam="dsdp" monthParam="msdp" yearParam="ysdp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td id="subtd"><ui:input-date  dayParam="dedp" monthParam="medp" yearParam="yedp" yearRangeStart="2011" yearRangeEnd="2030"/></td></tr>
                             <tr><td style="width:100px;">Maintenance </td><td  id="subtd"><ui:input-date  dayParam="dsmp" monthParam="msmp" yearParam="ysmp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td  id="subtd"><ui:input-date  dayParam="demp" monthParam="memp" yearParam="yemp" yearRangeStart="2011" yearRangeEnd="2030"/></td></tr>
                             <tr><td style="width:100px;">Trainning </td><td  id="subtd"><ui:input-date  dayParam="dstrp" monthParam="mstrp" yearParam="ystrp" yearRangeStart="2011" yearRangeEnd="2030"/></td><td>&nbsp;&nbsp;&nbsp;</td><td id="subtd"><ui:input-date  dayParam="detrp" monthParam="metrp" yearParam="yetrp" yearRangeStart="2011" yearRangeEnd="2030"/></td></tr>

               </table>
              </fieldset>
              <table>
                   <tr><td colspan="3">&nbsp;</td></tr>
                   <tr><td style="width:150px">&nbsp;&nbsp;&nbsp;</td><td style="width:150px"><input type="submit" id="proactsubmit"name="proactsubmit"value="Submit" onclick="document.getElementById('proact').value='proact'"></td><td style="width:10px"><input type="reset"value="Cancel"></td></tr>
                   <input type="hidden" id="proact" name="proact">
              </table>
          </form>
       </div>
     
</div>
    </body>
</html>
