<%-- 
    Document   : complainReport
    Created on : Aug 22, 2011, 5:33:27 PM
    Author     : endashaw
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="javax.portlet.*"%>
<%@page import="com.example.service.model.*" %>
<%@page import="com.example.service.service.*" %>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="javax.portlet.*"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.util.*"%>

<portlet:defineObjects />
<%PortletPreferences prefs = renderRequest.getPreferences();%>
<%
if(request.getAttribute("msg")!=null)
    {
    %>


    <div class="portlet-msg-success">
    <%= request.getAttribute("msg")%></div>
   <%
    }
%>
<script>
    function center1link(idd)
    {

       if(idd.id=='bydate')
       {
            document.getElementById('center1').style.display='block';
            document.getElementById('center2').style.display='none';
            document.getElementById('center3').style.display='none';
            document.getElementById('center4').style.display='none';
            document.getElementById('center5').style.display='none';
       }
       else if(idd.id=='byproma')
       {
            document.getElementById('center1').style.display='none';
            document.getElementById('center2').style.display='block';
            document.getElementById('center3').style.display='none';
            document.getElementById('center4').style.display='none';
            document.getElementById('center5').style.display='none';
       }
        else if(idd.id=='bypro')
       {
            document.getElementById('center1').style.display='none';
            document.getElementById('center2').style.display='none';
            document.getElementById('center3').style.display='block';
            document.getElementById('center4').style.display='none';
            document.getElementById('center5').style.display='none';
       }
       else if(idd.id=='byorg')
       {
            document.getElementById('center1').style.display='none';
            document.getElementById('center2').style.display='none';
            document.getElementById('center3').style.display='none';
            document.getElementById('center4').style.display='block';
            document.getElementById('center5').style.display='none';

       }
       else if(idd.id=='byall')
       {

            document.getElementById('center1').style.display='none';
            document.getElementById('center2').style.display='none';
            document.getElementById('center3').style.display='none';
            document.getElementById('center4').style.display='none';
            document.getElementById('center5').style.display='block';
       }


    }
</script>


       <link href="/INSAOnlineCustomerService/css/styles.css" type="text/css" rel="stylesheet">
        <link href="/INSAOnlineCustomerService/css/sur.css" type="text/css" rel="stylesheet">


    <body>

        <div id="holder">
       <input type="hidden" name="scal" id="scal" value="temp">
    <div id="head" align="center" >Header</div>
    <div id="left"align="center">
            <form name="complainnLink" method="post" action="<portlet:actionURL/>">


         <%
                     ThemeDisplay xxxxx = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);

                                        String uid= Long.toString(xxxxx.getUserId());
                                        out.println(uid);
                                        int usid=Integer.parseInt(uid);;


              %>
        <table >
            <tbody style="border:1px"><tr>
                    <td>
                        <div id="listview">
                           <ul>

     <input type="hidden" id="compcatagory" name="compcatagory">
                                
                               
                                <li style="text-align:left;" class="LHSViewHeader"><a  id="byproma" href="#" onclick="center1link(this);">Complain By Project Manager</a></li>
                                <li style="text-align:left;" class="LHSViewHeader"><a  id="bypro" href="#" onclick="center1link(this);">Complain By Project </a></li>
                                <li style="text-align:left;" class="LHSViewHeader"><a  id="byorg" href="#"onclick="center1link(this);">Complain By Organization </a></li>
                                <li style="text-align:left;" class="LHSViewHeader"><a  id="bydate" href="#"onclick="center1link(this);">Complain By Date </a></li>
                                <li style="text-align:left;" class="LHSViewHeader"><a  id="byall" href="#"onclick="center1link(this);">All Type</a></li>


                            </ul>
                     </div>
                    </td>

                </tr>
                <tr>


                    <td class="leftBlockbdrb"><img src="/INSAproject/images/spacer.gif"></td>
                </tr>

        </tbody></table>
     
            </form>

</div>
<div>
<form name="catagoryform" method="post" action="<portlet:actionURL/>">
    <input type="hidden" name="catagoryText" id="catagoryText">
    <input type="hidden" name="comp" id="comp">
<div id="center1" style="display:block">
 
<fieldset>
    <legend>Complain By Date</legend>
    <br>
        <table>
             <tr>
                <td>Complain Type</td><td> <select id="selectedcomptype" name="selectedcomptype" onchange="document.getElementById('comp').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option>
               <option id="new">new</option>
                <option id="recomplain">recomplain</option>
                <option id="transfered">transfered</option>
                <option id="completed">completed</option>
                <option id="all">All</option></select></td>
            </tr>
             <tr>
                <td>Date/Time</td><td> <select id="selecteddate" name="selecteddate"> <option selected>---- Select ----</option><option>Today</option>
                                                                                                                                <option>This Week</option>
                                                                                                                                <option>This Month</option>
                                                                                                                                <option>Last Three Month</option>
                                                                                                                                <option>This Year</option></select></td>
            </tr>
            <tr>
                <td> <a href="#"style="background-image:url('/INSAOnlineCustomerService/images/arrow-dn.gif')" onclick="document.getElementById('catagoryText').value='catagoryDate';document.forms['catagoryform'].submit();">Go</a> </td>
            </tr>
        </table>
        <br>
</fieldset>
    
</div>
<div id="center2" style="display:none">
       
<fieldset>
    <legend>Complain By Project Manager</legend>
    <br>
        <table>
              <tr>
                <td>Project Manager Name</td><td> <select id="selectedProman" name="selectedProman"> <option selected>---- Select ----</option>
                  <%String pm="ProjectManager";
                                             for(int f=0; f<user_LocalServiceUtil.getuser_sCount();f++)
                                             {
                                                 if(pm.equalsIgnoreCase(user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getJobTitle()))
                                                     {
                                                        out.println("<option id=\""+user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getUserId()+"\""+">"+ user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getScreenName() + "</option>");

                                                     }} %></select></td>
            </tr>
            <tr>
                <td>Complain Type</td><td> <select id="selectedcomptype" name="selectedcomptype"onchange="document.getElementById('comp').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option>
               <option id="new">new</option>
                <option id="recomplain">recomplain</option>
                <option id="transfered">transfered</option>
                <option id="completed">completed</option>
                <option id="all">All</option></select></td>
            </tr>
            <tr>
              <td> <a href="#"style="background-image:url('/INSAOnlineCustomerService/images/arrow-dn.gif')" onclick="document.getElementById('catagoryText').value='catagoryPromanager';document.forms['catagoryform'].submit();">Go</a> </td>
            </tr>

        </table>
   <br>
 </fieldset>
     
</div>
<div id="center3"style="display:none">
      
<fieldset>
    <legend>Complain By Project Title</legend>
    <br>
        <table>
             <tr>
                 <td>Project Title</td><td> <select id="selectedProtitle" name="selectedProtitle" onchange="document.getElementById('proid').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option>
                    <%
                                            for(int z=0;z<customerprojectLocalServiceUtil.getcustomerprojectsCount();z++)
                                            {


                                                        out.println("<option id=\""+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId()+"\""+">"+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName() + "</option>");

                                            }

                                        if(request.getAttribute("projectName")!=null){
                                        %>

                                    <option selected><%= request.getAttribute("projectName") %></option> <% } %>

                                </select> <input type="hidden" id="proid" name="projectIId"</td>
            </tr>
            <tr>
                <td>Complain Type</td><td> <select id="selectedcomptype" name="selectedcomptype"onchange="document.getElementById('comp').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option>
               <option id="new">new</option>
                <option id="recomplain">recomplain</option>
                <option id="transfered">transfered</option>
                <option id="completed">completed</option>
                <option id="all">All</option></select></td>
            </tr>
            <tr>
                <td> <a href="#"style="background-image:url('/INSAOnlineCustomerService/images/arrow-dn.gif')" onclick="document.getElementById('catagoryText').value='catagoryProtitle';document.forms['catagoryform'].submit();">Go</a> </td>
            </tr>

        </table>
    <br>
 </fieldset>
     
</div>

<div id="center4"style="display:none">
      
<fieldset>
    <legend>Complain By Organization</legend>
    <br>
        <table>
             <tr>
                 <td>Complain Type</td><td> <select id="selectedcomptype" name="selectedcomptype"onchange="document.getElementById('comp').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option>
                <option id="new">new</option>
                <option id="recomplain">recomplain</option>
                <option id="transfered">transfered</option>
                <option id="completed">completed</option>
                <option id="all">All</option></select></td>
            </tr>
             <tr>
                <td>Organization Name</td><td> <select id="selectedorgname" name="selectedorgname"onchange="document.getElementById('orgid').value=this.options[selectedIndex].id"> <option selected>---- Select ----</option> <%
                                            for(int z=0;z<customerLocalServiceUtil.getcustomersCount();z++)
                                            {


                                                        out.println("<option id=\""+ customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(z).getId()+"\""+">"+ customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(z).getOrganName() + "</option>");

                                            }

                                        if(request.getAttribute("projectName")!=null){
                                        %>

                                    <option selected><%= request.getAttribute("projectName") %></option> <% } %>

                                </select> <input type="hidden" id="orgid" name="orgname"</td>
            </tr>
            <tr>
              <td> <a href="#" style="background-image:url('/INSAOnlineCustomerService/images/arrow-dn.gif')" onclick="document.getElementById('catagoryText').value='catagoryOrganization';document.forms['catagoryform'].submit();">Go</a> </td>
            </tr>

        </table>
    <br>
</fieldset>
     
</div>
<div id="center5"style="display:none;">
      
<fieldset>
    <legend>Complain By All Types</legend>
    <br>
        <table>
              <tr>
                <td>Project Manager Name</td><td> <select id="selectedProman" name="selectedProman"> <option selected>---- Select ----</option>
                  <%String pmall="ProjectManager";
                                             for(int f=0; f<user_LocalServiceUtil.getuser_sCount();f++)
                                             {
                                                 if(pmall.equalsIgnoreCase(user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getJobTitle()))
                                                     {
                                                        out.println("<option id=\""+user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getUserId()+"\""+">"+ user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(f).getScreenName() + "</option>");

                                                     }} %></select></td>
            </tr>
             <tr>
                <td>Project Title</td><td> <select id="selectedProtitle" name="selectedProtitle"> <option selected>---- Select ----</option>
                    <%
                                            for(int z=0;z<customerprojectLocalServiceUtil.getcustomerprojectsCount();z++)
                                            {

                                                   
                                                        out.println("<option id=\""+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId()+"\""+">"+ customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName() + "</option>");
                                                    
                                            }

                                        if(request.getAttribute("projectName")!=null){
                                        %>

                                    <option selected><%= request.getAttribute("projectName") %></option> <% } %>

                                </select> <input type="hidden" id="proid" name="projectIId"</td>
            </tr>
     
             <tr>
                <td>Complain Type</td><td> <select id="selectedcomptype" name="selectedcomptype"onchange="document.getElementById('comp').value=this.options[selectedIndex].id">
                <option selected>---- Select ----</option>
                <option id="new">new</option>
                <option id="recomplain">recomplain</option>
                <option id="transfered">transfered</option>
                <option id="completed">completed</option>
                <option id="all">All</option></select></td>
            </tr>
             <tr>
                <td>Date/Time</td><td> <select id="selecteddate" name="selecteddate"> <option selected>---- Select ----</option><option></option></select></td>
             </tr>
            <tr>
               <td> <a href="#"style="background-image:url('/INSAOnlineCustomerService/images/arrow-dn.gif')" onclick="document.getElementById('catagoryText').value='catagoryAll';document.forms['catagoryform'].submit();">Go</a> </td>
            </tr>
            
        </table>
     <br>
 </fieldset>
     
</div>
</form>
</div>
<div id="center">
    <br>
    <table>
  <tr>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;<input type="checkbox"> </th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Complain Id</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Complain Subject</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Complain Assigned To</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Complain Status</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Sub Project</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;Complain Date</th>
        <th id="tableheader" height="30" width="100" align="center">&nbsp;&nbsp;&nbsp;View Detail</th>

  </tr>
  <tr>
      <%

        if (request.getAttribute("res" + 0) != null && request.getAttribute("res" + 1) != null && request.getAttribute("res" + 2) != null && request.getAttribute("res" + 3) != null && request.getAttribute("res" + 4) != null && request.getAttribute("res" + 5) != null) {
            request.setAttribute("vv", "has");
            String[] res0 = (String[]) request.getAttribute("res" + 0);
            String[] res1 = (String[]) request.getAttribute("res" + 1);
            String[] res2 = (String[]) request.getAttribute("res" + 2);
            String[] res3 = (String[]) request.getAttribute("res" + 3);
            String[] res4 = (String[]) request.getAttribute("res" + 4);
            String[] res5 = (String[]) request.getAttribute("res" + 5);
            String[] res6 = (String[]) request.getAttribute("res" + 6);


            for (int fre = 0; fre < res0.length; fre++) {
                if (res0[fre] != null && res1[fre] != null && res2[fre] != null && res3[fre] != null && res4[fre] != null) {
                    out.println(res0[fre]);
                    out.println(res1[fre]);
                    out.println(res2[fre]);
                    out.println(res3[fre]);
                    out.println(res4[fre]);
                    out.println(res5[fre]);
                    out.println("<td id='ff'><a href='#' onclick='javascript:viewcomp(this)' id='"+res6[fre]+"'>ViewDetail</a></td></tr>");


                }
            }
        }







      %>
  </tr>
  <tr><td>Hiiiiiiiiiiii</td></tr>
  </table>
</div>
</div>
    </body>

