package com.example.service.model.impl;

import com.example.service.model.customercomplain;
import com.example.service.model.customercomplainSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customercomplainModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>customercomplain</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customercomplain
 * @see com.example.service.model.customercomplainModel
 * @see com.example.service.model.impl.customercomplainImpl
 *
 */
public class customercomplainModelImpl extends BaseModelImpl<customercomplain> {
    public static final String TABLE_NAME = "customercomplain";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "complainSubject", new Integer(Types.VARCHAR) },
            

            { "complainDetail", new Integer(Types.VARCHAR) },
            

            { "subprojectId", new Integer(Types.VARCHAR) },
            

            { "status", new Integer(Types.VARCHAR) },
            

            { "complainDate", new Integer(Types.VARCHAR) },
            

            { "lastanswerDate", new Integer(Types.VARCHAR) },
            

            { "assignedTo", new Integer(Types.VARCHAR) },
            

            { "reIdOf", new Integer(Types.INTEGER) }
        };
    public static final String TABLE_SQL_CREATE = "create table customercomplain (id INTEGER not null primary key,complainSubject VARCHAR(75) null,complainDetail VARCHAR(75) null,subprojectId VARCHAR(75) null,status VARCHAR(75) null,complainDate VARCHAR(75) null,lastanswerDate VARCHAR(75) null,assignedTo VARCHAR(75) null,reIdOf INTEGER)";
    public static final String TABLE_SQL_DROP = "drop table customercomplain";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.customercomplain"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.customercomplain"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.customercomplain"));
    private int _id;
    private String _complainSubject;
    private String _complainDetail;
    private String _subprojectId;
    private String _status;
    private String _complainDate;
    private String _lastanswerDate;
    private String _assignedTo;
    private int _reIdOf;

    public customercomplainModelImpl() {
    }

    public static customercomplain toModel(customercomplainSoap soapModel) {
        customercomplain model = new customercomplainImpl();

        model.setId(soapModel.getId());
        model.setComplainSubject(soapModel.getComplainSubject());
        model.setComplainDetail(soapModel.getComplainDetail());
        model.setSubprojectId(soapModel.getSubprojectId());
        model.setStatus(soapModel.getStatus());
        model.setComplainDate(soapModel.getComplainDate());
        model.setLastanswerDate(soapModel.getLastanswerDate());
        model.setAssignedTo(soapModel.getAssignedTo());
        model.setReIdOf(soapModel.getReIdOf());

        return model;
    }

    public static List<customercomplain> toModels(
        customercomplainSoap[] soapModels) {
        List<customercomplain> models = new ArrayList<customercomplain>(soapModels.length);

        for (customercomplainSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainSubject() {
        return GetterUtil.getString(_complainSubject);
    }

    public void setComplainSubject(String complainSubject) {
        _complainSubject = complainSubject;
    }

    public String getComplainDetail() {
        return GetterUtil.getString(_complainDetail);
    }

    public void setComplainDetail(String complainDetail) {
        _complainDetail = complainDetail;
    }

    public String getSubprojectId() {
        return GetterUtil.getString(_subprojectId);
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getStatus() {
        return GetterUtil.getString(_status);
    }

    public void setStatus(String status) {
        _status = status;
    }

    public String getComplainDate() {
        return GetterUtil.getString(_complainDate);
    }

    public void setComplainDate(String complainDate) {
        _complainDate = complainDate;
    }

    public String getLastanswerDate() {
        return GetterUtil.getString(_lastanswerDate);
    }

    public void setLastanswerDate(String lastanswerDate) {
        _lastanswerDate = lastanswerDate;
    }

    public String getAssignedTo() {
        return GetterUtil.getString(_assignedTo);
    }

    public void setAssignedTo(String assignedTo) {
        _assignedTo = assignedTo;
    }

    public int getReIdOf() {
        return _reIdOf;
    }

    public void setReIdOf(int reIdOf) {
        _reIdOf = reIdOf;
    }

    public customercomplain toEscapedModel() {
        if (isEscapedModel()) {
            return (customercomplain) this;
        } else {
            customercomplain model = new customercomplainImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setComplainSubject(HtmlUtil.escape(getComplainSubject()));
            model.setComplainDetail(HtmlUtil.escape(getComplainDetail()));
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setStatus(HtmlUtil.escape(getStatus()));
            model.setComplainDate(HtmlUtil.escape(getComplainDate()));
            model.setLastanswerDate(HtmlUtil.escape(getLastanswerDate()));
            model.setAssignedTo(HtmlUtil.escape(getAssignedTo()));
            model.setReIdOf(getReIdOf());

            model = (customercomplain) Proxy.newProxyInstance(customercomplain.class.getClassLoader(),
                    new Class[] { customercomplain.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customercomplainImpl clone = new customercomplainImpl();

        clone.setId(getId());
        clone.setComplainSubject(getComplainSubject());
        clone.setComplainDetail(getComplainDetail());
        clone.setSubprojectId(getSubprojectId());
        clone.setStatus(getStatus());
        clone.setComplainDate(getComplainDate());
        clone.setLastanswerDate(getLastanswerDate());
        clone.setAssignedTo(getAssignedTo());
        clone.setReIdOf(getReIdOf());

        return clone;
    }

    public int compareTo(customercomplain customercomplain) {
        int pk = customercomplain.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customercomplain customercomplain = null;

        try {
            customercomplain = (customercomplain) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customercomplain.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", complainSubject=");
        sb.append(getComplainSubject());
        sb.append(", complainDetail=");
        sb.append(getComplainDetail());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", status=");
        sb.append(getStatus());
        sb.append(", complainDate=");
        sb.append(getComplainDate());
        sb.append(", lastanswerDate=");
        sb.append(getLastanswerDate());
        sb.append(", assignedTo=");
        sb.append(getAssignedTo());
        sb.append(", reIdOf=");
        sb.append(getReIdOf());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customercomplain");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainSubject</column-name><column-value><![CDATA[");
        sb.append(getComplainSubject());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainDetail</column-name><column-value><![CDATA[");
        sb.append(getComplainDetail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>status</column-name><column-value><![CDATA[");
        sb.append(getStatus());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainDate</column-name><column-value><![CDATA[");
        sb.append(getComplainDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>lastanswerDate</column-name><column-value><![CDATA[");
        sb.append(getLastanswerDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>assignedTo</column-name><column-value><![CDATA[");
        sb.append(getAssignedTo());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>reIdOf</column-name><column-value><![CDATA[");
        sb.append(getReIdOf());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
