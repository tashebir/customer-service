package com.example.service.model.impl;

import com.example.service.model.customerproject;
import com.example.service.model.customerprojectSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerprojectModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>customerproject</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customerproject
 * @see com.example.service.model.customerprojectModel
 * @see com.example.service.model.impl.customerprojectImpl
 *
 */
public class customerprojectModelImpl extends BaseModelImpl<customerproject> {
    public static final String TABLE_NAME = "customerproject";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "projectId", new Integer(Types.VARCHAR) },
            

            { "projectName", new Integer(Types.VARCHAR) },
            

            { "startDate", new Integer(Types.VARCHAR) },
            

            { "endDate", new Integer(Types.VARCHAR) },
            

            { "userIId", new Integer(Types.VARCHAR) },
            

            { "projectManagerId", new Integer(Types.VARCHAR) },
            

            { "orgid", new Integer(Types.INTEGER) }
        };
    public static final String TABLE_SQL_CREATE = "create table customerproject (id INTEGER not null primary key,projectId VARCHAR(75) null,projectName VARCHAR(75) null,startDate VARCHAR(75) null,endDate VARCHAR(75) null,userIId VARCHAR(75) null,projectManagerId VARCHAR(75) null,orgid INTEGER)";
    public static final String TABLE_SQL_DROP = "drop table customerproject";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.customerproject"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.customerproject"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.customerproject"));
    private int _id;
    private String _projectId;
    private String _projectName;
    private String _startDate;
    private String _endDate;
    private String _userIId;
    private String _projectManagerId;
    private int _orgid;

    public customerprojectModelImpl() {
    }

    public static customerproject toModel(customerprojectSoap soapModel) {
        customerproject model = new customerprojectImpl();

        model.setId(soapModel.getId());
        model.setProjectId(soapModel.getProjectId());
        model.setProjectName(soapModel.getProjectName());
        model.setStartDate(soapModel.getStartDate());
        model.setEndDate(soapModel.getEndDate());
        model.setUserIId(soapModel.getUserIId());
        model.setProjectManagerId(soapModel.getProjectManagerId());
        model.setOrgid(soapModel.getOrgid());

        return model;
    }

    public static List<customerproject> toModels(
        customerprojectSoap[] soapModels) {
        List<customerproject> models = new ArrayList<customerproject>(soapModels.length);

        for (customerprojectSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getProjectId() {
        return GetterUtil.getString(_projectId);
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public String getProjectName() {
        return GetterUtil.getString(_projectName);
    }

    public void setProjectName(String projectName) {
        _projectName = projectName;
    }

    public String getStartDate() {
        return GetterUtil.getString(_startDate);
    }

    public void setStartDate(String startDate) {
        _startDate = startDate;
    }

    public String getEndDate() {
        return GetterUtil.getString(_endDate);
    }

    public void setEndDate(String endDate) {
        _endDate = endDate;
    }

    public String getUserIId() {
        return GetterUtil.getString(_userIId);
    }

    public void setUserIId(String userIId) {
        _userIId = userIId;
    }

    public String getProjectManagerId() {
        return GetterUtil.getString(_projectManagerId);
    }

    public void setProjectManagerId(String projectManagerId) {
        _projectManagerId = projectManagerId;
    }

    public int getOrgid() {
        return _orgid;
    }

    public void setOrgid(int orgid) {
        _orgid = orgid;
    }

    public customerproject toEscapedModel() {
        if (isEscapedModel()) {
            return (customerproject) this;
        } else {
            customerproject model = new customerprojectImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setProjectId(HtmlUtil.escape(getProjectId()));
            model.setProjectName(HtmlUtil.escape(getProjectName()));
            model.setStartDate(HtmlUtil.escape(getStartDate()));
            model.setEndDate(HtmlUtil.escape(getEndDate()));
            model.setUserIId(HtmlUtil.escape(getUserIId()));
            model.setProjectManagerId(HtmlUtil.escape(getProjectManagerId()));
            model.setOrgid(getOrgid());

            model = (customerproject) Proxy.newProxyInstance(customerproject.class.getClassLoader(),
                    new Class[] { customerproject.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customerprojectImpl clone = new customerprojectImpl();

        clone.setId(getId());
        clone.setProjectId(getProjectId());
        clone.setProjectName(getProjectName());
        clone.setStartDate(getStartDate());
        clone.setEndDate(getEndDate());
        clone.setUserIId(getUserIId());
        clone.setProjectManagerId(getProjectManagerId());
        clone.setOrgid(getOrgid());

        return clone;
    }

    public int compareTo(customerproject customerproject) {
        int pk = customerproject.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customerproject customerproject = null;

        try {
            customerproject = (customerproject) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customerproject.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append(", projectName=");
        sb.append(getProjectName());
        sb.append(", startDate=");
        sb.append(getStartDate());
        sb.append(", endDate=");
        sb.append(getEndDate());
        sb.append(", userIId=");
        sb.append(getUserIId());
        sb.append(", projectManagerId=");
        sb.append(getProjectManagerId());
        sb.append(", orgid=");
        sb.append(getOrgid());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customerproject");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectName</column-name><column-value><![CDATA[");
        sb.append(getProjectName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>startDate</column-name><column-value><![CDATA[");
        sb.append(getStartDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>endDate</column-name><column-value><![CDATA[");
        sb.append(getEndDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userIId</column-name><column-value><![CDATA[");
        sb.append(getUserIId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectManagerId</column-name><column-value><![CDATA[");
        sb.append(getProjectManagerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>orgid</column-name><column-value><![CDATA[");
        sb.append(getOrgid());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
