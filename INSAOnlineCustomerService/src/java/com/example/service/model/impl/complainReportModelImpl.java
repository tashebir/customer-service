package com.example.service.model.impl;

import com.example.service.model.complainReport;
import com.example.service.model.complainReportSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="complainReportModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>complainReport</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.complainReport
 * @see com.example.service.model.complainReportModel
 * @see com.example.service.model.impl.complainReportImpl
 *
 */
public class complainReportModelImpl extends BaseModelImpl<complainReport> {
    public static final String TABLE_NAME = "complainReport";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "complainType", new Integer(Types.VARCHAR) },
            

            { "complainId", new Integer(Types.VARCHAR) },
            

            { "remark", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table complainReport (id INTEGER not null primary key,complainType VARCHAR(75) null,complainId VARCHAR(75) null,remark VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table complainReport";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.complainReport"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.complainReport"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.complainReport"));
    private int _id;
    private String _complainType;
    private String _complainId;
    private String _remark;

    public complainReportModelImpl() {
    }

    public static complainReport toModel(complainReportSoap soapModel) {
        complainReport model = new complainReportImpl();

        model.setId(soapModel.getId());
        model.setComplainType(soapModel.getComplainType());
        model.setComplainId(soapModel.getComplainId());
        model.setRemark(soapModel.getRemark());

        return model;
    }

    public static List<complainReport> toModels(complainReportSoap[] soapModels) {
        List<complainReport> models = new ArrayList<complainReport>(soapModels.length);

        for (complainReportSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getComplainType() {
        return GetterUtil.getString(_complainType);
    }

    public void setComplainType(String complainType) {
        _complainType = complainType;
    }

    public String getComplainId() {
        return GetterUtil.getString(_complainId);
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getRemark() {
        return GetterUtil.getString(_remark);
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public complainReport toEscapedModel() {
        if (isEscapedModel()) {
            return (complainReport) this;
        } else {
            complainReport model = new complainReportImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setComplainType(HtmlUtil.escape(getComplainType()));
            model.setComplainId(HtmlUtil.escape(getComplainId()));
            model.setRemark(HtmlUtil.escape(getRemark()));

            model = (complainReport) Proxy.newProxyInstance(complainReport.class.getClassLoader(),
                    new Class[] { complainReport.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        complainReportImpl clone = new complainReportImpl();

        clone.setId(getId());
        clone.setComplainType(getComplainType());
        clone.setComplainId(getComplainId());
        clone.setRemark(getRemark());

        return clone;
    }

    public int compareTo(complainReport complainReport) {
        int pk = complainReport.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        complainReport complainReport = null;

        try {
            complainReport = (complainReport) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = complainReport.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", complainType=");
        sb.append(getComplainType());
        sb.append(", complainId=");
        sb.append(getComplainId());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.complainReport");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainType</column-name><column-value><![CDATA[");
        sb.append(getComplainType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainId</column-name><column-value><![CDATA[");
        sb.append(getComplainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
