package com.example.service.model.impl;

import com.example.service.model.projectstatus;
import com.example.service.model.projectstatusSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="projectstatusModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>projectstatus</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectstatus
 * @see com.example.service.model.projectstatusModel
 * @see com.example.service.model.impl.projectstatusImpl
 *
 */
public class projectstatusModelImpl extends BaseModelImpl<projectstatus> {
    public static final String TABLE_NAME = "projectstatus";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "subprojectId", new Integer(Types.VARCHAR) },
            

            { "activityType", new Integer(Types.VARCHAR) },
            

            { "percentageCompleted", new Integer(Types.VARCHAR) },
            

            { "remaining", new Integer(Types.VARCHAR) },
            

            { "statusfilldate", new Integer(Types.VARCHAR) },
            

            { "uploaddd", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table projectstatus (id INTEGER not null primary key,subprojectId VARCHAR(75) null,activityType VARCHAR(75) null,percentageCompleted VARCHAR(75) null,remaining VARCHAR(75) null,statusfilldate VARCHAR(75) null,uploaddd VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table projectstatus";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.projectstatus"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.projectstatus"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.projectstatus"));
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _percentageCompleted;
    private String _remaining;
    private String _statusfilldate;
    private String _uploaddd;

    public projectstatusModelImpl() {
    }

    public static projectstatus toModel(projectstatusSoap soapModel) {
        projectstatus model = new projectstatusImpl();

        model.setId(soapModel.getId());
        model.setSubprojectId(soapModel.getSubprojectId());
        model.setActivityType(soapModel.getActivityType());
        model.setPercentageCompleted(soapModel.getPercentageCompleted());
        model.setRemaining(soapModel.getRemaining());
        model.setStatusfilldate(soapModel.getStatusfilldate());
        model.setUploaddd(soapModel.getUploaddd());

        return model;
    }

    public static List<projectstatus> toModels(projectstatusSoap[] soapModels) {
        List<projectstatus> models = new ArrayList<projectstatus>(soapModels.length);

        for (projectstatusSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return GetterUtil.getString(_subprojectId);
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return GetterUtil.getString(_activityType);
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getPercentageCompleted() {
        return GetterUtil.getString(_percentageCompleted);
    }

    public void setPercentageCompleted(String percentageCompleted) {
        _percentageCompleted = percentageCompleted;
    }

    public String getRemaining() {
        return GetterUtil.getString(_remaining);
    }

    public void setRemaining(String remaining) {
        _remaining = remaining;
    }

    public String getStatusfilldate() {
        return GetterUtil.getString(_statusfilldate);
    }

    public void setStatusfilldate(String statusfilldate) {
        _statusfilldate = statusfilldate;
    }

    public String getUploaddd() {
        return GetterUtil.getString(_uploaddd);
    }

    public void setUploaddd(String uploaddd) {
        _uploaddd = uploaddd;
    }

    public projectstatus toEscapedModel() {
        if (isEscapedModel()) {
            return (projectstatus) this;
        } else {
            projectstatus model = new projectstatusImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setActivityType(HtmlUtil.escape(getActivityType()));
            model.setPercentageCompleted(HtmlUtil.escape(
                    getPercentageCompleted()));
            model.setRemaining(HtmlUtil.escape(getRemaining()));
            model.setStatusfilldate(HtmlUtil.escape(getStatusfilldate()));
            model.setUploaddd(HtmlUtil.escape(getUploaddd()));

            model = (projectstatus) Proxy.newProxyInstance(projectstatus.class.getClassLoader(),
                    new Class[] { projectstatus.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        projectstatusImpl clone = new projectstatusImpl();

        clone.setId(getId());
        clone.setSubprojectId(getSubprojectId());
        clone.setActivityType(getActivityType());
        clone.setPercentageCompleted(getPercentageCompleted());
        clone.setRemaining(getRemaining());
        clone.setStatusfilldate(getStatusfilldate());
        clone.setUploaddd(getUploaddd());

        return clone;
    }

    public int compareTo(projectstatus projectstatus) {
        int pk = projectstatus.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        projectstatus projectstatus = null;

        try {
            projectstatus = (projectstatus) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = projectstatus.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", activityType=");
        sb.append(getActivityType());
        sb.append(", percentageCompleted=");
        sb.append(getPercentageCompleted());
        sb.append(", remaining=");
        sb.append(getRemaining());
        sb.append(", statusfilldate=");
        sb.append(getStatusfilldate());
        sb.append(", uploaddd=");
        sb.append(getUploaddd());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.projectstatus");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activityType</column-name><column-value><![CDATA[");
        sb.append(getActivityType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>percentageCompleted</column-name><column-value><![CDATA[");
        sb.append(getPercentageCompleted());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remaining</column-name><column-value><![CDATA[");
        sb.append(getRemaining());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>statusfilldate</column-name><column-value><![CDATA[");
        sb.append(getStatusfilldate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>uploaddd</column-name><column-value><![CDATA[");
        sb.append(getUploaddd());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
