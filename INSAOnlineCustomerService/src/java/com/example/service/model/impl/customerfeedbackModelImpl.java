package com.example.service.model.impl;

import com.example.service.model.customerfeedback;
import com.example.service.model.customerfeedbackSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerfeedbackModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>customerfeedback</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customerfeedback
 * @see com.example.service.model.customerfeedbackModel
 * @see com.example.service.model.impl.customerfeedbackImpl
 *
 */
public class customerfeedbackModelImpl extends BaseModelImpl<customerfeedback> {
    public static final String TABLE_NAME = "customerfeedback";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "orgname", new Integer(Types.VARCHAR) },
            

            { "customerName", new Integer(Types.VARCHAR) },
            

            { "email", new Integer(Types.VARCHAR) },
            

            { "urfeedback", new Integer(Types.VARCHAR) },
            

            { "projectid", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table customerfeedback (id INTEGER not null primary key,orgname VARCHAR(75) null,customerName VARCHAR(75) null,email VARCHAR(75) null,urfeedback VARCHAR(75) null,projectid VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table customerfeedback";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.customerfeedback"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.customerfeedback"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.customerfeedback"));
    private int _id;
    private String _orgname;
    private String _customerName;
    private String _email;
    private String _urfeedback;
    private String _projectid;

    public customerfeedbackModelImpl() {
    }

    public static customerfeedback toModel(customerfeedbackSoap soapModel) {
        customerfeedback model = new customerfeedbackImpl();

        model.setId(soapModel.getId());
        model.setOrgname(soapModel.getOrgname());
        model.setCustomerName(soapModel.getCustomerName());
        model.setEmail(soapModel.getEmail());
        model.setUrfeedback(soapModel.getUrfeedback());
        model.setProjectid(soapModel.getProjectid());

        return model;
    }

    public static List<customerfeedback> toModels(
        customerfeedbackSoap[] soapModels) {
        List<customerfeedback> models = new ArrayList<customerfeedback>(soapModels.length);

        for (customerfeedbackSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrgname() {
        return GetterUtil.getString(_orgname);
    }

    public void setOrgname(String orgname) {
        _orgname = orgname;
    }

    public String getCustomerName() {
        return GetterUtil.getString(_customerName);
    }

    public void setCustomerName(String customerName) {
        _customerName = customerName;
    }

    public String getEmail() {
        return GetterUtil.getString(_email);
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getUrfeedback() {
        return GetterUtil.getString(_urfeedback);
    }

    public void setUrfeedback(String urfeedback) {
        _urfeedback = urfeedback;
    }

    public String getProjectid() {
        return GetterUtil.getString(_projectid);
    }

    public void setProjectid(String projectid) {
        _projectid = projectid;
    }

    public customerfeedback toEscapedModel() {
        if (isEscapedModel()) {
            return (customerfeedback) this;
        } else {
            customerfeedback model = new customerfeedbackImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setOrgname(HtmlUtil.escape(getOrgname()));
            model.setCustomerName(HtmlUtil.escape(getCustomerName()));
            model.setEmail(HtmlUtil.escape(getEmail()));
            model.setUrfeedback(HtmlUtil.escape(getUrfeedback()));
            model.setProjectid(HtmlUtil.escape(getProjectid()));

            model = (customerfeedback) Proxy.newProxyInstance(customerfeedback.class.getClassLoader(),
                    new Class[] { customerfeedback.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customerfeedbackImpl clone = new customerfeedbackImpl();

        clone.setId(getId());
        clone.setOrgname(getOrgname());
        clone.setCustomerName(getCustomerName());
        clone.setEmail(getEmail());
        clone.setUrfeedback(getUrfeedback());
        clone.setProjectid(getProjectid());

        return clone;
    }

    public int compareTo(customerfeedback customerfeedback) {
        int pk = customerfeedback.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customerfeedback customerfeedback = null;

        try {
            customerfeedback = (customerfeedback) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customerfeedback.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", orgname=");
        sb.append(getOrgname());
        sb.append(", customerName=");
        sb.append(getCustomerName());
        sb.append(", email=");
        sb.append(getEmail());
        sb.append(", urfeedback=");
        sb.append(getUrfeedback());
        sb.append(", projectid=");
        sb.append(getProjectid());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customerfeedback");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>orgname</column-name><column-value><![CDATA[");
        sb.append(getOrgname());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>customerName</column-name><column-value><![CDATA[");
        sb.append(getCustomerName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>email</column-name><column-value><![CDATA[");
        sb.append(getEmail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>urfeedback</column-name><column-value><![CDATA[");
        sb.append(getUrfeedback());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectid</column-name><column-value><![CDATA[");
        sb.append(getProjectid());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
