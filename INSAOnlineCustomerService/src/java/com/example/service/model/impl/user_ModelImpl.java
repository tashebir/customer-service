package com.example.service.model.impl;

import com.example.service.model.user_;
import com.example.service.model.user_Soap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="user_ModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>user_</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.user_
 * @see com.example.service.model.user_Model
 * @see com.example.service.model.impl.user_Impl
 *
 */
public class user_ModelImpl extends BaseModelImpl<user_> {
    public static final String TABLE_NAME = "user_";
    public static final Object[][] TABLE_COLUMNS = {
            { "userId", new Integer(Types.INTEGER) },
            

            { "screenName", new Integer(Types.VARCHAR) },
            

            { "emailAddress", new Integer(Types.VARCHAR) },
            

            { "jobTitle", new Integer(Types.VARCHAR) },
            

            { "uuid_", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table user_ (userId INTEGER not null primary key,screenName VARCHAR(75) null,emailAddress VARCHAR(75) null,jobTitle VARCHAR(75) null,uuid_ VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table user_";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.user_"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.user_"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.user_"));
    private int _userId;
    private String _screenName;
    private String _emailAddress;
    private String _jobTitle;
    private String _uuid_;

    public user_ModelImpl() {
    }

    public static user_ toModel(user_Soap soapModel) {
        user_ model = new user_Impl();

        model.setUserId(soapModel.getUserId());
        model.setScreenName(soapModel.getScreenName());
        model.setEmailAddress(soapModel.getEmailAddress());
        model.setJobTitle(soapModel.getJobTitle());
        model.setUuid_(soapModel.getUuid_());

        return model;
    }

    public static List<user_> toModels(user_Soap[] soapModels) {
        List<user_> models = new ArrayList<user_>(soapModels.length);

        for (user_Soap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _userId;
    }

    public void setPrimaryKey(int pk) {
        setUserId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_userId);
    }

    public int getUserId() {
        return _userId;
    }

    public void setUserId(int userId) {
        _userId = userId;
    }

    public String getScreenName() {
        return GetterUtil.getString(_screenName);
    }

    public void setScreenName(String screenName) {
        _screenName = screenName;
    }

    public String getEmailAddress() {
        return GetterUtil.getString(_emailAddress);
    }

    public void setEmailAddress(String emailAddress) {
        _emailAddress = emailAddress;
    }

    public String getJobTitle() {
        return GetterUtil.getString(_jobTitle);
    }

    public void setJobTitle(String jobTitle) {
        _jobTitle = jobTitle;
    }

    public String getUuid_() {
        return GetterUtil.getString(_uuid_);
    }

    public void setUuid_(String uuid_) {
        _uuid_ = uuid_;
    }

    public user_ toEscapedModel() {
        if (isEscapedModel()) {
            return (user_) this;
        } else {
            user_ model = new user_Impl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setUserId(getUserId());
            model.setScreenName(HtmlUtil.escape(getScreenName()));
            model.setEmailAddress(HtmlUtil.escape(getEmailAddress()));
            model.setJobTitle(HtmlUtil.escape(getJobTitle()));
            model.setUuid_(HtmlUtil.escape(getUuid_()));

            model = (user_) Proxy.newProxyInstance(user_.class.getClassLoader(),
                    new Class[] { user_.class }, new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        user_Impl clone = new user_Impl();

        clone.setUserId(getUserId());
        clone.setScreenName(getScreenName());
        clone.setEmailAddress(getEmailAddress());
        clone.setJobTitle(getJobTitle());
        clone.setUuid_(getUuid_());

        return clone;
    }

    public int compareTo(user_ user_) {
        int pk = user_.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        user_ user_ = null;

        try {
            user_ = (user_) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = user_.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{userId=");
        sb.append(getUserId());
        sb.append(", screenName=");
        sb.append(getScreenName());
        sb.append(", emailAddress=");
        sb.append(getEmailAddress());
        sb.append(", jobTitle=");
        sb.append(getJobTitle());
        sb.append(", uuid_=");
        sb.append(getUuid_());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.user_");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>screenName</column-name><column-value><![CDATA[");
        sb.append(getScreenName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>emailAddress</column-name><column-value><![CDATA[");
        sb.append(getEmailAddress());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>jobTitle</column-name><column-value><![CDATA[");
        sb.append(getJobTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>uuid_</column-name><column-value><![CDATA[");
        sb.append(getUuid_());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
