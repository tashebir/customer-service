package com.example.service.model.impl;

import com.example.service.model.subprojectactivityplan;
import com.example.service.model.subprojectactivityplanSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="subprojectactivityplanModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>subprojectactivityplan</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.subprojectactivityplan
 * @see com.example.service.model.subprojectactivityplanModel
 * @see com.example.service.model.impl.subprojectactivityplanImpl
 *
 */
public class subprojectactivityplanModelImpl extends BaseModelImpl<subprojectactivityplan> {
    public static final String TABLE_NAME = "subprojectactivityplan";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "subprojectId", new Integer(Types.VARCHAR) },
            

            { "activityType", new Integer(Types.VARCHAR) },
            

            { "start_date", new Integer(Types.VARCHAR) },
            

            { "end_date", new Integer(Types.VARCHAR) },
            

            { "duration", new Integer(Types.VARCHAR) },
            

            { "description", new Integer(Types.VARCHAR) },
            

            { "more", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table subprojectactivityplan (id INTEGER not null primary key,subprojectId VARCHAR(75) null,activityType VARCHAR(75) null,start_date VARCHAR(75) null,end_date VARCHAR(75) null,duration VARCHAR(75) null,description VARCHAR(75) null,more VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table subprojectactivityplan";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.subprojectactivityplan"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.subprojectactivityplan"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.subprojectactivityplan"));
    private int _id;
    private String _subprojectId;
    private String _activityType;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _more;

    public subprojectactivityplanModelImpl() {
    }

    public static subprojectactivityplan toModel(
        subprojectactivityplanSoap soapModel) {
        subprojectactivityplan model = new subprojectactivityplanImpl();

        model.setId(soapModel.getId());
        model.setSubprojectId(soapModel.getSubprojectId());
        model.setActivityType(soapModel.getActivityType());
        model.setStart_date(soapModel.getStart_date());
        model.setEnd_date(soapModel.getEnd_date());
        model.setDuration(soapModel.getDuration());
        model.setDescription(soapModel.getDescription());
        model.setMore(soapModel.getMore());

        return model;
    }

    public static List<subprojectactivityplan> toModels(
        subprojectactivityplanSoap[] soapModels) {
        List<subprojectactivityplan> models = new ArrayList<subprojectactivityplan>(soapModels.length);

        for (subprojectactivityplanSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubprojectId() {
        return GetterUtil.getString(_subprojectId);
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public String getActivityType() {
        return GetterUtil.getString(_activityType);
    }

    public void setActivityType(String activityType) {
        _activityType = activityType;
    }

    public String getStart_date() {
        return GetterUtil.getString(_start_date);
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return GetterUtil.getString(_end_date);
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return GetterUtil.getString(_duration);
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return GetterUtil.getString(_description);
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getMore() {
        return GetterUtil.getString(_more);
    }

    public void setMore(String more) {
        _more = more;
    }

    public subprojectactivityplan toEscapedModel() {
        if (isEscapedModel()) {
            return (subprojectactivityplan) this;
        } else {
            subprojectactivityplan model = new subprojectactivityplanImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));
            model.setActivityType(HtmlUtil.escape(getActivityType()));
            model.setStart_date(HtmlUtil.escape(getStart_date()));
            model.setEnd_date(HtmlUtil.escape(getEnd_date()));
            model.setDuration(HtmlUtil.escape(getDuration()));
            model.setDescription(HtmlUtil.escape(getDescription()));
            model.setMore(HtmlUtil.escape(getMore()));

            model = (subprojectactivityplan) Proxy.newProxyInstance(subprojectactivityplan.class.getClassLoader(),
                    new Class[] { subprojectactivityplan.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        subprojectactivityplanImpl clone = new subprojectactivityplanImpl();

        clone.setId(getId());
        clone.setSubprojectId(getSubprojectId());
        clone.setActivityType(getActivityType());
        clone.setStart_date(getStart_date());
        clone.setEnd_date(getEnd_date());
        clone.setDuration(getDuration());
        clone.setDescription(getDescription());
        clone.setMore(getMore());

        return clone;
    }

    public int compareTo(subprojectactivityplan subprojectactivityplan) {
        int pk = subprojectactivityplan.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        subprojectactivityplan subprojectactivityplan = null;

        try {
            subprojectactivityplan = (subprojectactivityplan) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = subprojectactivityplan.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append(", activityType=");
        sb.append(getActivityType());
        sb.append(", start_date=");
        sb.append(getStart_date());
        sb.append(", end_date=");
        sb.append(getEnd_date());
        sb.append(", duration=");
        sb.append(getDuration());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", more=");
        sb.append(getMore());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.subprojectactivityplan");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activityType</column-name><column-value><![CDATA[");
        sb.append(getActivityType());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>start_date</column-name><column-value><![CDATA[");
        sb.append(getStart_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>end_date</column-name><column-value><![CDATA[");
        sb.append(getEnd_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>duration</column-name><column-value><![CDATA[");
        sb.append(getDuration());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>more</column-name><column-value><![CDATA[");
        sb.append(getMore());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
