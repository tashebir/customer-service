package com.example.service.model.impl;

import com.example.service.model.detailinfo;
import com.example.service.model.detailinfoSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="detailinfoModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>detailinfo</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.detailinfo
 * @see com.example.service.model.detailinfoModel
 * @see com.example.service.model.impl.detailinfoImpl
 *
 */
public class detailinfoModelImpl extends BaseModelImpl<detailinfo> {
    public static final String TABLE_NAME = "detailinfo";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "title", new Integer(Types.VARCHAR) },
            

            { "dname", new Integer(Types.VARCHAR) },
            

            { "description", new Integer(Types.VARCHAR) },
            

            { "imagepath", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table detailinfo (id INTEGER not null primary key,title VARCHAR(75) null,dname VARCHAR(75) null,description VARCHAR(75) null,imagepath VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table detailinfo";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.detailinfo"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.detailinfo"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.detailinfo"));
    private int _id;
    private String _title;
    private String _dname;
    private String _descirption;
    private String _imagepath;

    public detailinfoModelImpl() {
    }

    public static detailinfo toModel(detailinfoSoap soapModel) {
        detailinfo model = new detailinfoImpl();

        model.setId(soapModel.getId());
        model.setTitle(soapModel.getTitle());
        model.setDname(soapModel.getDname());
        model.setDescirption(soapModel.getDescirption());
        model.setImagepath(soapModel.getImagepath());

        return model;
    }

    public static List<detailinfo> toModels(detailinfoSoap[] soapModels) {
        List<detailinfo> models = new ArrayList<detailinfo>(soapModels.length);

        for (detailinfoSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getTitle() {
        return GetterUtil.getString(_title);
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getDname() {
        return GetterUtil.getString(_dname);
    }

    public void setDname(String dname) {
        _dname = dname;
    }

    public String getDescirption() {
        return GetterUtil.getString(_descirption);
    }

    public void setDescirption(String descirption) {
        _descirption = descirption;
    }

    public String getImagepath() {
        return GetterUtil.getString(_imagepath);
    }

    public void setImagepath(String imagepath) {
        _imagepath = imagepath;
    }

    public detailinfo toEscapedModel() {
        if (isEscapedModel()) {
            return (detailinfo) this;
        } else {
            detailinfo model = new detailinfoImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setTitle(HtmlUtil.escape(getTitle()));
            model.setDname(HtmlUtil.escape(getDname()));
            model.setDescirption(HtmlUtil.escape(getDescirption()));
            model.setImagepath(HtmlUtil.escape(getImagepath()));

            model = (detailinfo) Proxy.newProxyInstance(detailinfo.class.getClassLoader(),
                    new Class[] { detailinfo.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        detailinfoImpl clone = new detailinfoImpl();

        clone.setId(getId());
        clone.setTitle(getTitle());
        clone.setDname(getDname());
        clone.setDescirption(getDescirption());
        clone.setImagepath(getImagepath());

        return clone;
    }

    public int compareTo(detailinfo detailinfo) {
        int pk = detailinfo.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        detailinfo detailinfo = null;

        try {
            detailinfo = (detailinfo) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = detailinfo.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", dname=");
        sb.append(getDname());
        sb.append(", descirption=");
        sb.append(getDescirption());
        sb.append(", imagepath=");
        sb.append(getImagepath());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.detailinfo");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dname</column-name><column-value><![CDATA[");
        sb.append(getDname());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>descirption</column-name><column-value><![CDATA[");
        sb.append(getDescirption());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>imagepath</column-name><column-value><![CDATA[");
        sb.append(getImagepath());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
