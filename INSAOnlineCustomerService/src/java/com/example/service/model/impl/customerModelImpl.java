package com.example.service.model.impl;

import com.example.service.model.customer;
import com.example.service.model.customerSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="customerModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>customer</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.customer
 * @see com.example.service.model.customerModel
 * @see com.example.service.model.impl.customerImpl
 *
 */
public class customerModelImpl extends BaseModelImpl<customer> {
    public static final String TABLE_NAME = "customer";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "organName", new Integer(Types.VARCHAR) },
            

            { "sector", new Integer(Types.VARCHAR) },
            

            { "email", new Integer(Types.VARCHAR) },
            

            { "fixedtele", new Integer(Types.VARCHAR) },
            

            { "mobiletele", new Integer(Types.VARCHAR) },
            

            { "fax", new Integer(Types.VARCHAR) },
            

            { "region", new Integer(Types.VARCHAR) },
            

            { "zone", new Integer(Types.VARCHAR) },
            

            { "woreda", new Integer(Types.VARCHAR) },
            

            { "kebele", new Integer(Types.VARCHAR) },
            

            { "houseno", new Integer(Types.VARCHAR) },
            

            { "userIId", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table customer (id INTEGER not null primary key,organName VARCHAR(75) null,sector VARCHAR(75) null,email VARCHAR(75) null,fixedtele VARCHAR(75) null,mobiletele VARCHAR(75) null,fax VARCHAR(75) null,region VARCHAR(75) null,zone VARCHAR(75) null,woreda VARCHAR(75) null,kebele VARCHAR(75) null,houseno VARCHAR(75) null,userIId VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table customer";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.customer"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.customer"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.customer"));
    private int _id;
    private String _organName;
    private String _sector;
    private String _email;
    private String _fixedtele;
    private String _mobiletele;
    private String _fax;
    private String _region;
    private String _zone;
    private String _woreda;
    private String _kebele;
    private String _houseno;
    private String _userIId;

    public customerModelImpl() {
    }

    public static customer toModel(customerSoap soapModel) {
        customer model = new customerImpl();

        model.setId(soapModel.getId());
        model.setOrganName(soapModel.getOrganName());
        model.setSector(soapModel.getSector());
        model.setEmail(soapModel.getEmail());
        model.setFixedtele(soapModel.getFixedtele());
        model.setMobiletele(soapModel.getMobiletele());
        model.setFax(soapModel.getFax());
        model.setRegion(soapModel.getRegion());
        model.setZone(soapModel.getZone());
        model.setWoreda(soapModel.getWoreda());
        model.setKebele(soapModel.getKebele());
        model.setHouseno(soapModel.getHouseno());
        model.setUserIId(soapModel.getUserIId());

        return model;
    }

    public static List<customer> toModels(customerSoap[] soapModels) {
        List<customer> models = new ArrayList<customer>(soapModels.length);

        for (customerSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getOrganName() {
        return GetterUtil.getString(_organName);
    }

    public void setOrganName(String organName) {
        _organName = organName;
    }

    public String getSector() {
        return GetterUtil.getString(_sector);
    }

    public void setSector(String sector) {
        _sector = sector;
    }

    public String getEmail() {
        return GetterUtil.getString(_email);
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getFixedtele() {
        return GetterUtil.getString(_fixedtele);
    }

    public void setFixedtele(String fixedtele) {
        _fixedtele = fixedtele;
    }

    public String getMobiletele() {
        return GetterUtil.getString(_mobiletele);
    }

    public void setMobiletele(String mobiletele) {
        _mobiletele = mobiletele;
    }

    public String getFax() {
        return GetterUtil.getString(_fax);
    }

    public void setFax(String fax) {
        _fax = fax;
    }

    public String getRegion() {
        return GetterUtil.getString(_region);
    }

    public void setRegion(String region) {
        _region = region;
    }

    public String getZone() {
        return GetterUtil.getString(_zone);
    }

    public void setZone(String zone) {
        _zone = zone;
    }

    public String getWoreda() {
        return GetterUtil.getString(_woreda);
    }

    public void setWoreda(String woreda) {
        _woreda = woreda;
    }

    public String getKebele() {
        return GetterUtil.getString(_kebele);
    }

    public void setKebele(String kebele) {
        _kebele = kebele;
    }

    public String getHouseno() {
        return GetterUtil.getString(_houseno);
    }

    public void setHouseno(String houseno) {
        _houseno = houseno;
    }

    public String getUserIId() {
        return GetterUtil.getString(_userIId);
    }

    public void setUserIId(String userIId) {
        _userIId = userIId;
    }

    public customer toEscapedModel() {
        if (isEscapedModel()) {
            return (customer) this;
        } else {
            customer model = new customerImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setOrganName(HtmlUtil.escape(getOrganName()));
            model.setSector(HtmlUtil.escape(getSector()));
            model.setEmail(HtmlUtil.escape(getEmail()));
            model.setFixedtele(HtmlUtil.escape(getFixedtele()));
            model.setMobiletele(HtmlUtil.escape(getMobiletele()));
            model.setFax(HtmlUtil.escape(getFax()));
            model.setRegion(HtmlUtil.escape(getRegion()));
            model.setZone(HtmlUtil.escape(getZone()));
            model.setWoreda(HtmlUtil.escape(getWoreda()));
            model.setKebele(HtmlUtil.escape(getKebele()));
            model.setHouseno(HtmlUtil.escape(getHouseno()));
            model.setUserIId(HtmlUtil.escape(getUserIId()));

            model = (customer) Proxy.newProxyInstance(customer.class.getClassLoader(),
                    new Class[] { customer.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        customerImpl clone = new customerImpl();

        clone.setId(getId());
        clone.setOrganName(getOrganName());
        clone.setSector(getSector());
        clone.setEmail(getEmail());
        clone.setFixedtele(getFixedtele());
        clone.setMobiletele(getMobiletele());
        clone.setFax(getFax());
        clone.setRegion(getRegion());
        clone.setZone(getZone());
        clone.setWoreda(getWoreda());
        clone.setKebele(getKebele());
        clone.setHouseno(getHouseno());
        clone.setUserIId(getUserIId());

        return clone;
    }

    public int compareTo(customer customer) {
        int pk = customer.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        customer customer = null;

        try {
            customer = (customer) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = customer.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", organName=");
        sb.append(getOrganName());
        sb.append(", sector=");
        sb.append(getSector());
        sb.append(", email=");
        sb.append(getEmail());
        sb.append(", fixedtele=");
        sb.append(getFixedtele());
        sb.append(", mobiletele=");
        sb.append(getMobiletele());
        sb.append(", fax=");
        sb.append(getFax());
        sb.append(", region=");
        sb.append(getRegion());
        sb.append(", zone=");
        sb.append(getZone());
        sb.append(", woreda=");
        sb.append(getWoreda());
        sb.append(", kebele=");
        sb.append(getKebele());
        sb.append(", houseno=");
        sb.append(getHouseno());
        sb.append(", userIId=");
        sb.append(getUserIId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.customer");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organName</column-name><column-value><![CDATA[");
        sb.append(getOrganName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>sector</column-name><column-value><![CDATA[");
        sb.append(getSector());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>email</column-name><column-value><![CDATA[");
        sb.append(getEmail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fixedtele</column-name><column-value><![CDATA[");
        sb.append(getFixedtele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>mobiletele</column-name><column-value><![CDATA[");
        sb.append(getMobiletele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fax</column-name><column-value><![CDATA[");
        sb.append(getFax());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>region</column-name><column-value><![CDATA[");
        sb.append(getRegion());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>zone</column-name><column-value><![CDATA[");
        sb.append(getZone());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>woreda</column-name><column-value><![CDATA[");
        sb.append(getWoreda());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>kebele</column-name><column-value><![CDATA[");
        sb.append(getKebele());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>houseno</column-name><column-value><![CDATA[");
        sb.append(getHouseno());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userIId</column-name><column-value><![CDATA[");
        sb.append(getUserIId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
