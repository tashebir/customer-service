package com.example.service.model.impl;

import com.example.service.model.complainSolution;
import com.example.service.model.complainSolutionSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="complainSolutionModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>complainSolution</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.complainSolution
 * @see com.example.service.model.complainSolutionModel
 * @see com.example.service.model.impl.complainSolutionImpl
 *
 */
public class complainSolutionModelImpl extends BaseModelImpl<complainSolution> {
    public static final String TABLE_NAME = "complainSolution";
    public static final Object[][] TABLE_COLUMNS = {
            { "solutionId", new Integer(Types.INTEGER) },
            

            { "solutionSubject", new Integer(Types.VARCHAR) },
            

            { "solutionDetail", new Integer(Types.VARCHAR) },
            

            { "solutionDate", new Integer(Types.VARCHAR) },
            

            { "complainId", new Integer(Types.VARCHAR) },
            

            { "subprojectId", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table complainSolution (solutionId INTEGER not null primary key,solutionSubject VARCHAR(75) null,solutionDetail VARCHAR(75) null,solutionDate VARCHAR(75) null,complainId VARCHAR(75) null,subprojectId VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table complainSolution";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.complainSolution"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.complainSolution"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.complainSolution"));
    private int _solutionId;
    private String _solutionSubject;
    private String _solutionDetail;
    private String _solutionDate;
    private String _complainId;
    private String _subprojectId;

    public complainSolutionModelImpl() {
    }

    public static complainSolution toModel(complainSolutionSoap soapModel) {
        complainSolution model = new complainSolutionImpl();

        model.setSolutionId(soapModel.getSolutionId());
        model.setSolutionSubject(soapModel.getSolutionSubject());
        model.setSolutionDetail(soapModel.getSolutionDetail());
        model.setSolutionDate(soapModel.getSolutionDate());
        model.setComplainId(soapModel.getComplainId());
        model.setSubprojectId(soapModel.getSubprojectId());

        return model;
    }

    public static List<complainSolution> toModels(
        complainSolutionSoap[] soapModels) {
        List<complainSolution> models = new ArrayList<complainSolution>(soapModels.length);

        for (complainSolutionSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _solutionId;
    }

    public void setPrimaryKey(int pk) {
        setSolutionId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_solutionId);
    }

    public int getSolutionId() {
        return _solutionId;
    }

    public void setSolutionId(int solutionId) {
        _solutionId = solutionId;
    }

    public String getSolutionSubject() {
        return GetterUtil.getString(_solutionSubject);
    }

    public void setSolutionSubject(String solutionSubject) {
        _solutionSubject = solutionSubject;
    }

    public String getSolutionDetail() {
        return GetterUtil.getString(_solutionDetail);
    }

    public void setSolutionDetail(String solutionDetail) {
        _solutionDetail = solutionDetail;
    }

    public String getSolutionDate() {
        return GetterUtil.getString(_solutionDate);
    }

    public void setSolutionDate(String solutionDate) {
        _solutionDate = solutionDate;
    }

    public String getComplainId() {
        return GetterUtil.getString(_complainId);
    }

    public void setComplainId(String complainId) {
        _complainId = complainId;
    }

    public String getSubprojectId() {
        return GetterUtil.getString(_subprojectId);
    }

    public void setSubprojectId(String subprojectId) {
        _subprojectId = subprojectId;
    }

    public complainSolution toEscapedModel() {
        if (isEscapedModel()) {
            return (complainSolution) this;
        } else {
            complainSolution model = new complainSolutionImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setSolutionId(getSolutionId());
            model.setSolutionSubject(HtmlUtil.escape(getSolutionSubject()));
            model.setSolutionDetail(HtmlUtil.escape(getSolutionDetail()));
            model.setSolutionDate(HtmlUtil.escape(getSolutionDate()));
            model.setComplainId(HtmlUtil.escape(getComplainId()));
            model.setSubprojectId(HtmlUtil.escape(getSubprojectId()));

            model = (complainSolution) Proxy.newProxyInstance(complainSolution.class.getClassLoader(),
                    new Class[] { complainSolution.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        complainSolutionImpl clone = new complainSolutionImpl();

        clone.setSolutionId(getSolutionId());
        clone.setSolutionSubject(getSolutionSubject());
        clone.setSolutionDetail(getSolutionDetail());
        clone.setSolutionDate(getSolutionDate());
        clone.setComplainId(getComplainId());
        clone.setSubprojectId(getSubprojectId());

        return clone;
    }

    public int compareTo(complainSolution complainSolution) {
        int pk = complainSolution.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        complainSolution complainSolution = null;

        try {
            complainSolution = (complainSolution) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = complainSolution.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{solutionId=");
        sb.append(getSolutionId());
        sb.append(", solutionSubject=");
        sb.append(getSolutionSubject());
        sb.append(", solutionDetail=");
        sb.append(getSolutionDetail());
        sb.append(", solutionDate=");
        sb.append(getSolutionDate());
        sb.append(", complainId=");
        sb.append(getComplainId());
        sb.append(", subprojectId=");
        sb.append(getSubprojectId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.complainSolution");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>solutionId</column-name><column-value><![CDATA[");
        sb.append(getSolutionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionSubject</column-name><column-value><![CDATA[");
        sb.append(getSolutionSubject());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionDetail</column-name><column-value><![CDATA[");
        sb.append(getSolutionDetail());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>solutionDate</column-name><column-value><![CDATA[");
        sb.append(getSolutionDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>complainId</column-name><column-value><![CDATA[");
        sb.append(getComplainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subprojectId</column-name><column-value><![CDATA[");
        sb.append(getSubprojectId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
