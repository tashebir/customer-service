package com.example.service.model.impl;

import com.example.service.model.projectactivity;
import com.example.service.model.projectactivitySoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="projectactivityModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>projectactivity</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.projectactivity
 * @see com.example.service.model.projectactivityModel
 * @see com.example.service.model.impl.projectactivityImpl
 *
 */
public class projectactivityModelImpl extends BaseModelImpl<projectactivity> {
    public static final String TABLE_NAME = "projectactivity";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "activity", new Integer(Types.VARCHAR) },
            

            { "description", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table projectactivity (id INTEGER not null primary key,activity VARCHAR(75) null,description VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table projectactivity";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.projectactivity"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.projectactivity"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.projectactivity"));
    private int _id;
    private String _activity;
    private String _description;

    public projectactivityModelImpl() {
    }

    public static projectactivity toModel(projectactivitySoap soapModel) {
        projectactivity model = new projectactivityImpl();

        model.setId(soapModel.getId());
        model.setActivity(soapModel.getActivity());
        model.setDescription(soapModel.getDescription());

        return model;
    }

    public static List<projectactivity> toModels(
        projectactivitySoap[] soapModels) {
        List<projectactivity> models = new ArrayList<projectactivity>(soapModels.length);

        for (projectactivitySoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getActivity() {
        return GetterUtil.getString(_activity);
    }

    public void setActivity(String activity) {
        _activity = activity;
    }

    public String getDescription() {
        return GetterUtil.getString(_description);
    }

    public void setDescription(String description) {
        _description = description;
    }

    public projectactivity toEscapedModel() {
        if (isEscapedModel()) {
            return (projectactivity) this;
        } else {
            projectactivity model = new projectactivityImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setActivity(HtmlUtil.escape(getActivity()));
            model.setDescription(HtmlUtil.escape(getDescription()));

            model = (projectactivity) Proxy.newProxyInstance(projectactivity.class.getClassLoader(),
                    new Class[] { projectactivity.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        projectactivityImpl clone = new projectactivityImpl();

        clone.setId(getId());
        clone.setActivity(getActivity());
        clone.setDescription(getDescription());

        return clone;
    }

    public int compareTo(projectactivity projectactivity) {
        int pk = projectactivity.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        projectactivity projectactivity = null;

        try {
            projectactivity = (projectactivity) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = projectactivity.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", activity=");
        sb.append(getActivity());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.projectactivity");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activity</column-name><column-value><![CDATA[");
        sb.append(getActivity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
