package com.example.service.model.impl;

import com.example.service.model.subproject;
import com.example.service.model.subprojectSoap;

import com.liferay.portal.kernel.bean.ReadOnlyBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Types;

import java.util.ArrayList;
import java.util.List;


/**
 * <a href="subprojectModelImpl.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is a model that represents the <code>subproject</code> table
 * in the database.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.example.service.model.subproject
 * @see com.example.service.model.subprojectModel
 * @see com.example.service.model.impl.subprojectImpl
 *
 */
public class subprojectModelImpl extends BaseModelImpl<subproject> {
    public static final String TABLE_NAME = "subproject";
    public static final Object[][] TABLE_COLUMNS = {
            { "id", new Integer(Types.INTEGER) },
            

            { "subproject_id", new Integer(Types.VARCHAR) },
            

            { "subproject_name", new Integer(Types.VARCHAR) },
            

            { "start_date", new Integer(Types.VARCHAR) },
            

            { "end_date", new Integer(Types.VARCHAR) },
            

            { "duration", new Integer(Types.VARCHAR) },
            

            { "description", new Integer(Types.VARCHAR) },
            

            { "projectId", new Integer(Types.VARCHAR) }
        };
    public static final String TABLE_SQL_CREATE = "create table subproject (id INTEGER not null primary key,subproject_id VARCHAR(75) null,subproject_name VARCHAR(75) null,start_date VARCHAR(75) null,end_date VARCHAR(75) null,duration VARCHAR(75) null,description VARCHAR(75) null,projectId VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table subproject";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.example.service.model.subproject"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.example.service.model.subproject"),
            true);
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.example.service.model.subproject"));
    private int _id;
    private String _subproject_id;
    private String _subproject_name;
    private String _start_date;
    private String _end_date;
    private String _duration;
    private String _description;
    private String _projectId;

    public subprojectModelImpl() {
    }

    public static subproject toModel(subprojectSoap soapModel) {
        subproject model = new subprojectImpl();

        model.setId(soapModel.getId());
        model.setSubproject_id(soapModel.getSubproject_id());
        model.setSubproject_name(soapModel.getSubproject_name());
        model.setStart_date(soapModel.getStart_date());
        model.setEnd_date(soapModel.getEnd_date());
        model.setDuration(soapModel.getDuration());
        model.setDescription(soapModel.getDescription());
        model.setProjectId(soapModel.getProjectId());

        return model;
    }

    public static List<subproject> toModels(subprojectSoap[] soapModels) {
        List<subproject> models = new ArrayList<subproject>(soapModels.length);

        for (subprojectSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    public int getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(int pk) {
        setId(pk);
    }

    public Serializable getPrimaryKeyObj() {
        return new Integer(_id);
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public String getSubproject_id() {
        return GetterUtil.getString(_subproject_id);
    }

    public void setSubproject_id(String subproject_id) {
        _subproject_id = subproject_id;
    }

    public String getSubproject_name() {
        return GetterUtil.getString(_subproject_name);
    }

    public void setSubproject_name(String subproject_name) {
        _subproject_name = subproject_name;
    }

    public String getStart_date() {
        return GetterUtil.getString(_start_date);
    }

    public void setStart_date(String start_date) {
        _start_date = start_date;
    }

    public String getEnd_date() {
        return GetterUtil.getString(_end_date);
    }

    public void setEnd_date(String end_date) {
        _end_date = end_date;
    }

    public String getDuration() {
        return GetterUtil.getString(_duration);
    }

    public void setDuration(String duration) {
        _duration = duration;
    }

    public String getDescription() {
        return GetterUtil.getString(_description);
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getProjectId() {
        return GetterUtil.getString(_projectId);
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public subproject toEscapedModel() {
        if (isEscapedModel()) {
            return (subproject) this;
        } else {
            subproject model = new subprojectImpl();

            model.setNew(isNew());
            model.setEscapedModel(true);

            model.setId(getId());
            model.setSubproject_id(HtmlUtil.escape(getSubproject_id()));
            model.setSubproject_name(HtmlUtil.escape(getSubproject_name()));
            model.setStart_date(HtmlUtil.escape(getStart_date()));
            model.setEnd_date(HtmlUtil.escape(getEnd_date()));
            model.setDuration(HtmlUtil.escape(getDuration()));
            model.setDescription(HtmlUtil.escape(getDescription()));
            model.setProjectId(HtmlUtil.escape(getProjectId()));

            model = (subproject) Proxy.newProxyInstance(subproject.class.getClassLoader(),
                    new Class[] { subproject.class },
                    new ReadOnlyBeanHandler(model));

            return model;
        }
    }

    public Object clone() {
        subprojectImpl clone = new subprojectImpl();

        clone.setId(getId());
        clone.setSubproject_id(getSubproject_id());
        clone.setSubproject_name(getSubproject_name());
        clone.setStart_date(getStart_date());
        clone.setEnd_date(getEnd_date());
        clone.setDuration(getDuration());
        clone.setDescription(getDescription());
        clone.setProjectId(getProjectId());

        return clone;
    }

    public int compareTo(subproject subproject) {
        int pk = subproject.getPrimaryKey();

        if (getPrimaryKey() < pk) {
            return -1;
        } else if (getPrimaryKey() > pk) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        subproject subproject = null;

        try {
            subproject = (subproject) obj;
        } catch (ClassCastException cce) {
            return false;
        }

        int pk = subproject.getPrimaryKey();

        if (getPrimaryKey() == pk) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return getPrimaryKey();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{id=");
        sb.append(getId());
        sb.append(", subproject_id=");
        sb.append(getSubproject_id());
        sb.append(", subproject_name=");
        sb.append(getSubproject_name());
        sb.append(", start_date=");
        sb.append(getStart_date());
        sb.append(", end_date=");
        sb.append(getEnd_date());
        sb.append(", duration=");
        sb.append(getDuration());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<model><model-name>");
        sb.append("com.example.service.model.subproject");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subproject_id</column-name><column-value><![CDATA[");
        sb.append(getSubproject_id());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subproject_name</column-name><column-value><![CDATA[");
        sb.append(getSubproject_name());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>start_date</column-name><column-value><![CDATA[");
        sb.append(getStart_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>end_date</column-name><column-value><![CDATA[");
        sb.append(getEnd_date());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>duration</column-name><column-value><![CDATA[");
        sb.append(getDuration());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
