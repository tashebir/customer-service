package com.example.service.service.persistence;

import com.example.service.NoSuchcomplainReportException;
import com.example.service.model.complainReport;
import com.example.service.model.impl.complainReportImpl;
import com.example.service.model.impl.complainReportModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class complainReportPersistenceImpl extends BasePersistenceImpl
    implements complainReportPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = complainReportImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(complainReportModelImpl.ENTITY_CACHE_ENABLED,
            complainReportModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(complainReportModelImpl.ENTITY_CACHE_ENABLED,
            complainReportModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(complainReportPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(complainReport complainReport) {
        EntityCacheUtil.putResult(complainReportModelImpl.ENTITY_CACHE_ENABLED,
            complainReportImpl.class, complainReport.getPrimaryKey(),
            complainReport);
    }

    public void cacheResult(List<complainReport> complainReports) {
        for (complainReport complainReport : complainReports) {
            if (EntityCacheUtil.getResult(
                        complainReportModelImpl.ENTITY_CACHE_ENABLED,
                        complainReportImpl.class,
                        complainReport.getPrimaryKey(), this) == null) {
                cacheResult(complainReport);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(complainReportImpl.class.getName());
        EntityCacheUtil.clearCache(complainReportImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public complainReport create(int id) {
        complainReport complainReport = new complainReportImpl();

        complainReport.setNew(true);
        complainReport.setPrimaryKey(id);

        return complainReport;
    }

    public complainReport remove(int id)
        throws NoSuchcomplainReportException, SystemException {
        Session session = null;

        try {
            session = openSession();

            complainReport complainReport = (complainReport) session.get(complainReportImpl.class,
                    new Integer(id));

            if (complainReport == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No complainReport exists with the primary key " +
                        id);
                }

                throw new NoSuchcomplainReportException(
                    "No complainReport exists with the primary key " + id);
            }

            return remove(complainReport);
        } catch (NoSuchcomplainReportException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public complainReport remove(complainReport complainReport)
        throws SystemException {
        for (ModelListener<complainReport> listener : listeners) {
            listener.onBeforeRemove(complainReport);
        }

        complainReport = removeImpl(complainReport);

        for (ModelListener<complainReport> listener : listeners) {
            listener.onAfterRemove(complainReport);
        }

        return complainReport;
    }

    protected complainReport removeImpl(complainReport complainReport)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (complainReport.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(complainReportImpl.class,
                        complainReport.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(complainReport);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(complainReportModelImpl.ENTITY_CACHE_ENABLED,
            complainReportImpl.class, complainReport.getPrimaryKey());

        return complainReport;
    }

    /**
     * @deprecated Use <code>update(complainReport complainReport, boolean merge)</code>.
     */
    public complainReport update(complainReport complainReport)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(complainReport complainReport) method. Use update(complainReport complainReport, boolean merge) instead.");
        }

        return update(complainReport, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainReport the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainReport is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public complainReport update(complainReport complainReport, boolean merge)
        throws SystemException {
        boolean isNew = complainReport.isNew();

        for (ModelListener<complainReport> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(complainReport);
            } else {
                listener.onBeforeUpdate(complainReport);
            }
        }

        complainReport = updateImpl(complainReport, merge);

        for (ModelListener<complainReport> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(complainReport);
            } else {
                listener.onAfterUpdate(complainReport);
            }
        }

        return complainReport;
    }

    public complainReport updateImpl(
        com.example.service.model.complainReport complainReport, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, complainReport, merge);

            complainReport.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(complainReportModelImpl.ENTITY_CACHE_ENABLED,
            complainReportImpl.class, complainReport.getPrimaryKey(),
            complainReport);

        return complainReport;
    }

    public complainReport findByPrimaryKey(int id)
        throws NoSuchcomplainReportException, SystemException {
        complainReport complainReport = fetchByPrimaryKey(id);

        if (complainReport == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No complainReport exists with the primary key " +
                    id);
            }

            throw new NoSuchcomplainReportException(
                "No complainReport exists with the primary key " + id);
        }

        return complainReport;
    }

    public complainReport fetchByPrimaryKey(int id) throws SystemException {
        complainReport complainReport = (complainReport) EntityCacheUtil.getResult(complainReportModelImpl.ENTITY_CACHE_ENABLED,
                complainReportImpl.class, id, this);

        if (complainReport == null) {
            Session session = null;

            try {
                session = openSession();

                complainReport = (complainReport) session.get(complainReportImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (complainReport != null) {
                    cacheResult(complainReport);
                }

                closeSession(session);
            }
        }

        return complainReport;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<complainReport> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<complainReport> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<complainReport> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<complainReport> list = (List<complainReport>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.complainReport ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<complainReport>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<complainReport>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<complainReport>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (complainReport complainReport : findAll()) {
            remove(complainReport);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.complainReport");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.complainReport")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<complainReport>> listenersList = new ArrayList<ModelListener<complainReport>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<complainReport>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
