package com.example.service.service.persistence;

import com.example.service.NoSuchcustomerprojectException;
import com.example.service.model.customerproject;
import com.example.service.model.impl.customerprojectImpl;
import com.example.service.model.impl.customerprojectModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class customerprojectPersistenceImpl extends BasePersistenceImpl
    implements customerprojectPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = customerprojectImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
            customerprojectModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
            customerprojectModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(customerprojectPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(customerproject customerproject) {
        EntityCacheUtil.putResult(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
            customerprojectImpl.class, customerproject.getPrimaryKey(),
            customerproject);
    }

    public void cacheResult(List<customerproject> customerprojects) {
        for (customerproject customerproject : customerprojects) {
            if (EntityCacheUtil.getResult(
                        customerprojectModelImpl.ENTITY_CACHE_ENABLED,
                        customerprojectImpl.class,
                        customerproject.getPrimaryKey(), this) == null) {
                cacheResult(customerproject);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(customerprojectImpl.class.getName());
        EntityCacheUtil.clearCache(customerprojectImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public customerproject create(int id) {
        customerproject customerproject = new customerprojectImpl();

        customerproject.setNew(true);
        customerproject.setPrimaryKey(id);

        return customerproject;
    }

    public customerproject remove(int id)
        throws NoSuchcustomerprojectException, SystemException {
        Session session = null;

        try {
            session = openSession();

            customerproject customerproject = (customerproject) session.get(customerprojectImpl.class,
                    new Integer(id));

            if (customerproject == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No customerproject exists with the primary key " +
                        id);
                }

                throw new NoSuchcustomerprojectException(
                    "No customerproject exists with the primary key " + id);
            }

            return remove(customerproject);
        } catch (NoSuchcustomerprojectException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public customerproject remove(customerproject customerproject)
        throws SystemException {
        for (ModelListener<customerproject> listener : listeners) {
            listener.onBeforeRemove(customerproject);
        }

        customerproject = removeImpl(customerproject);

        for (ModelListener<customerproject> listener : listeners) {
            listener.onAfterRemove(customerproject);
        }

        return customerproject;
    }

    protected customerproject removeImpl(customerproject customerproject)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (customerproject.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(customerprojectImpl.class,
                        customerproject.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(customerproject);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
            customerprojectImpl.class, customerproject.getPrimaryKey());

        return customerproject;
    }

    /**
     * @deprecated Use <code>update(customerproject customerproject, boolean merge)</code>.
     */
    public customerproject update(customerproject customerproject)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(customerproject customerproject) method. Use update(customerproject customerproject, boolean merge) instead.");
        }

        return update(customerproject, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customerproject the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customerproject is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public customerproject update(customerproject customerproject, boolean merge)
        throws SystemException {
        boolean isNew = customerproject.isNew();

        for (ModelListener<customerproject> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(customerproject);
            } else {
                listener.onBeforeUpdate(customerproject);
            }
        }

        customerproject = updateImpl(customerproject, merge);

        for (ModelListener<customerproject> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(customerproject);
            } else {
                listener.onAfterUpdate(customerproject);
            }
        }

        return customerproject;
    }

    public customerproject updateImpl(
        com.example.service.model.customerproject customerproject, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, customerproject, merge);

            customerproject.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
            customerprojectImpl.class, customerproject.getPrimaryKey(),
            customerproject);

        return customerproject;
    }

    public customerproject findByPrimaryKey(int id)
        throws NoSuchcustomerprojectException, SystemException {
        customerproject customerproject = fetchByPrimaryKey(id);

        if (customerproject == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No customerproject exists with the primary key " +
                    id);
            }

            throw new NoSuchcustomerprojectException(
                "No customerproject exists with the primary key " + id);
        }

        return customerproject;
    }

    public customerproject fetchByPrimaryKey(int id) throws SystemException {
        customerproject customerproject = (customerproject) EntityCacheUtil.getResult(customerprojectModelImpl.ENTITY_CACHE_ENABLED,
                customerprojectImpl.class, id, this);

        if (customerproject == null) {
            Session session = null;

            try {
                session = openSession();

                customerproject = (customerproject) session.get(customerprojectImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (customerproject != null) {
                    cacheResult(customerproject);
                }

                closeSession(session);
            }
        }

        return customerproject;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<customerproject> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<customerproject> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<customerproject> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<customerproject> list = (List<customerproject>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.customerproject ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<customerproject>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<customerproject>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<customerproject>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (customerproject customerproject : findAll()) {
            remove(customerproject);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.customerproject");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.customerproject")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<customerproject>> listenersList = new ArrayList<ModelListener<customerproject>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<customerproject>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
