package com.example.service.service.persistence;

import com.example.service.NoSuchsubprojectactivityplanException;
import com.example.service.model.impl.subprojectactivityplanImpl;
import com.example.service.model.impl.subprojectactivityplanModelImpl;
import com.example.service.model.subprojectactivityplan;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class subprojectactivityplanPersistenceImpl extends BasePersistenceImpl
    implements subprojectactivityplanPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = subprojectactivityplanImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
            subprojectactivityplanModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
            subprojectactivityplanModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(subprojectactivityplanPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(subprojectactivityplan subprojectactivityplan) {
        EntityCacheUtil.putResult(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
            subprojectactivityplanImpl.class,
            subprojectactivityplan.getPrimaryKey(), subprojectactivityplan);
    }

    public void cacheResult(
        List<subprojectactivityplan> subprojectactivityplans) {
        for (subprojectactivityplan subprojectactivityplan : subprojectactivityplans) {
            if (EntityCacheUtil.getResult(
                        subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
                        subprojectactivityplanImpl.class,
                        subprojectactivityplan.getPrimaryKey(), this) == null) {
                cacheResult(subprojectactivityplan);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(subprojectactivityplanImpl.class.getName());
        EntityCacheUtil.clearCache(subprojectactivityplanImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public subprojectactivityplan create(int id) {
        subprojectactivityplan subprojectactivityplan = new subprojectactivityplanImpl();

        subprojectactivityplan.setNew(true);
        subprojectactivityplan.setPrimaryKey(id);

        return subprojectactivityplan;
    }

    public subprojectactivityplan remove(int id)
        throws NoSuchsubprojectactivityplanException, SystemException {
        Session session = null;

        try {
            session = openSession();

            subprojectactivityplan subprojectactivityplan = (subprojectactivityplan) session.get(subprojectactivityplanImpl.class,
                    new Integer(id));

            if (subprojectactivityplan == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(
                        "No subprojectactivityplan exists with the primary key " +
                        id);
                }

                throw new NoSuchsubprojectactivityplanException(
                    "No subprojectactivityplan exists with the primary key " +
                    id);
            }

            return remove(subprojectactivityplan);
        } catch (NoSuchsubprojectactivityplanException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public subprojectactivityplan remove(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        for (ModelListener<subprojectactivityplan> listener : listeners) {
            listener.onBeforeRemove(subprojectactivityplan);
        }

        subprojectactivityplan = removeImpl(subprojectactivityplan);

        for (ModelListener<subprojectactivityplan> listener : listeners) {
            listener.onAfterRemove(subprojectactivityplan);
        }

        return subprojectactivityplan;
    }

    protected subprojectactivityplan removeImpl(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (subprojectactivityplan.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(subprojectactivityplanImpl.class,
                        subprojectactivityplan.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(subprojectactivityplan);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
            subprojectactivityplanImpl.class,
            subprojectactivityplan.getPrimaryKey());

        return subprojectactivityplan;
    }

    /**
     * @deprecated Use <code>update(subprojectactivityplan subprojectactivityplan, boolean merge)</code>.
     */
    public subprojectactivityplan update(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(subprojectactivityplan subprojectactivityplan) method. Use update(subprojectactivityplan subprojectactivityplan, boolean merge) instead.");
        }

        return update(subprojectactivityplan, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                subprojectactivityplan the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when subprojectactivityplan is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public subprojectactivityplan update(
        subprojectactivityplan subprojectactivityplan, boolean merge)
        throws SystemException {
        boolean isNew = subprojectactivityplan.isNew();

        for (ModelListener<subprojectactivityplan> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(subprojectactivityplan);
            } else {
                listener.onBeforeUpdate(subprojectactivityplan);
            }
        }

        subprojectactivityplan = updateImpl(subprojectactivityplan, merge);

        for (ModelListener<subprojectactivityplan> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(subprojectactivityplan);
            } else {
                listener.onAfterUpdate(subprojectactivityplan);
            }
        }

        return subprojectactivityplan;
    }

    public subprojectactivityplan updateImpl(
        com.example.service.model.subprojectactivityplan subprojectactivityplan,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, subprojectactivityplan, merge);

            subprojectactivityplan.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
            subprojectactivityplanImpl.class,
            subprojectactivityplan.getPrimaryKey(), subprojectactivityplan);

        return subprojectactivityplan;
    }

    public subprojectactivityplan findByPrimaryKey(int id)
        throws NoSuchsubprojectactivityplanException, SystemException {
        subprojectactivityplan subprojectactivityplan = fetchByPrimaryKey(id);

        if (subprojectactivityplan == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(
                    "No subprojectactivityplan exists with the primary key " +
                    id);
            }

            throw new NoSuchsubprojectactivityplanException(
                "No subprojectactivityplan exists with the primary key " + id);
        }

        return subprojectactivityplan;
    }

    public subprojectactivityplan fetchByPrimaryKey(int id)
        throws SystemException {
        subprojectactivityplan subprojectactivityplan = (subprojectactivityplan) EntityCacheUtil.getResult(subprojectactivityplanModelImpl.ENTITY_CACHE_ENABLED,
                subprojectactivityplanImpl.class, id, this);

        if (subprojectactivityplan == null) {
            Session session = null;

            try {
                session = openSession();

                subprojectactivityplan = (subprojectactivityplan) session.get(subprojectactivityplanImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (subprojectactivityplan != null) {
                    cacheResult(subprojectactivityplan);
                }

                closeSession(session);
            }
        }

        return subprojectactivityplan;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<subprojectactivityplan> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<subprojectactivityplan> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<subprojectactivityplan> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<subprojectactivityplan> list = (List<subprojectactivityplan>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append(
                    "FROM com.example.service.model.subprojectactivityplan ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<subprojectactivityplan>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<subprojectactivityplan>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<subprojectactivityplan>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (subprojectactivityplan subprojectactivityplan : findAll()) {
            remove(subprojectactivityplan);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.subprojectactivityplan");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.subprojectactivityplan")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<subprojectactivityplan>> listenersList = new ArrayList<ModelListener<subprojectactivityplan>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<subprojectactivityplan>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
