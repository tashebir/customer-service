package com.example.service.service.persistence;

import com.example.service.NoSuchcustomercomplainException;
import com.example.service.model.customercomplain;
import com.example.service.model.impl.customercomplainImpl;
import com.example.service.model.impl.customercomplainModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class customercomplainPersistenceImpl extends BasePersistenceImpl
    implements customercomplainPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = customercomplainImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
            customercomplainModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
            customercomplainModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(customercomplainPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(customercomplain customercomplain) {
        EntityCacheUtil.putResult(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
            customercomplainImpl.class, customercomplain.getPrimaryKey(),
            customercomplain);
    }

    public void cacheResult(List<customercomplain> customercomplains) {
        for (customercomplain customercomplain : customercomplains) {
            if (EntityCacheUtil.getResult(
                        customercomplainModelImpl.ENTITY_CACHE_ENABLED,
                        customercomplainImpl.class,
                        customercomplain.getPrimaryKey(), this) == null) {
                cacheResult(customercomplain);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(customercomplainImpl.class.getName());
        EntityCacheUtil.clearCache(customercomplainImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public customercomplain create(int id) {
        customercomplain customercomplain = new customercomplainImpl();

        customercomplain.setNew(true);
        customercomplain.setPrimaryKey(id);

        return customercomplain;
    }

    public customercomplain remove(int id)
        throws NoSuchcustomercomplainException, SystemException {
        Session session = null;

        try {
            session = openSession();

            customercomplain customercomplain = (customercomplain) session.get(customercomplainImpl.class,
                    new Integer(id));

            if (customercomplain == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(
                        "No customercomplain exists with the primary key " +
                        id);
                }

                throw new NoSuchcustomercomplainException(
                    "No customercomplain exists with the primary key " + id);
            }

            return remove(customercomplain);
        } catch (NoSuchcustomercomplainException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public customercomplain remove(customercomplain customercomplain)
        throws SystemException {
        for (ModelListener<customercomplain> listener : listeners) {
            listener.onBeforeRemove(customercomplain);
        }

        customercomplain = removeImpl(customercomplain);

        for (ModelListener<customercomplain> listener : listeners) {
            listener.onAfterRemove(customercomplain);
        }

        return customercomplain;
    }

    protected customercomplain removeImpl(customercomplain customercomplain)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (customercomplain.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(customercomplainImpl.class,
                        customercomplain.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(customercomplain);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
            customercomplainImpl.class, customercomplain.getPrimaryKey());

        return customercomplain;
    }

    /**
     * @deprecated Use <code>update(customercomplain customercomplain, boolean merge)</code>.
     */
    public customercomplain update(customercomplain customercomplain)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(customercomplain customercomplain) method. Use update(customercomplain customercomplain, boolean merge) instead.");
        }

        return update(customercomplain, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customercomplain the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customercomplain is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public customercomplain update(customercomplain customercomplain,
        boolean merge) throws SystemException {
        boolean isNew = customercomplain.isNew();

        for (ModelListener<customercomplain> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(customercomplain);
            } else {
                listener.onBeforeUpdate(customercomplain);
            }
        }

        customercomplain = updateImpl(customercomplain, merge);

        for (ModelListener<customercomplain> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(customercomplain);
            } else {
                listener.onAfterUpdate(customercomplain);
            }
        }

        return customercomplain;
    }

    public customercomplain updateImpl(
        com.example.service.model.customercomplain customercomplain,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, customercomplain, merge);

            customercomplain.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
            customercomplainImpl.class, customercomplain.getPrimaryKey(),
            customercomplain);

        return customercomplain;
    }

    public customercomplain findByPrimaryKey(int id)
        throws NoSuchcustomercomplainException, SystemException {
        customercomplain customercomplain = fetchByPrimaryKey(id);

        if (customercomplain == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No customercomplain exists with the primary key " +
                    id);
            }

            throw new NoSuchcustomercomplainException(
                "No customercomplain exists with the primary key " + id);
        }

        return customercomplain;
    }

    public customercomplain fetchByPrimaryKey(int id) throws SystemException {
        customercomplain customercomplain = (customercomplain) EntityCacheUtil.getResult(customercomplainModelImpl.ENTITY_CACHE_ENABLED,
                customercomplainImpl.class, id, this);

        if (customercomplain == null) {
            Session session = null;

            try {
                session = openSession();

                customercomplain = (customercomplain) session.get(customercomplainImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (customercomplain != null) {
                    cacheResult(customercomplain);
                }

                closeSession(session);
            }
        }

        return customercomplain;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<customercomplain> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<customercomplain> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<customercomplain> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<customercomplain> list = (List<customercomplain>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.customercomplain ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<customercomplain>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<customercomplain>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<customercomplain>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (customercomplain customercomplain : findAll()) {
            remove(customercomplain);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.customercomplain");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.customercomplain")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<customercomplain>> listenersList = new ArrayList<ModelListener<customercomplain>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<customercomplain>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
