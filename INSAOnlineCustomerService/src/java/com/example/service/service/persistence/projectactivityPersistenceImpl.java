package com.example.service.service.persistence;

import com.example.service.NoSuchprojectactivityException;
import com.example.service.model.impl.projectactivityImpl;
import com.example.service.model.impl.projectactivityModelImpl;
import com.example.service.model.projectactivity;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class projectactivityPersistenceImpl extends BasePersistenceImpl
    implements projectactivityPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = projectactivityImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
            projectactivityModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
            projectactivityModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(projectactivityPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(projectactivity projectactivity) {
        EntityCacheUtil.putResult(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
            projectactivityImpl.class, projectactivity.getPrimaryKey(),
            projectactivity);
    }

    public void cacheResult(List<projectactivity> projectactivities) {
        for (projectactivity projectactivity : projectactivities) {
            if (EntityCacheUtil.getResult(
                        projectactivityModelImpl.ENTITY_CACHE_ENABLED,
                        projectactivityImpl.class,
                        projectactivity.getPrimaryKey(), this) == null) {
                cacheResult(projectactivity);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(projectactivityImpl.class.getName());
        EntityCacheUtil.clearCache(projectactivityImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public projectactivity create(int id) {
        projectactivity projectactivity = new projectactivityImpl();

        projectactivity.setNew(true);
        projectactivity.setPrimaryKey(id);

        return projectactivity;
    }

    public projectactivity remove(int id)
        throws NoSuchprojectactivityException, SystemException {
        Session session = null;

        try {
            session = openSession();

            projectactivity projectactivity = (projectactivity) session.get(projectactivityImpl.class,
                    new Integer(id));

            if (projectactivity == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No projectactivity exists with the primary key " +
                        id);
                }

                throw new NoSuchprojectactivityException(
                    "No projectactivity exists with the primary key " + id);
            }

            return remove(projectactivity);
        } catch (NoSuchprojectactivityException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public projectactivity remove(projectactivity projectactivity)
        throws SystemException {
        for (ModelListener<projectactivity> listener : listeners) {
            listener.onBeforeRemove(projectactivity);
        }

        projectactivity = removeImpl(projectactivity);

        for (ModelListener<projectactivity> listener : listeners) {
            listener.onAfterRemove(projectactivity);
        }

        return projectactivity;
    }

    protected projectactivity removeImpl(projectactivity projectactivity)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (projectactivity.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(projectactivityImpl.class,
                        projectactivity.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(projectactivity);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
            projectactivityImpl.class, projectactivity.getPrimaryKey());

        return projectactivity;
    }

    /**
     * @deprecated Use <code>update(projectactivity projectactivity, boolean merge)</code>.
     */
    public projectactivity update(projectactivity projectactivity)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(projectactivity projectactivity) method. Use update(projectactivity projectactivity, boolean merge) instead.");
        }

        return update(projectactivity, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                projectactivity the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when projectactivity is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public projectactivity update(projectactivity projectactivity, boolean merge)
        throws SystemException {
        boolean isNew = projectactivity.isNew();

        for (ModelListener<projectactivity> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(projectactivity);
            } else {
                listener.onBeforeUpdate(projectactivity);
            }
        }

        projectactivity = updateImpl(projectactivity, merge);

        for (ModelListener<projectactivity> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(projectactivity);
            } else {
                listener.onAfterUpdate(projectactivity);
            }
        }

        return projectactivity;
    }

    public projectactivity updateImpl(
        com.example.service.model.projectactivity projectactivity, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, projectactivity, merge);

            projectactivity.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
            projectactivityImpl.class, projectactivity.getPrimaryKey(),
            projectactivity);

        return projectactivity;
    }

    public projectactivity findByPrimaryKey(int id)
        throws NoSuchprojectactivityException, SystemException {
        projectactivity projectactivity = fetchByPrimaryKey(id);

        if (projectactivity == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No projectactivity exists with the primary key " +
                    id);
            }

            throw new NoSuchprojectactivityException(
                "No projectactivity exists with the primary key " + id);
        }

        return projectactivity;
    }

    public projectactivity fetchByPrimaryKey(int id) throws SystemException {
        projectactivity projectactivity = (projectactivity) EntityCacheUtil.getResult(projectactivityModelImpl.ENTITY_CACHE_ENABLED,
                projectactivityImpl.class, id, this);

        if (projectactivity == null) {
            Session session = null;

            try {
                session = openSession();

                projectactivity = (projectactivity) session.get(projectactivityImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (projectactivity != null) {
                    cacheResult(projectactivity);
                }

                closeSession(session);
            }
        }

        return projectactivity;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<projectactivity> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<projectactivity> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<projectactivity> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<projectactivity> list = (List<projectactivity>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.projectactivity ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<projectactivity>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<projectactivity>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<projectactivity>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (projectactivity projectactivity : findAll()) {
            remove(projectactivity);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.projectactivity");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.projectactivity")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<projectactivity>> listenersList = new ArrayList<ModelListener<projectactivity>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<projectactivity>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
