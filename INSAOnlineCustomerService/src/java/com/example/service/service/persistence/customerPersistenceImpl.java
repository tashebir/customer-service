package com.example.service.service.persistence;

import com.example.service.NoSuchcustomerException;
import com.example.service.model.customer;
import com.example.service.model.impl.customerImpl;
import com.example.service.model.impl.customerModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class customerPersistenceImpl extends BasePersistenceImpl
    implements customerPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = customerImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
            customerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
            customerModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(customerPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(customer customer) {
        EntityCacheUtil.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
            customerImpl.class, customer.getPrimaryKey(), customer);
    }

    public void cacheResult(List<customer> customers) {
        for (customer customer : customers) {
            if (EntityCacheUtil.getResult(
                        customerModelImpl.ENTITY_CACHE_ENABLED,
                        customerImpl.class, customer.getPrimaryKey(), this) == null) {
                cacheResult(customer);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(customerImpl.class.getName());
        EntityCacheUtil.clearCache(customerImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public customer create(int id) {
        customer customer = new customerImpl();

        customer.setNew(true);
        customer.setPrimaryKey(id);

        return customer;
    }

    public customer remove(int id)
        throws NoSuchcustomerException, SystemException {
        Session session = null;

        try {
            session = openSession();

            customer customer = (customer) session.get(customerImpl.class,
                    new Integer(id));

            if (customer == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No customer exists with the primary key " + id);
                }

                throw new NoSuchcustomerException(
                    "No customer exists with the primary key " + id);
            }

            return remove(customer);
        } catch (NoSuchcustomerException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public customer remove(customer customer) throws SystemException {
        for (ModelListener<customer> listener : listeners) {
            listener.onBeforeRemove(customer);
        }

        customer = removeImpl(customer);

        for (ModelListener<customer> listener : listeners) {
            listener.onAfterRemove(customer);
        }

        return customer;
    }

    protected customer removeImpl(customer customer) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (customer.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(customerImpl.class,
                        customer.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(customer);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(customerModelImpl.ENTITY_CACHE_ENABLED,
            customerImpl.class, customer.getPrimaryKey());

        return customer;
    }

    /**
     * @deprecated Use <code>update(customer customer, boolean merge)</code>.
     */
    public customer update(customer customer) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(customer customer) method. Use update(customer customer, boolean merge) instead.");
        }

        return update(customer, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customer the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customer is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public customer update(customer customer, boolean merge)
        throws SystemException {
        boolean isNew = customer.isNew();

        for (ModelListener<customer> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(customer);
            } else {
                listener.onBeforeUpdate(customer);
            }
        }

        customer = updateImpl(customer, merge);

        for (ModelListener<customer> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(customer);
            } else {
                listener.onAfterUpdate(customer);
            }
        }

        return customer;
    }

    public customer updateImpl(com.example.service.model.customer customer,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, customer, merge);

            customer.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
            customerImpl.class, customer.getPrimaryKey(), customer);

        return customer;
    }

    public customer findByPrimaryKey(int id)
        throws NoSuchcustomerException, SystemException {
        customer customer = fetchByPrimaryKey(id);

        if (customer == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No customer exists with the primary key " + id);
            }

            throw new NoSuchcustomerException(
                "No customer exists with the primary key " + id);
        }

        return customer;
    }

    public customer fetchByPrimaryKey(int id) throws SystemException {
        customer customer = (customer) EntityCacheUtil.getResult(customerModelImpl.ENTITY_CACHE_ENABLED,
                customerImpl.class, id, this);

        if (customer == null) {
            Session session = null;

            try {
                session = openSession();

                customer = (customer) session.get(customerImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (customer != null) {
                    cacheResult(customer);
                }

                closeSession(session);
            }
        }

        return customer;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<customer> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<customer> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    public List<customer> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<customer> list = (List<customer>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.customer ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<customer>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<customer>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<customer>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (customer customer : findAll()) {
            remove(customer);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.customer");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.customer")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<customer>> listenersList = new ArrayList<ModelListener<customer>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<customer>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
