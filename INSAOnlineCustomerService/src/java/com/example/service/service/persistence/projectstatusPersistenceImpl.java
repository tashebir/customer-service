package com.example.service.service.persistence;

import com.example.service.NoSuchprojectstatusException;
import com.example.service.model.impl.projectstatusImpl;
import com.example.service.model.impl.projectstatusModelImpl;
import com.example.service.model.projectstatus;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class projectstatusPersistenceImpl extends BasePersistenceImpl
    implements projectstatusPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = projectstatusImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
            projectstatusModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
            projectstatusModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(projectstatusPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(projectstatus projectstatus) {
        EntityCacheUtil.putResult(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
            projectstatusImpl.class, projectstatus.getPrimaryKey(),
            projectstatus);
    }

    public void cacheResult(List<projectstatus> projectstatuses) {
        for (projectstatus projectstatus : projectstatuses) {
            if (EntityCacheUtil.getResult(
                        projectstatusModelImpl.ENTITY_CACHE_ENABLED,
                        projectstatusImpl.class, projectstatus.getPrimaryKey(),
                        this) == null) {
                cacheResult(projectstatus);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(projectstatusImpl.class.getName());
        EntityCacheUtil.clearCache(projectstatusImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public projectstatus create(int id) {
        projectstatus projectstatus = new projectstatusImpl();

        projectstatus.setNew(true);
        projectstatus.setPrimaryKey(id);

        return projectstatus;
    }

    public projectstatus remove(int id)
        throws NoSuchprojectstatusException, SystemException {
        Session session = null;

        try {
            session = openSession();

            projectstatus projectstatus = (projectstatus) session.get(projectstatusImpl.class,
                    new Integer(id));

            if (projectstatus == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No projectstatus exists with the primary key " +
                        id);
                }

                throw new NoSuchprojectstatusException(
                    "No projectstatus exists with the primary key " + id);
            }

            return remove(projectstatus);
        } catch (NoSuchprojectstatusException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public projectstatus remove(projectstatus projectstatus)
        throws SystemException {
        for (ModelListener<projectstatus> listener : listeners) {
            listener.onBeforeRemove(projectstatus);
        }

        projectstatus = removeImpl(projectstatus);

        for (ModelListener<projectstatus> listener : listeners) {
            listener.onAfterRemove(projectstatus);
        }

        return projectstatus;
    }

    protected projectstatus removeImpl(projectstatus projectstatus)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (projectstatus.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(projectstatusImpl.class,
                        projectstatus.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(projectstatus);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
            projectstatusImpl.class, projectstatus.getPrimaryKey());

        return projectstatus;
    }

    /**
     * @deprecated Use <code>update(projectstatus projectstatus, boolean merge)</code>.
     */
    public projectstatus update(projectstatus projectstatus)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(projectstatus projectstatus) method. Use update(projectstatus projectstatus, boolean merge) instead.");
        }

        return update(projectstatus, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                projectstatus the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when projectstatus is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public projectstatus update(projectstatus projectstatus, boolean merge)
        throws SystemException {
        boolean isNew = projectstatus.isNew();

        for (ModelListener<projectstatus> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(projectstatus);
            } else {
                listener.onBeforeUpdate(projectstatus);
            }
        }

        projectstatus = updateImpl(projectstatus, merge);

        for (ModelListener<projectstatus> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(projectstatus);
            } else {
                listener.onAfterUpdate(projectstatus);
            }
        }

        return projectstatus;
    }

    public projectstatus updateImpl(
        com.example.service.model.projectstatus projectstatus, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, projectstatus, merge);

            projectstatus.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
            projectstatusImpl.class, projectstatus.getPrimaryKey(),
            projectstatus);

        return projectstatus;
    }

    public projectstatus findByPrimaryKey(int id)
        throws NoSuchprojectstatusException, SystemException {
        projectstatus projectstatus = fetchByPrimaryKey(id);

        if (projectstatus == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No projectstatus exists with the primary key " + id);
            }

            throw new NoSuchprojectstatusException(
                "No projectstatus exists with the primary key " + id);
        }

        return projectstatus;
    }

    public projectstatus fetchByPrimaryKey(int id) throws SystemException {
        projectstatus projectstatus = (projectstatus) EntityCacheUtil.getResult(projectstatusModelImpl.ENTITY_CACHE_ENABLED,
                projectstatusImpl.class, id, this);

        if (projectstatus == null) {
            Session session = null;

            try {
                session = openSession();

                projectstatus = (projectstatus) session.get(projectstatusImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (projectstatus != null) {
                    cacheResult(projectstatus);
                }

                closeSession(session);
            }
        }

        return projectstatus;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<projectstatus> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<projectstatus> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<projectstatus> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<projectstatus> list = (List<projectstatus>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.projectstatus ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<projectstatus>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<projectstatus>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<projectstatus>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (projectstatus projectstatus : findAll()) {
            remove(projectstatus);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.projectstatus");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.projectstatus")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<projectstatus>> listenersList = new ArrayList<ModelListener<projectstatus>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<projectstatus>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
