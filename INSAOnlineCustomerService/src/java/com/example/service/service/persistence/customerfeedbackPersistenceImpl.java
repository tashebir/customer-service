package com.example.service.service.persistence;

import com.example.service.NoSuchcustomerfeedbackException;
import com.example.service.model.customerfeedback;
import com.example.service.model.impl.customerfeedbackImpl;
import com.example.service.model.impl.customerfeedbackModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class customerfeedbackPersistenceImpl extends BasePersistenceImpl
    implements customerfeedbackPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = customerfeedbackImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
            customerfeedbackModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
            customerfeedbackModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(customerfeedbackPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(customerfeedback customerfeedback) {
        EntityCacheUtil.putResult(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
            customerfeedbackImpl.class, customerfeedback.getPrimaryKey(),
            customerfeedback);
    }

    public void cacheResult(List<customerfeedback> customerfeedbacks) {
        for (customerfeedback customerfeedback : customerfeedbacks) {
            if (EntityCacheUtil.getResult(
                        customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
                        customerfeedbackImpl.class,
                        customerfeedback.getPrimaryKey(), this) == null) {
                cacheResult(customerfeedback);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(customerfeedbackImpl.class.getName());
        EntityCacheUtil.clearCache(customerfeedbackImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public customerfeedback create(int id) {
        customerfeedback customerfeedback = new customerfeedbackImpl();

        customerfeedback.setNew(true);
        customerfeedback.setPrimaryKey(id);

        return customerfeedback;
    }

    public customerfeedback remove(int id)
        throws NoSuchcustomerfeedbackException, SystemException {
        Session session = null;

        try {
            session = openSession();

            customerfeedback customerfeedback = (customerfeedback) session.get(customerfeedbackImpl.class,
                    new Integer(id));

            if (customerfeedback == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(
                        "No customerfeedback exists with the primary key " +
                        id);
                }

                throw new NoSuchcustomerfeedbackException(
                    "No customerfeedback exists with the primary key " + id);
            }

            return remove(customerfeedback);
        } catch (NoSuchcustomerfeedbackException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public customerfeedback remove(customerfeedback customerfeedback)
        throws SystemException {
        for (ModelListener<customerfeedback> listener : listeners) {
            listener.onBeforeRemove(customerfeedback);
        }

        customerfeedback = removeImpl(customerfeedback);

        for (ModelListener<customerfeedback> listener : listeners) {
            listener.onAfterRemove(customerfeedback);
        }

        return customerfeedback;
    }

    protected customerfeedback removeImpl(customerfeedback customerfeedback)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (customerfeedback.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(customerfeedbackImpl.class,
                        customerfeedback.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(customerfeedback);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
            customerfeedbackImpl.class, customerfeedback.getPrimaryKey());

        return customerfeedback;
    }

    /**
     * @deprecated Use <code>update(customerfeedback customerfeedback, boolean merge)</code>.
     */
    public customerfeedback update(customerfeedback customerfeedback)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(customerfeedback customerfeedback) method. Use update(customerfeedback customerfeedback, boolean merge) instead.");
        }

        return update(customerfeedback, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                customerfeedback the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when customerfeedback is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public customerfeedback update(customerfeedback customerfeedback,
        boolean merge) throws SystemException {
        boolean isNew = customerfeedback.isNew();

        for (ModelListener<customerfeedback> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(customerfeedback);
            } else {
                listener.onBeforeUpdate(customerfeedback);
            }
        }

        customerfeedback = updateImpl(customerfeedback, merge);

        for (ModelListener<customerfeedback> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(customerfeedback);
            } else {
                listener.onAfterUpdate(customerfeedback);
            }
        }

        return customerfeedback;
    }

    public customerfeedback updateImpl(
        com.example.service.model.customerfeedback customerfeedback,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, customerfeedback, merge);

            customerfeedback.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
            customerfeedbackImpl.class, customerfeedback.getPrimaryKey(),
            customerfeedback);

        return customerfeedback;
    }

    public customerfeedback findByPrimaryKey(int id)
        throws NoSuchcustomerfeedbackException, SystemException {
        customerfeedback customerfeedback = fetchByPrimaryKey(id);

        if (customerfeedback == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No customerfeedback exists with the primary key " +
                    id);
            }

            throw new NoSuchcustomerfeedbackException(
                "No customerfeedback exists with the primary key " + id);
        }

        return customerfeedback;
    }

    public customerfeedback fetchByPrimaryKey(int id) throws SystemException {
        customerfeedback customerfeedback = (customerfeedback) EntityCacheUtil.getResult(customerfeedbackModelImpl.ENTITY_CACHE_ENABLED,
                customerfeedbackImpl.class, id, this);

        if (customerfeedback == null) {
            Session session = null;

            try {
                session = openSession();

                customerfeedback = (customerfeedback) session.get(customerfeedbackImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (customerfeedback != null) {
                    cacheResult(customerfeedback);
                }

                closeSession(session);
            }
        }

        return customerfeedback;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<customerfeedback> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<customerfeedback> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<customerfeedback> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<customerfeedback> list = (List<customerfeedback>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.customerfeedback ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<customerfeedback>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<customerfeedback>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<customerfeedback>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (customerfeedback customerfeedback : findAll()) {
            remove(customerfeedback);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.customerfeedback");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.customerfeedback")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<customerfeedback>> listenersList = new ArrayList<ModelListener<customerfeedback>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<customerfeedback>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
