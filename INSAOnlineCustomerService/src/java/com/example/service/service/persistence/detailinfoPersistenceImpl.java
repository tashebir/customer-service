package com.example.service.service.persistence;

import com.example.service.NoSuchdetailinfoException;
import com.example.service.model.detailinfo;
import com.example.service.model.impl.detailinfoImpl;
import com.example.service.model.impl.detailinfoModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class detailinfoPersistenceImpl extends BasePersistenceImpl
    implements detailinfoPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = detailinfoImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
            detailinfoModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
            detailinfoModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(detailinfoPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(detailinfo detailinfo) {
        EntityCacheUtil.putResult(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
            detailinfoImpl.class, detailinfo.getPrimaryKey(), detailinfo);
    }

    public void cacheResult(List<detailinfo> detailinfos) {
        for (detailinfo detailinfo : detailinfos) {
            if (EntityCacheUtil.getResult(
                        detailinfoModelImpl.ENTITY_CACHE_ENABLED,
                        detailinfoImpl.class, detailinfo.getPrimaryKey(), this) == null) {
                cacheResult(detailinfo);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(detailinfoImpl.class.getName());
        EntityCacheUtil.clearCache(detailinfoImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public detailinfo create(int id) {
        detailinfo detailinfo = new detailinfoImpl();

        detailinfo.setNew(true);
        detailinfo.setPrimaryKey(id);

        return detailinfo;
    }

    public detailinfo remove(int id)
        throws NoSuchdetailinfoException, SystemException {
        Session session = null;

        try {
            session = openSession();

            detailinfo detailinfo = (detailinfo) session.get(detailinfoImpl.class,
                    new Integer(id));

            if (detailinfo == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No detailinfo exists with the primary key " +
                        id);
                }

                throw new NoSuchdetailinfoException(
                    "No detailinfo exists with the primary key " + id);
            }

            return remove(detailinfo);
        } catch (NoSuchdetailinfoException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public detailinfo remove(detailinfo detailinfo) throws SystemException {
        for (ModelListener<detailinfo> listener : listeners) {
            listener.onBeforeRemove(detailinfo);
        }

        detailinfo = removeImpl(detailinfo);

        for (ModelListener<detailinfo> listener : listeners) {
            listener.onAfterRemove(detailinfo);
        }

        return detailinfo;
    }

    protected detailinfo removeImpl(detailinfo detailinfo)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (detailinfo.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(detailinfoImpl.class,
                        detailinfo.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(detailinfo);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
            detailinfoImpl.class, detailinfo.getPrimaryKey());

        return detailinfo;
    }

    /**
     * @deprecated Use <code>update(detailinfo detailinfo, boolean merge)</code>.
     */
    public detailinfo update(detailinfo detailinfo) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(detailinfo detailinfo) method. Use update(detailinfo detailinfo, boolean merge) instead.");
        }

        return update(detailinfo, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                detailinfo the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when detailinfo is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public detailinfo update(detailinfo detailinfo, boolean merge)
        throws SystemException {
        boolean isNew = detailinfo.isNew();

        for (ModelListener<detailinfo> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(detailinfo);
            } else {
                listener.onBeforeUpdate(detailinfo);
            }
        }

        detailinfo = updateImpl(detailinfo, merge);

        for (ModelListener<detailinfo> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(detailinfo);
            } else {
                listener.onAfterUpdate(detailinfo);
            }
        }

        return detailinfo;
    }

    public detailinfo updateImpl(
        com.example.service.model.detailinfo detailinfo, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, detailinfo, merge);

            detailinfo.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
            detailinfoImpl.class, detailinfo.getPrimaryKey(), detailinfo);

        return detailinfo;
    }

    public detailinfo findByPrimaryKey(int id)
        throws NoSuchdetailinfoException, SystemException {
        detailinfo detailinfo = fetchByPrimaryKey(id);

        if (detailinfo == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No detailinfo exists with the primary key " + id);
            }

            throw new NoSuchdetailinfoException(
                "No detailinfo exists with the primary key " + id);
        }

        return detailinfo;
    }

    public detailinfo fetchByPrimaryKey(int id) throws SystemException {
        detailinfo detailinfo = (detailinfo) EntityCacheUtil.getResult(detailinfoModelImpl.ENTITY_CACHE_ENABLED,
                detailinfoImpl.class, id, this);

        if (detailinfo == null) {
            Session session = null;

            try {
                session = openSession();

                detailinfo = (detailinfo) session.get(detailinfoImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (detailinfo != null) {
                    cacheResult(detailinfo);
                }

                closeSession(session);
            }
        }

        return detailinfo;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<detailinfo> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<detailinfo> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<detailinfo> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<detailinfo> list = (List<detailinfo>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.detailinfo ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<detailinfo>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<detailinfo>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<detailinfo>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (detailinfo detailinfo : findAll()) {
            remove(detailinfo);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.detailinfo");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.detailinfo")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<detailinfo>> listenersList = new ArrayList<ModelListener<detailinfo>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<detailinfo>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
