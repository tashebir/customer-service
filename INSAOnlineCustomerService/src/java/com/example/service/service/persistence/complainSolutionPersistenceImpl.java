package com.example.service.service.persistence;

import com.example.service.NoSuchcomplainSolutionException;
import com.example.service.model.complainSolution;
import com.example.service.model.impl.complainSolutionImpl;
import com.example.service.model.impl.complainSolutionModelImpl;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class complainSolutionPersistenceImpl extends BasePersistenceImpl
    implements complainSolutionPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = complainSolutionImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
            complainSolutionModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
            complainSolutionModelImpl.FINDER_CACHE_ENABLED,
            FINDER_CLASS_NAME_LIST, "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(complainSolutionPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(complainSolution complainSolution) {
        EntityCacheUtil.putResult(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
            complainSolutionImpl.class, complainSolution.getPrimaryKey(),
            complainSolution);
    }

    public void cacheResult(List<complainSolution> complainSolutions) {
        for (complainSolution complainSolution : complainSolutions) {
            if (EntityCacheUtil.getResult(
                        complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
                        complainSolutionImpl.class,
                        complainSolution.getPrimaryKey(), this) == null) {
                cacheResult(complainSolution);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(complainSolutionImpl.class.getName());
        EntityCacheUtil.clearCache(complainSolutionImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public complainSolution create(int solutionId) {
        complainSolution complainSolution = new complainSolutionImpl();

        complainSolution.setNew(true);
        complainSolution.setPrimaryKey(solutionId);

        return complainSolution;
    }

    public complainSolution remove(int solutionId)
        throws NoSuchcomplainSolutionException, SystemException {
        Session session = null;

        try {
            session = openSession();

            complainSolution complainSolution = (complainSolution) session.get(complainSolutionImpl.class,
                    new Integer(solutionId));

            if (complainSolution == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(
                        "No complainSolution exists with the primary key " +
                        solutionId);
                }

                throw new NoSuchcomplainSolutionException(
                    "No complainSolution exists with the primary key " +
                    solutionId);
            }

            return remove(complainSolution);
        } catch (NoSuchcomplainSolutionException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public complainSolution remove(complainSolution complainSolution)
        throws SystemException {
        for (ModelListener<complainSolution> listener : listeners) {
            listener.onBeforeRemove(complainSolution);
        }

        complainSolution = removeImpl(complainSolution);

        for (ModelListener<complainSolution> listener : listeners) {
            listener.onAfterRemove(complainSolution);
        }

        return complainSolution;
    }

    protected complainSolution removeImpl(complainSolution complainSolution)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (complainSolution.isCachedModel() ||
                    BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(complainSolutionImpl.class,
                        complainSolution.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(complainSolution);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
            complainSolutionImpl.class, complainSolution.getPrimaryKey());

        return complainSolution;
    }

    /**
     * @deprecated Use <code>update(complainSolution complainSolution, boolean merge)</code>.
     */
    public complainSolution update(complainSolution complainSolution)
        throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(complainSolution complainSolution) method. Use update(complainSolution complainSolution, boolean merge) instead.");
        }

        return update(complainSolution, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                complainSolution the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when complainSolution is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public complainSolution update(complainSolution complainSolution,
        boolean merge) throws SystemException {
        boolean isNew = complainSolution.isNew();

        for (ModelListener<complainSolution> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(complainSolution);
            } else {
                listener.onBeforeUpdate(complainSolution);
            }
        }

        complainSolution = updateImpl(complainSolution, merge);

        for (ModelListener<complainSolution> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(complainSolution);
            } else {
                listener.onAfterUpdate(complainSolution);
            }
        }

        return complainSolution;
    }

    public complainSolution updateImpl(
        com.example.service.model.complainSolution complainSolution,
        boolean merge) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, complainSolution, merge);

            complainSolution.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
            complainSolutionImpl.class, complainSolution.getPrimaryKey(),
            complainSolution);

        return complainSolution;
    }

    public complainSolution findByPrimaryKey(int solutionId)
        throws NoSuchcomplainSolutionException, SystemException {
        complainSolution complainSolution = fetchByPrimaryKey(solutionId);

        if (complainSolution == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No complainSolution exists with the primary key " +
                    solutionId);
            }

            throw new NoSuchcomplainSolutionException(
                "No complainSolution exists with the primary key " +
                solutionId);
        }

        return complainSolution;
    }

    public complainSolution fetchByPrimaryKey(int solutionId)
        throws SystemException {
        complainSolution complainSolution = (complainSolution) EntityCacheUtil.getResult(complainSolutionModelImpl.ENTITY_CACHE_ENABLED,
                complainSolutionImpl.class, solutionId, this);

        if (complainSolution == null) {
            Session session = null;

            try {
                session = openSession();

                complainSolution = (complainSolution) session.get(complainSolutionImpl.class,
                        new Integer(solutionId));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (complainSolution != null) {
                    cacheResult(complainSolution);
                }

                closeSession(session);
            }
        }

        return complainSolution;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<complainSolution> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<complainSolution> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<complainSolution> findAll(int start, int end,
        OrderByComparator obc) throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<complainSolution> list = (List<complainSolution>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.complainSolution ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<complainSolution>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<complainSolution>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<complainSolution>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (complainSolution complainSolution : findAll()) {
            remove(complainSolution);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.complainSolution");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.complainSolution")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<complainSolution>> listenersList = new ArrayList<ModelListener<complainSolution>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<complainSolution>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
