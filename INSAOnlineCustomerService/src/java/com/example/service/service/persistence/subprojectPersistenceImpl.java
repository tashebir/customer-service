package com.example.service.service.persistence;

import com.example.service.NoSuchsubprojectException;
import com.example.service.model.impl.subprojectImpl;
import com.example.service.model.impl.subprojectModelImpl;
import com.example.service.model.subproject;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class subprojectPersistenceImpl extends BasePersistenceImpl
    implements subprojectPersistence {
    public static final String FINDER_CLASS_NAME_ENTITY = subprojectImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(subprojectModelImpl.ENTITY_CACHE_ENABLED,
            subprojectModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(subprojectModelImpl.ENTITY_CACHE_ENABLED,
            subprojectModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(subprojectPersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(subproject subproject) {
        EntityCacheUtil.putResult(subprojectModelImpl.ENTITY_CACHE_ENABLED,
            subprojectImpl.class, subproject.getPrimaryKey(), subproject);
    }

    public void cacheResult(List<subproject> subprojects) {
        for (subproject subproject : subprojects) {
            if (EntityCacheUtil.getResult(
                        subprojectModelImpl.ENTITY_CACHE_ENABLED,
                        subprojectImpl.class, subproject.getPrimaryKey(), this) == null) {
                cacheResult(subproject);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(subprojectImpl.class.getName());
        EntityCacheUtil.clearCache(subprojectImpl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public subproject create(int id) {
        subproject subproject = new subprojectImpl();

        subproject.setNew(true);
        subproject.setPrimaryKey(id);

        return subproject;
    }

    public subproject remove(int id)
        throws NoSuchsubprojectException, SystemException {
        Session session = null;

        try {
            session = openSession();

            subproject subproject = (subproject) session.get(subprojectImpl.class,
                    new Integer(id));

            if (subproject == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No subproject exists with the primary key " +
                        id);
                }

                throw new NoSuchsubprojectException(
                    "No subproject exists with the primary key " + id);
            }

            return remove(subproject);
        } catch (NoSuchsubprojectException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public subproject remove(subproject subproject) throws SystemException {
        for (ModelListener<subproject> listener : listeners) {
            listener.onBeforeRemove(subproject);
        }

        subproject = removeImpl(subproject);

        for (ModelListener<subproject> listener : listeners) {
            listener.onAfterRemove(subproject);
        }

        return subproject;
    }

    protected subproject removeImpl(subproject subproject)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (subproject.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(subprojectImpl.class,
                        subproject.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(subproject);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(subprojectModelImpl.ENTITY_CACHE_ENABLED,
            subprojectImpl.class, subproject.getPrimaryKey());

        return subproject;
    }

    /**
     * @deprecated Use <code>update(subproject subproject, boolean merge)</code>.
     */
    public subproject update(subproject subproject) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(subproject subproject) method. Use update(subproject subproject, boolean merge) instead.");
        }

        return update(subproject, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                subproject the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when subproject is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public subproject update(subproject subproject, boolean merge)
        throws SystemException {
        boolean isNew = subproject.isNew();

        for (ModelListener<subproject> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(subproject);
            } else {
                listener.onBeforeUpdate(subproject);
            }
        }

        subproject = updateImpl(subproject, merge);

        for (ModelListener<subproject> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(subproject);
            } else {
                listener.onAfterUpdate(subproject);
            }
        }

        return subproject;
    }

    public subproject updateImpl(
        com.example.service.model.subproject subproject, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, subproject, merge);

            subproject.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(subprojectModelImpl.ENTITY_CACHE_ENABLED,
            subprojectImpl.class, subproject.getPrimaryKey(), subproject);

        return subproject;
    }

    public subproject findByPrimaryKey(int id)
        throws NoSuchsubprojectException, SystemException {
        subproject subproject = fetchByPrimaryKey(id);

        if (subproject == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No subproject exists with the primary key " + id);
            }

            throw new NoSuchsubprojectException(
                "No subproject exists with the primary key " + id);
        }

        return subproject;
    }

    public subproject fetchByPrimaryKey(int id) throws SystemException {
        subproject subproject = (subproject) EntityCacheUtil.getResult(subprojectModelImpl.ENTITY_CACHE_ENABLED,
                subprojectImpl.class, id, this);

        if (subproject == null) {
            Session session = null;

            try {
                session = openSession();

                subproject = (subproject) session.get(subprojectImpl.class,
                        new Integer(id));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (subproject != null) {
                    cacheResult(subproject);
                }

                closeSession(session);
            }
        }

        return subproject;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<subproject> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<subproject> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    public List<subproject> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<subproject> list = (List<subproject>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.subproject ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<subproject>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<subproject>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<subproject>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (subproject subproject : findAll()) {
            remove(subproject);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.subproject");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.subproject")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<subproject>> listenersList = new ArrayList<ModelListener<subproject>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<subproject>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
