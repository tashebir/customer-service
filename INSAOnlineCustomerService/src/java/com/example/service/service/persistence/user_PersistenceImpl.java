package com.example.service.service.persistence;

import com.example.service.NoSuchuser_Exception;
import com.example.service.model.impl.user_Impl;
import com.example.service.model.impl.user_ModelImpl;
import com.example.service.model.user_;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistry;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class user_PersistenceImpl extends BasePersistenceImpl
    implements user_Persistence {
    public static final String FINDER_CLASS_NAME_ENTITY = user_Impl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
        ".List";
    public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(user_ModelImpl.ENTITY_CACHE_ENABLED,
            user_ModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(user_ModelImpl.ENTITY_CACHE_ENABLED,
            user_ModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
            "countAll", new String[0]);
    private static Log _log = LogFactoryUtil.getLog(user_PersistenceImpl.class);
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected com.example.service.service.persistence.customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected com.example.service.service.persistence.customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected com.example.service.service.persistence.subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected com.example.service.service.persistence.user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected com.example.service.service.persistence.subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected com.example.service.service.persistence.customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected com.example.service.service.persistence.projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected com.example.service.service.persistence.projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected com.example.service.service.persistence.complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected com.example.service.service.persistence.customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected com.example.service.service.persistence.detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected com.example.service.service.persistence.complainReportPersistence complainReportPersistence;

    public void cacheResult(user_ user_) {
        EntityCacheUtil.putResult(user_ModelImpl.ENTITY_CACHE_ENABLED,
            user_Impl.class, user_.getPrimaryKey(), user_);
    }

    public void cacheResult(List<user_> user_s) {
        for (user_ user_ : user_s) {
            if (EntityCacheUtil.getResult(user_ModelImpl.ENTITY_CACHE_ENABLED,
                        user_Impl.class, user_.getPrimaryKey(), this) == null) {
                cacheResult(user_);
            }
        }
    }

    public void clearCache() {
        CacheRegistry.clear(user_Impl.class.getName());
        EntityCacheUtil.clearCache(user_Impl.class.getName());
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
    }

    public user_ create(int userId) {
        user_ user_ = new user_Impl();

        user_.setNew(true);
        user_.setPrimaryKey(userId);

        return user_;
    }

    public user_ remove(int userId)
        throws NoSuchuser_Exception, SystemException {
        Session session = null;

        try {
            session = openSession();

            user_ user_ = (user_) session.get(user_Impl.class,
                    new Integer(userId));

            if (user_ == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn("No user_ exists with the primary key " + userId);
                }

                throw new NoSuchuser_Exception(
                    "No user_ exists with the primary key " + userId);
            }

            return remove(user_);
        } catch (NoSuchuser_Exception nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public user_ remove(user_ user_) throws SystemException {
        for (ModelListener<user_> listener : listeners) {
            listener.onBeforeRemove(user_);
        }

        user_ = removeImpl(user_);

        for (ModelListener<user_> listener : listeners) {
            listener.onAfterRemove(user_);
        }

        return user_;
    }

    protected user_ removeImpl(user_ user_) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            if (user_.isCachedModel() || BatchSessionUtil.isEnabled()) {
                Object staleObject = session.get(user_Impl.class,
                        user_.getPrimaryKeyObj());

                if (staleObject != null) {
                    session.evict(staleObject);
                }
            }

            session.delete(user_);

            session.flush();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.removeResult(user_ModelImpl.ENTITY_CACHE_ENABLED,
            user_Impl.class, user_.getPrimaryKey());

        return user_;
    }

    /**
     * @deprecated Use <code>update(user_ user_, boolean merge)</code>.
     */
    public user_ update(user_ user_) throws SystemException {
        if (_log.isWarnEnabled()) {
            _log.warn(
                "Using the deprecated update(user_ user_) method. Use update(user_ user_, boolean merge) instead.");
        }

        return update(user_, false);
    }

    /**
     * Add, update, or merge, the entity. This method also calls the model
     * listeners to trigger the proper events associated with adding, deleting,
     * or updating an entity.
     *
     * @param                user_ the entity to add, update, or merge
     * @param                merge boolean value for whether to merge the entity. The
     *                                default value is false. Setting merge to true is more
     *                                expensive and should only be true when user_ is
     *                                transient. See LEP-5473 for a detailed discussion of this
     *                                method.
     * @return                true if the portlet can be displayed via Ajax
     */
    public user_ update(user_ user_, boolean merge) throws SystemException {
        boolean isNew = user_.isNew();

        for (ModelListener<user_> listener : listeners) {
            if (isNew) {
                listener.onBeforeCreate(user_);
            } else {
                listener.onBeforeUpdate(user_);
            }
        }

        user_ = updateImpl(user_, merge);

        for (ModelListener<user_> listener : listeners) {
            if (isNew) {
                listener.onAfterCreate(user_);
            } else {
                listener.onAfterUpdate(user_);
            }
        }

        return user_;
    }

    public user_ updateImpl(com.example.service.model.user_ user_, boolean merge)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, user_, merge);

            user_.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

        EntityCacheUtil.putResult(user_ModelImpl.ENTITY_CACHE_ENABLED,
            user_Impl.class, user_.getPrimaryKey(), user_);

        return user_;
    }

    public user_ findByPrimaryKey(int userId)
        throws NoSuchuser_Exception, SystemException {
        user_ user_ = fetchByPrimaryKey(userId);

        if (user_ == null) {
            if (_log.isWarnEnabled()) {
                _log.warn("No user_ exists with the primary key " + userId);
            }

            throw new NoSuchuser_Exception(
                "No user_ exists with the primary key " + userId);
        }

        return user_;
    }

    public user_ fetchByPrimaryKey(int userId) throws SystemException {
        user_ user_ = (user_) EntityCacheUtil.getResult(user_ModelImpl.ENTITY_CACHE_ENABLED,
                user_Impl.class, userId, this);

        if (user_ == null) {
            Session session = null;

            try {
                session = openSession();

                user_ = (user_) session.get(user_Impl.class, new Integer(userId));
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (user_ != null) {
                    cacheResult(user_);
                }

                closeSession(session);
            }
        }

        return user_;
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<Object> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        Session session = null;

        try {
            session = openSession();

            dynamicQuery.setLimit(start, end);

            dynamicQuery.compile(session);

            return dynamicQuery.list();
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    public List<user_> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    public List<user_> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    public List<user_> findAll(int start, int end, OrderByComparator obc)
        throws SystemException {
        Object[] finderArgs = new Object[] {
                String.valueOf(start), String.valueOf(end), String.valueOf(obc)
            };

        List<user_> list = (List<user_>) FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
                finderArgs, this);

        if (list == null) {
            Session session = null;

            try {
                session = openSession();

                StringBuilder query = new StringBuilder();

                query.append("FROM com.example.service.model.user_ ");

                if (obc != null) {
                    query.append("ORDER BY ");
                    query.append(obc.getOrderBy());
                }

                Query q = session.createQuery(query.toString());

                if (obc == null) {
                    list = (List<user_>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);
                } else {
                    list = (List<user_>) QueryUtil.list(q, getDialect(), start,
                            end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    list = new ArrayList<user_>();
                }

                cacheResult(list);

                FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs, list);

                closeSession(session);
            }
        }

        return list;
    }

    public void removeAll() throws SystemException {
        for (user_ user_ : findAll()) {
            remove(user_);
        }
    }

    public int countAll() throws SystemException {
        Object[] finderArgs = new Object[0];

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                finderArgs, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(
                        "SELECT COUNT(*) FROM com.example.service.model.user_");

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
                    count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.example.service.model.user_")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<user_>> listenersList = new ArrayList<ModelListener<user_>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<user_>) Class.forName(
                            listenerClassName).newInstance());
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }
}
