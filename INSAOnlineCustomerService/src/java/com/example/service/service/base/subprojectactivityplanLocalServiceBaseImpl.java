package com.example.service.service.base;

import com.example.service.model.subprojectactivityplan;
import com.example.service.service.complainReportLocalService;
import com.example.service.service.complainReportService;
import com.example.service.service.complainSolutionLocalService;
import com.example.service.service.complainSolutionService;
import com.example.service.service.customerLocalService;
import com.example.service.service.customerService;
import com.example.service.service.customercomplainLocalService;
import com.example.service.service.customercomplainService;
import com.example.service.service.customerfeedbackLocalService;
import com.example.service.service.customerfeedbackService;
import com.example.service.service.customerprojectLocalService;
import com.example.service.service.customerprojectService;
import com.example.service.service.detailinfoLocalService;
import com.example.service.service.detailinfoService;
import com.example.service.service.persistence.complainReportPersistence;
import com.example.service.service.persistence.complainSolutionPersistence;
import com.example.service.service.persistence.customerPersistence;
import com.example.service.service.persistence.customercomplainPersistence;
import com.example.service.service.persistence.customerfeedbackPersistence;
import com.example.service.service.persistence.customerprojectPersistence;
import com.example.service.service.persistence.detailinfoPersistence;
import com.example.service.service.persistence.projectactivityPersistence;
import com.example.service.service.persistence.projectstatusPersistence;
import com.example.service.service.persistence.subprojectPersistence;
import com.example.service.service.persistence.subprojectactivityplanPersistence;
import com.example.service.service.persistence.user_Persistence;
import com.example.service.service.projectactivityLocalService;
import com.example.service.service.projectactivityService;
import com.example.service.service.projectstatusLocalService;
import com.example.service.service.projectstatusService;
import com.example.service.service.subprojectLocalService;
import com.example.service.service.subprojectService;
import com.example.service.service.subprojectactivityplanLocalService;
import com.example.service.service.subprojectactivityplanService;
import com.example.service.service.user_LocalService;
import com.example.service.service.user_Service;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.util.PortalUtil;

import java.util.List;


public abstract class subprojectactivityplanLocalServiceBaseImpl
    implements subprojectactivityplanLocalService {
    @BeanReference(name = "com.example.service.service.customerprojectLocalService.impl")
    protected customerprojectLocalService customerprojectLocalService;
    @BeanReference(name = "com.example.service.service.customerprojectService.impl")
    protected customerprojectService customerprojectService;
    @BeanReference(name = "com.example.service.service.persistence.customerprojectPersistence.impl")
    protected customerprojectPersistence customerprojectPersistence;
    @BeanReference(name = "com.example.service.service.customerLocalService.impl")
    protected customerLocalService customerLocalService;
    @BeanReference(name = "com.example.service.service.customerService.impl")
    protected customerService customerService;
    @BeanReference(name = "com.example.service.service.persistence.customerPersistence.impl")
    protected customerPersistence customerPersistence;
    @BeanReference(name = "com.example.service.service.subprojectactivityplanLocalService.impl")
    protected subprojectactivityplanLocalService subprojectactivityplanLocalService;
    @BeanReference(name = "com.example.service.service.subprojectactivityplanService.impl")
    protected subprojectactivityplanService subprojectactivityplanService;
    @BeanReference(name = "com.example.service.service.persistence.subprojectactivityplanPersistence.impl")
    protected subprojectactivityplanPersistence subprojectactivityplanPersistence;
    @BeanReference(name = "com.example.service.service.user_LocalService.impl")
    protected user_LocalService user_LocalService;
    @BeanReference(name = "com.example.service.service.user_Service.impl")
    protected user_Service user_Service;
    @BeanReference(name = "com.example.service.service.persistence.user_Persistence.impl")
    protected user_Persistence user_Persistence;
    @BeanReference(name = "com.example.service.service.subprojectLocalService.impl")
    protected subprojectLocalService subprojectLocalService;
    @BeanReference(name = "com.example.service.service.subprojectService.impl")
    protected subprojectService subprojectService;
    @BeanReference(name = "com.example.service.service.persistence.subprojectPersistence.impl")
    protected subprojectPersistence subprojectPersistence;
    @BeanReference(name = "com.example.service.service.customercomplainLocalService.impl")
    protected customercomplainLocalService customercomplainLocalService;
    @BeanReference(name = "com.example.service.service.customercomplainService.impl")
    protected customercomplainService customercomplainService;
    @BeanReference(name = "com.example.service.service.persistence.customercomplainPersistence.impl")
    protected customercomplainPersistence customercomplainPersistence;
    @BeanReference(name = "com.example.service.service.projectstatusLocalService.impl")
    protected projectstatusLocalService projectstatusLocalService;
    @BeanReference(name = "com.example.service.service.projectstatusService.impl")
    protected projectstatusService projectstatusService;
    @BeanReference(name = "com.example.service.service.persistence.projectstatusPersistence.impl")
    protected projectstatusPersistence projectstatusPersistence;
    @BeanReference(name = "com.example.service.service.projectactivityLocalService.impl")
    protected projectactivityLocalService projectactivityLocalService;
    @BeanReference(name = "com.example.service.service.projectactivityService.impl")
    protected projectactivityService projectactivityService;
    @BeanReference(name = "com.example.service.service.persistence.projectactivityPersistence.impl")
    protected projectactivityPersistence projectactivityPersistence;
    @BeanReference(name = "com.example.service.service.complainSolutionLocalService.impl")
    protected complainSolutionLocalService complainSolutionLocalService;
    @BeanReference(name = "com.example.service.service.complainSolutionService.impl")
    protected complainSolutionService complainSolutionService;
    @BeanReference(name = "com.example.service.service.persistence.complainSolutionPersistence.impl")
    protected complainSolutionPersistence complainSolutionPersistence;
    @BeanReference(name = "com.example.service.service.customerfeedbackLocalService.impl")
    protected customerfeedbackLocalService customerfeedbackLocalService;
    @BeanReference(name = "com.example.service.service.customerfeedbackService.impl")
    protected customerfeedbackService customerfeedbackService;
    @BeanReference(name = "com.example.service.service.persistence.customerfeedbackPersistence.impl")
    protected customerfeedbackPersistence customerfeedbackPersistence;
    @BeanReference(name = "com.example.service.service.detailinfoLocalService.impl")
    protected detailinfoLocalService detailinfoLocalService;
    @BeanReference(name = "com.example.service.service.detailinfoService.impl")
    protected detailinfoService detailinfoService;
    @BeanReference(name = "com.example.service.service.persistence.detailinfoPersistence.impl")
    protected detailinfoPersistence detailinfoPersistence;
    @BeanReference(name = "com.example.service.service.complainReportLocalService.impl")
    protected complainReportLocalService complainReportLocalService;
    @BeanReference(name = "com.example.service.service.complainReportService.impl")
    protected complainReportService complainReportService;
    @BeanReference(name = "com.example.service.service.persistence.complainReportPersistence.impl")
    protected complainReportPersistence complainReportPersistence;

    public subprojectactivityplan addsubprojectactivityplan(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        subprojectactivityplan.setNew(true);

        return subprojectactivityplanPersistence.update(subprojectactivityplan,
            false);
    }

    public subprojectactivityplan createsubprojectactivityplan(int id) {
        return subprojectactivityplanPersistence.create(id);
    }

    public void deletesubprojectactivityplan(int id)
        throws PortalException, SystemException {
        subprojectactivityplanPersistence.remove(id);
    }

    public void deletesubprojectactivityplan(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        subprojectactivityplanPersistence.remove(subprojectactivityplan);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return subprojectactivityplanPersistence.findWithDynamicQuery(dynamicQuery);
    }

    public List<Object> dynamicQuery(DynamicQuery dynamicQuery, int start,
        int end) throws SystemException {
        return subprojectactivityplanPersistence.findWithDynamicQuery(dynamicQuery,
            start, end);
    }

    public subprojectactivityplan getsubprojectactivityplan(int id)
        throws PortalException, SystemException {
        return subprojectactivityplanPersistence.findByPrimaryKey(id);
    }

    public List<subprojectactivityplan> getsubprojectactivityplans(int start,
        int end) throws SystemException {
        return subprojectactivityplanPersistence.findAll(start, end);
    }

    public int getsubprojectactivityplansCount() throws SystemException {
        return subprojectactivityplanPersistence.countAll();
    }

    public subprojectactivityplan updatesubprojectactivityplan(
        subprojectactivityplan subprojectactivityplan)
        throws SystemException {
        subprojectactivityplan.setNew(false);

        return subprojectactivityplanPersistence.update(subprojectactivityplan,
            true);
    }

    public subprojectactivityplan updatesubprojectactivityplan(
        subprojectactivityplan subprojectactivityplan, boolean merge)
        throws SystemException {
        subprojectactivityplan.setNew(false);

        return subprojectactivityplanPersistence.update(subprojectactivityplan,
            merge);
    }

    public customerprojectLocalService getcustomerprojectLocalService() {
        return customerprojectLocalService;
    }

    public void setcustomerprojectLocalService(
        customerprojectLocalService customerprojectLocalService) {
        this.customerprojectLocalService = customerprojectLocalService;
    }

    public customerprojectService getcustomerprojectService() {
        return customerprojectService;
    }

    public void setcustomerprojectService(
        customerprojectService customerprojectService) {
        this.customerprojectService = customerprojectService;
    }

    public customerprojectPersistence getcustomerprojectPersistence() {
        return customerprojectPersistence;
    }

    public void setcustomerprojectPersistence(
        customerprojectPersistence customerprojectPersistence) {
        this.customerprojectPersistence = customerprojectPersistence;
    }

    public customerLocalService getcustomerLocalService() {
        return customerLocalService;
    }

    public void setcustomerLocalService(
        customerLocalService customerLocalService) {
        this.customerLocalService = customerLocalService;
    }

    public customerService getcustomerService() {
        return customerService;
    }

    public void setcustomerService(customerService customerService) {
        this.customerService = customerService;
    }

    public customerPersistence getcustomerPersistence() {
        return customerPersistence;
    }

    public void setcustomerPersistence(customerPersistence customerPersistence) {
        this.customerPersistence = customerPersistence;
    }

    public subprojectactivityplanLocalService getsubprojectactivityplanLocalService() {
        return subprojectactivityplanLocalService;
    }

    public void setsubprojectactivityplanLocalService(
        subprojectactivityplanLocalService subprojectactivityplanLocalService) {
        this.subprojectactivityplanLocalService = subprojectactivityplanLocalService;
    }

    public subprojectactivityplanService getsubprojectactivityplanService() {
        return subprojectactivityplanService;
    }

    public void setsubprojectactivityplanService(
        subprojectactivityplanService subprojectactivityplanService) {
        this.subprojectactivityplanService = subprojectactivityplanService;
    }

    public subprojectactivityplanPersistence getsubprojectactivityplanPersistence() {
        return subprojectactivityplanPersistence;
    }

    public void setsubprojectactivityplanPersistence(
        subprojectactivityplanPersistence subprojectactivityplanPersistence) {
        this.subprojectactivityplanPersistence = subprojectactivityplanPersistence;
    }

    public user_LocalService getuser_LocalService() {
        return user_LocalService;
    }

    public void setuser_LocalService(user_LocalService user_LocalService) {
        this.user_LocalService = user_LocalService;
    }

    public user_Service getuser_Service() {
        return user_Service;
    }

    public void setuser_Service(user_Service user_Service) {
        this.user_Service = user_Service;
    }

    public user_Persistence getuser_Persistence() {
        return user_Persistence;
    }

    public void setuser_Persistence(user_Persistence user_Persistence) {
        this.user_Persistence = user_Persistence;
    }

    public subprojectLocalService getsubprojectLocalService() {
        return subprojectLocalService;
    }

    public void setsubprojectLocalService(
        subprojectLocalService subprojectLocalService) {
        this.subprojectLocalService = subprojectLocalService;
    }

    public subprojectService getsubprojectService() {
        return subprojectService;
    }

    public void setsubprojectService(subprojectService subprojectService) {
        this.subprojectService = subprojectService;
    }

    public subprojectPersistence getsubprojectPersistence() {
        return subprojectPersistence;
    }

    public void setsubprojectPersistence(
        subprojectPersistence subprojectPersistence) {
        this.subprojectPersistence = subprojectPersistence;
    }

    public customercomplainLocalService getcustomercomplainLocalService() {
        return customercomplainLocalService;
    }

    public void setcustomercomplainLocalService(
        customercomplainLocalService customercomplainLocalService) {
        this.customercomplainLocalService = customercomplainLocalService;
    }

    public customercomplainService getcustomercomplainService() {
        return customercomplainService;
    }

    public void setcustomercomplainService(
        customercomplainService customercomplainService) {
        this.customercomplainService = customercomplainService;
    }

    public customercomplainPersistence getcustomercomplainPersistence() {
        return customercomplainPersistence;
    }

    public void setcustomercomplainPersistence(
        customercomplainPersistence customercomplainPersistence) {
        this.customercomplainPersistence = customercomplainPersistence;
    }

    public projectstatusLocalService getprojectstatusLocalService() {
        return projectstatusLocalService;
    }

    public void setprojectstatusLocalService(
        projectstatusLocalService projectstatusLocalService) {
        this.projectstatusLocalService = projectstatusLocalService;
    }

    public projectstatusService getprojectstatusService() {
        return projectstatusService;
    }

    public void setprojectstatusService(
        projectstatusService projectstatusService) {
        this.projectstatusService = projectstatusService;
    }

    public projectstatusPersistence getprojectstatusPersistence() {
        return projectstatusPersistence;
    }

    public void setprojectstatusPersistence(
        projectstatusPersistence projectstatusPersistence) {
        this.projectstatusPersistence = projectstatusPersistence;
    }

    public projectactivityLocalService getprojectactivityLocalService() {
        return projectactivityLocalService;
    }

    public void setprojectactivityLocalService(
        projectactivityLocalService projectactivityLocalService) {
        this.projectactivityLocalService = projectactivityLocalService;
    }

    public projectactivityService getprojectactivityService() {
        return projectactivityService;
    }

    public void setprojectactivityService(
        projectactivityService projectactivityService) {
        this.projectactivityService = projectactivityService;
    }

    public projectactivityPersistence getprojectactivityPersistence() {
        return projectactivityPersistence;
    }

    public void setprojectactivityPersistence(
        projectactivityPersistence projectactivityPersistence) {
        this.projectactivityPersistence = projectactivityPersistence;
    }

    public complainSolutionLocalService getcomplainSolutionLocalService() {
        return complainSolutionLocalService;
    }

    public void setcomplainSolutionLocalService(
        complainSolutionLocalService complainSolutionLocalService) {
        this.complainSolutionLocalService = complainSolutionLocalService;
    }

    public complainSolutionService getcomplainSolutionService() {
        return complainSolutionService;
    }

    public void setcomplainSolutionService(
        complainSolutionService complainSolutionService) {
        this.complainSolutionService = complainSolutionService;
    }

    public complainSolutionPersistence getcomplainSolutionPersistence() {
        return complainSolutionPersistence;
    }

    public void setcomplainSolutionPersistence(
        complainSolutionPersistence complainSolutionPersistence) {
        this.complainSolutionPersistence = complainSolutionPersistence;
    }

    public customerfeedbackLocalService getcustomerfeedbackLocalService() {
        return customerfeedbackLocalService;
    }

    public void setcustomerfeedbackLocalService(
        customerfeedbackLocalService customerfeedbackLocalService) {
        this.customerfeedbackLocalService = customerfeedbackLocalService;
    }

    public customerfeedbackService getcustomerfeedbackService() {
        return customerfeedbackService;
    }

    public void setcustomerfeedbackService(
        customerfeedbackService customerfeedbackService) {
        this.customerfeedbackService = customerfeedbackService;
    }

    public customerfeedbackPersistence getcustomerfeedbackPersistence() {
        return customerfeedbackPersistence;
    }

    public void setcustomerfeedbackPersistence(
        customerfeedbackPersistence customerfeedbackPersistence) {
        this.customerfeedbackPersistence = customerfeedbackPersistence;
    }

    public detailinfoLocalService getdetailinfoLocalService() {
        return detailinfoLocalService;
    }

    public void setdetailinfoLocalService(
        detailinfoLocalService detailinfoLocalService) {
        this.detailinfoLocalService = detailinfoLocalService;
    }

    public detailinfoService getdetailinfoService() {
        return detailinfoService;
    }

    public void setdetailinfoService(detailinfoService detailinfoService) {
        this.detailinfoService = detailinfoService;
    }

    public detailinfoPersistence getdetailinfoPersistence() {
        return detailinfoPersistence;
    }

    public void setdetailinfoPersistence(
        detailinfoPersistence detailinfoPersistence) {
        this.detailinfoPersistence = detailinfoPersistence;
    }

    public complainReportLocalService getcomplainReportLocalService() {
        return complainReportLocalService;
    }

    public void setcomplainReportLocalService(
        complainReportLocalService complainReportLocalService) {
        this.complainReportLocalService = complainReportLocalService;
    }

    public complainReportService getcomplainReportService() {
        return complainReportService;
    }

    public void setcomplainReportService(
        complainReportService complainReportService) {
        this.complainReportService = complainReportService;
    }

    public complainReportPersistence getcomplainReportPersistence() {
        return complainReportPersistence;
    }

    public void setcomplainReportPersistence(
        complainReportPersistence complainReportPersistence) {
        this.complainReportPersistence = complainReportPersistence;
    }

    protected void runSQL(String sql) throws SystemException {
        try {
            PortalUtil.runSQL(sql);
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
