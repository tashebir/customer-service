package com.test;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.example.service.model.*;
import com.example.service.service.*;
/**
 * feedback Portlet Class
 */
public class feedback extends GenericPortlet {

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {

          if (response.getPortletMode().toString().equalsIgnoreCase("view")) {

                if (request.getParameter("feedsubmit") != null && request.getParameter("feedsubmit").equalsIgnoreCase("feedsubmit"))
                {
                     try {

Validation val = new Validation();
 customerfeedback feed=customerfeedbackLocalServiceUtil.createcustomerfeedback(0);
if( (val.validateUseremail(request.getParameter("feedemail")))&& (val.validateUser(request.getParameter("feeddetail"))) || (( (val.validateUser(request.getParameter("feedorgname"))) || (!val.validateUser(request.getParameter("feedorgname")))) || ((val.validateUser(request.getParameter("feedname")) )|| (!val.validateUser(request.getParameter("feedname")))) ))
 {
   
     feed.setOrgname(request.getParameter("feedorgname"));
                        feed.setCustomerName(request.getParameter("feedname"));
                        feed.setEmail(request.getParameter("feedemail"));
                        feed.setUrfeedback(request.getParameter("feeddetail"));
                        feed.setProjectid(request.getParameter("subid"));
                        customerfeedbackLocalServiceUtil.addcustomerfeedback(feed);
                        request.setAttribute("msg","seccussfully Saved");
                        request.setAttribute("page","/WEB-INF/jsp/Feedback_view.jsp");

  
}
else{
if( (!val.validateUseremail(request.getParameter("feedemail")))&& (val.validateUser(request.getParameter("feeddetail"))) || (( (val.validateUser(request.getParameter("feedorgname"))) || (!val.validateUser(request.getParameter("feedorgname")))) || ((val.validateUser(request.getParameter("feedname")) )|| (!val.validateUser(request.getParameter("feedname")))) ))
{request.setAttribute("mg", "Please fill valid Email");
 request.setAttribute("page","/WEB-INF/jsp/Feedback_view.jsp");
}
else if( (val.validateUseremail(request.getParameter("feedemail")))&& (!val.validateUser(request.getParameter("feeddetail"))) || (( (val.validateUser(request.getParameter("feedorgname"))) || (!val.validateUser(request.getParameter("feedorgname")))) || ((val.validateUser(request.getParameter("feedname")) )|| (!val.validateUser(request.getParameter("feedname")))) ))
{
    request.setAttribute("mg", "Please fill your Feedback ");
 request.setAttribute("page","/WEB-INF/jsp/Feedback_view.jsp");
}
else if( (!val.validateUseremail(request.getParameter("feedemail")))&& (!val.validateUser(request.getParameter("feeddetail"))) || (( (val.validateUser(request.getParameter("feedorgname"))) || (!val.validateUser(request.getParameter("feedorgname")))) || ((val.validateUser(request.getParameter("feedname")) )|| (!val.validateUser(request.getParameter("feedname")))) ))
{
    request.setAttribute("mg", "Please fill atleast valid Email and your Feedback ");
 request.setAttribute("page","/WEB-INF/jsp/Feedback_view.jsp");
}
}
                       
                     } catch (Exception io) {
                     }
                 }
          }







 }
    
    public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
            String callPage="/WEB-INF/jsp/Feedback_view.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
         String callPage="/WEB-INF/jsp/Feedback_edit.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
}