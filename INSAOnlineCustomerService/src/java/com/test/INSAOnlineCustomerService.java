package com.test;

import com.liferay.portal.SystemException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.portlet.GenericPortlet;
import com.liferay.portal.kernel.util.Validator;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.example.service.model.*;
import com.example.service.service.*;
import com.example.service.service.persistence.subprojectUtil;
import com.example.service.service.persistence.customerUtil;
//import com.example.service.model.subproject;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;

/**
 * INSAOnlineCustomerService Portlet Class
 */
public class INSAOnlineCustomerService extends GenericPortlet {

    public static int count = 1,  noof = 0, nams=0;

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException, IOException {


        if (response.getPortletMode().toString().equalsIgnoreCase("view")) {

            Validation val = new Validation();

            if ((request.getParameter("su3") != null) && request.getParameter("su3").equalsIgnoreCase("su3")) {
                request.setAttribute("msg", "new");
                request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");

            } else if ((request.getParameter("su2") != null) && request.getParameter("su2").equalsIgnoreCase("su2")) {
                int x = 0;
                try {
                    for (int y = 0; y < subprojectLocalServiceUtil.getsubprojectsCount(); y++) {
                        if (request.getParameter("ss" + y) != null && (request.getParameter("ss" + y).equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(y).getSubproject_id()))) {
                            x = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(y).getId();
                            break;

                        }
                    }
                    subprojectLocalServiceUtil.deletesubproject(x);
                    request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_view.jsp");
                    request.setAttribute("msg", "Delete");

                } catch (Exception io) {
                }






            } else if ((request.getParameter("su1") != null) && request.getParameter("su1").equalsIgnoreCase("su1")) {
                int x = 0;
                
                try {

                    request.setAttribute("msg", "wwwwwww");
                    for (int y = 0; y < subprojectLocalServiceUtil.getsubprojectsCount(); y++) {
                        if (request.getParameter("ss" + y) != null) {
                            // request.setAttribute("vv", request.getParameter("ss"+y)+"  entered");
                            x = 1;
                            String tempproid = "";

                            for (int z = 0; z < subprojectLocalServiceUtil.getsubprojectsCount(); z++) {

                                if (request.getParameter("ss" + y).equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id())) {

                                    tempproid = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId();
                                    request.setAttribute("subproid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id());
                                    request.setAttribute("suid",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getId());
                                    request.setAttribute("subproname", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_name());
                                    request.setAttribute("subsd", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getStart_date());
                                    request.setAttribute("subed", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getEnd_date());
                                    break;
                                }
                            }
                            for (int w = 0; w < customerprojectLocalServiceUtil.getcustomerprojectsCount(); w++) {
                                if (tempproid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectId())) {
                                    request.setAttribute("proid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectId());
                                    request.setAttribute("pid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getId());
                                    request.setAttribute("proname", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectName());
                                    request.setAttribute("prosd", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getStartDate());
                                    request.setAttribute("proed", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getEndDate());
                                    request.setAttribute("proman", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectManagerId());
                                    request.setAttribute("profollower", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getUserIId());
                                    // request.setAttribute("editdiplayy","block");
                                    //request.setAttribute("page", "/WEB-INF/jsp/editProject.jsp");
                                    break;
                                }

                            }

                            break;
                        }
                    }
                    if (x == 0) {
                        request.setAttribute("msg", "Please Select ");
                    } else {
                        request.setAttribute("page", "/WEB-INF/jsp/editProject.jsp");
                    }

                } catch (Exception io) {
                    request.setAttribute("msg", "sub display Error..");
                }

            }else if (request.getParameter("viewsubpro") != null && request.getParameter("viewsubpro").equalsIgnoreCase("viewsubpro")) {

                    if ((!val.validateUser(request.getParameter("subproject_id"))) && (!val.validateUser(request.getParameter("subproject_name")))) {
                    request.setAttribute("mg", "Please write atleast Sub Project Id  and SubProject Name  ");
                    request.setAttribute("page", "/WEB-INF/jsp/SubProjectInsertion.jsp");
                } else {


                    String prrrrrid = "",subprrrrid="";int orggid = 0;


                    prrrrrid = request.getParameter("projectsubtranId");
                    subprrrrid=request.getParameter("subproject_id");



                    try {
                        String SSD = request.getParameter("dsp") + "/" + request.getParameter("msp") + "/" + request.getParameter("ysp");
                        String SED = request.getParameter("dep") + "/" + request.getParameter("mep") + "/" + request.getParameter("yep");


                        subproject subpr = subprojectLocalServiceUtil.createsubproject(0);
                        subpr.setSubproject_id(request.getParameter("subproject_id"));
                        subpr.setSubproject_name(request.getParameter("subProject_name"));
                        subpr.setProjectId(request.getParameter("projectsubtranId"));
                        subpr.setStart_date(SSD);
                        subpr.setEnd_date(SED);
                        subprojectLocalServiceUtil.addsubproject(subpr);
                        request.setAttribute("msg", projectactivityLocalServiceUtil.getprojectactivitiesCount());
                        String SDPP = request.getParameter("dspp") + "/" + request.getParameter("mspp") + "/" + request.getParameter("yspp");
                        String SDR = request.getParameter("dsrp") + "/" + request.getParameter("msrp") + "/" + request.getParameter("ysrp");
                        String SDS = request.getParameter("dssp") + "/" + request.getParameter("mssp") + "/" + request.getParameter("yssp");
                        String SDI = request.getParameter("dsip") + "/" + request.getParameter("msip") + "/" + request.getParameter("ysip");
                        String SDT = request.getParameter("dstp") + "/" + request.getParameter("mstp") + "/" + request.getParameter("ystp");
                        String SDD = request.getParameter("dsdp") + "/" + request.getParameter("msdp") + "/" + request.getParameter("ysdp");
                        String SDM = request.getParameter("dsmp") + "/" + request.getParameter("msmp") + "/" + request.getParameter("ysmp");
                        String SDTR = request.getParameter("dstrp") + "/" + request.getParameter("mstrp") + "/" + request.getParameter("ystrp");

                        String EDPP = request.getParameter("depp") + "/" + request.getParameter("mepp") + "/" + request.getParameter("yepp");
                        String EDR = request.getParameter("derp") + "/" + request.getParameter("merp") + "/" + request.getParameter("yerp");
                        String EDS = request.getParameter("desp") + "/" + request.getParameter("mesp") + "/" + request.getParameter("yesp");
                        String EDI = request.getParameter("deip") + "/" + request.getParameter("meip") + "/" + request.getParameter("yeip");
                        String EDT = request.getParameter("detp") + "/" + request.getParameter("metp") + "/" + request.getParameter("yetp");
                        String EDD = request.getParameter("dedp") + "/" + request.getParameter("medp") + "/" + request.getParameter("yedp");
                        String EDM = request.getParameter("demp") + "/" + request.getParameter("memp") + "/" + request.getParameter("yemp");
                        String EDTR = request.getParameter("detrp") + "/" + request.getParameter("metrp") + "/" + request.getParameter("yetrp");

                        for (int i = 1; i <= projectactivityLocalServiceUtil.getprojectactivitiesCount(); i++) {
                            subprojectactivityplan subplan = subprojectactivityplanLocalServiceUtil.createsubprojectactivityplan(0);
                            subplan.setActivityType(Integer.toString(i));
                            subplan.setSubprojectId(request.getParameter("subproject_id"));
                            subplan.setDuration("ccccc");
                            if (i == 1) {
                                subplan.setStart_date(SDPP);
                                subplan.setEnd_date(EDPP);
                            } else if (i == 2) {
                                subplan.setStart_date(SDR);
                                subplan.setEnd_date(EDR);
                            } else if (i == 3) {
                                subplan.setStart_date(SDS);
                                subplan.setEnd_date(EDS);
                            } else if (i == 4) {
                                subplan.setStart_date(SDI);
                                subplan.setEnd_date(EDI);
                            } else if (i == 5) {
                                subplan.setStart_date(SDT);
                                subplan.setEnd_date(EDT);
                            } else if (i == 6) {
                                subplan.setStart_date(SDD);
                                subplan.setEnd_date(EDD);
                            } else if (i == 7) {
                                subplan.setStart_date(SDM);
                                subplan.setEnd_date(EDM);
                            } else {
                                subplan.setStart_date(SDTR);
                                subplan.setEnd_date(EDTR);
                            }
                            subprojectactivityplanLocalServiceUtil.addsubprojectactivityplan(subplan);

                        }
                    } catch (Exception io) {
                        request.setAttribute("msg", "Trail Insertion Errrrrrooorrrrr");
                    }


                    try {
                        for (int z = 0; z < customerprojectLocalServiceUtil.getcustomerprojectsCount(); z++) {
                            if (prrrrrid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId())) {
                                request.setAttribute("prodispid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId());
                                request.setAttribute("prodispname", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName());
                                request.setAttribute("prodispuserid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getUserIId());
                                orggid = customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getOrgid();
                                request.setAttribute("prodispmanid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectManagerId());
                                request.setAttribute("prodispStartDate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getStartDate());
                                request.setAttribute("prodispEnddate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getEndDate());
                                break;
                            }
                        }
                        for (int s = 0; s < customerLocalServiceUtil.getcustomersCount(); s++) {
                            if (orggid==customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getId()) {
                                request.setAttribute("prodisporgname", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getOrganName());
                                request.setAttribute("prodispsector", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getSector());
                                request.setAttribute("prodisporgemail", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getEmail());
                                request.setAttribute("prodisporgftele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFixedtele());
                                request.setAttribute("prodisporgmtele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getMobiletele());
                                request.setAttribute("prodisporgfax", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFax());

                                 request.setAttribute("prodisporgregion", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getRegion());
                                 request.setAttribute("prodisporgzone", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getZone());
                                 request.setAttribute("prodisporgworeda", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getWoreda());
                                 request.setAttribute("prodisporgkebele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getKebele());
                                 request.setAttribute("prodisporghouseno", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getHouseno());
                                break;
                            }
                        }
                        subprojectUtil.clearCache();
                        for (int z = 0; z < subprojectLocalServiceUtil.getsubprojectsCount(); z++) {
                            if ((prrrrrid.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId()))&&(subprrrrid.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id()))) {
                                request.setAttribute("subproid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id());
                                request.setAttribute("subproname", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_name());
                                request.setAttribute("subprosd", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getStart_date());
                                request.setAttribute("subproed", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getEnd_date());
                                request.setAttribute("proid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId());

                                break;
                            }
                        }
                    } catch (Exception io) {
                        request.setAttribute("msg", "Trail Fetching Errrrrrooorrrrr");
                    }
                    request.setAttribute("page", "/WEB-INF/jsp/display.jsp");

                }


        }else if (request.getParameter("newsub") != null && request.getParameter("newsub").equalsIgnoreCase("newsub")) {

                try
                {
                    for(int s=0;s<=customerprojectLocalServiceUtil.getcustomerprojectsCount();s++)
                    {
                        if(request.getParameter("newproid").equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(s).getProjectId()))
                        {
                            request.setAttribute("projecttranid",customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(s).getProjectId());
                            request.setAttribute("projecttranName",customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(s).getProjectName());
                            break;
                        }
                    }
                    request.setAttribute("page", "/WEB-INF/jsp/SubProjectInsertion.jsp");
                }
                catch(Exception io)
                {
                    
                }
                
            }
            else if (request.getParameter("supdate") != null && request.getParameter("supdate").equalsIgnoreCase("supdate")) {
                 //request.setAttribute("msg", "get in to Update");
               //  System.out.println("get in please");
                try {
                     
                    int piid=Integer.valueOf(request.getParameter("pid"));
                    int siid = Integer.valueOf(request.getParameter("suid"));
                    
                    customerproject custpro =customerprojectLocalServiceUtil.createcustomerproject(piid);
                    custpro.setProjectId(request.getParameter("projId"));
                    custpro.setProjectName(request.getParameter("projName"));
                    custpro.setProjectManagerId(request.getParameter("projMng"));
                    custpro.setUserIId(request.getParameter("projUserid"));
                    custpro.setStartDate(request.getParameter("projSd"));
                    custpro.setEndDate(request.getParameter("projEd"));
                    customerprojectLocalServiceUtil.updatecustomerproject(custpro);
               
                    subproject proj = subprojectLocalServiceUtil.createsubproject(siid);
                    proj.setSubproject_id(request.getParameter("subproject_id").trim());
                    proj.setSubproject_name(request.getParameter("subproname"));
                    proj.setStart_date(request.getParameter("subsd").trim());
                    proj.setEnd_date(request.getParameter("subed").trim());
                    proj.setProjectId(request.getParameter("projId").trim());
                    subprojectLocalServiceUtil.updatesubproject(proj);
                    request.setAttribute("msg", "Successfully Updated");
                    request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_view.jsp");

                    

                } catch (Exception io) {

                    request.setAttribute("msg", io);

              
                }


            } else if (request.getParameter("selam") != null) {
                String sel = request.getParameter("selam"),selpro = request.getParameter("selampro"), Proid = "", proma = "",useridd="";
                        int usefoll = 0;


                try {
                    for (int i = 0; i < subprojectLocalServiceUtil.getsubprojectsCount(); i++) {
                        if (sel.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id())) {
                            request.setAttribute("SubProId", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id());
                            request.setAttribute("SubProName", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_name());
                            request.setAttribute("SubStartDate", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getStart_date());
                            request.setAttribute("SubEndDate", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getEnd_date());
                            request.setAttribute("ProId", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getProjectId());
                            //Proid = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getProjectId();
                            break;

                        }
                    }
                    for (int j = 0; j < customerprojectLocalServiceUtil.getcustomerprojectsCount(); j++) {
                        if (selpro.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getProjectId())) {
                            request.setAttribute("proId", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getProjectId());
                            request.setAttribute("proName", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getProjectName());
                            request.setAttribute("proStratDate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getStartDate());
                            request.setAttribute("proEndDate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getEndDate());
                            request.setAttribute("proMan", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getProjectManagerId());
                            proma = customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getProjectManagerId();
                           useridd= customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getUserIId();
                            usefoll = customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(j).getOrgid();
                            break;
                        }
                    }
                    int promauid = Integer.parseInt(proma);
                    for (int k = 0; k < user_LocalServiceUtil.getuser_sCount(); k++) {
                        if (promauid == user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(k).getUserId()) {
                            request.setAttribute("promaname", user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(k).getScreenName());
                            break;
                        }
                    }
                    for (int l = 0; l < customerLocalServiceUtil.getcustomersCount(); l++) {
                        if (usefoll==customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getId()) {
                            request.setAttribute("orgname", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getOrganName());
                            request.setAttribute("sec", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getSector());
                            request.setAttribute("email", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getEmail());
                            request.setAttribute("ftele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getFixedtele());
                            request.setAttribute("mtele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getMobiletele());
                            request.setAttribute("fax", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(l).getFax());
                            break;

                        }
                    }
                    for(int i=0;i<user_LocalServiceUtil.getuser_sCount();i++){
                         if(Integer.parseInt(useridd)==(user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getUserId()))
                         {
                                 request.setAttribute("proFollower",user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getScreenName());
                         }
                    }

                    request.setAttribute("page", "/WEB-INF/jsp/viewprojectdetail.jsp");
                    request.setAttribute("selam", "wow test 1 2 3");

                } catch (Exception io) {
                }
            } else if (request.getParameter("canc") != null) {
                request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_view.jsp");

            } else if (request.getParameter("resh") != null) {


                request.setAttribute("msg", request.getParameter("resh"));




                try {
                    subprojectUtil.clearCache();

                    String[] suid = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] suna = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] susd = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] sued = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] Proid = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] vieew = new String[subprojectLocalServiceUtil.getsubprojectsCount()];
                    String[] vieewpro = new String[subprojectLocalServiceUtil.getsubprojectsCount()];

                    request.setAttribute("promoid", request.getParameter("resh"));
                    for (int i = 0; i < subprojectLocalServiceUtil.getsubprojectsCount(); i++) {
                        request.setAttribute("vv", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id());
                        if (subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getProjectId().equalsIgnoreCase(request.getParameter("resh"))) {


                            suid[i] = "<tr><td id='ff'><input type='checkbox' name='ss" + i + "' value=\"" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id() + "\"></td><td id='ff'>" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id() + "</td>";
                            suna[i] = "<td id='ff'>" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_name() + "</td>";
                            susd[i] = "<td id='ff'>" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getStart_date() + "</td>";
                            sued[i] = "<td id='ff'>" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getEnd_date() + "</td>";
                            Proid[i] = "<td id='ff'>" + subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getProjectId() + "</td>";
                            vieew[i] = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id();
                            vieewpro[i] = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getProjectId();




                        }
                        else
                        {
                           // request.getp
                        }
                    }




                    request.setAttribute("res0", suid);
                    request.setAttribute("res1", suna);
                    request.setAttribute("res2", susd);
                    request.setAttribute("res3", sued);
                    request.setAttribute("res4", Proid);
                    request.setAttribute("res5", vieew);
                    request.setAttribute("res6", vieewpro);




                } catch (Exception io) {

                    request.setAttribute("msg", "Error..");

                }







            } else {
                request.setAttribute("msg", "nothing..");
            }
        } ///edit
 else {
            String obrest=request.getParameter("pro");
        
            Validation val = new Validation();

/*if (obrest.equalsIgnoreCase("fornew") || obrest==null) {
            nams = 0;

        }*/
 if (obrest !=null && (request.getParameter("pro").equalsIgnoreCase("proform"))&& nams == 0) {
       try {
    String pmanid2 = request.getParameter("pmanid");
           String projectId2= request.getParameter("projectId").replace("<", "&gt;");
            projectId2.replace("'", "&uu");
                    projectId2.replace(">", "&gt;");
           String projectname2 = request.getParameter("projectname").replace("<", "&gt;");
           projectname2.replace("'", "&uu");
                    projectname2.replace(">", "&gt;");
           String organName2= request.getParameter("organName").replace("<", "&gt;");
           organName2.replace("'", "&uu");
                    organName2.replace(">", "&gt;");
           String email2= request.getParameter("email").replace("<", "&gt;");
           email2.replace("'", "&uu");
                    email2.replace(">", "&gt;");
           String ftele2 = request.getParameter("ftele").replace("<", "&gt;");
           ftele2.replace("'", "&uu");
                    ftele2.replace(">", "&gt;");
            String woreda2= request.getParameter("woreda").replace("<", "&gt;");
             woreda2.replace("'", "&uu");
                    woreda2.replace(">", "&gt;");

            String sector2= request.getParameter("sector");
           String region2= request.getParameter("region");
           String mtele2= request.getParameter("mtele").replace("<", "&gt;");
             mtele2.replace("'", "&uu");
                    mtele2.replace(">", "&gt;");;
           String kebele2 = request.getParameter("kebele").replace("<", "&gt;");
           kebele2.replace("'", "&uu");
                    kebele2.replace(">", "&gt;");
                    String fax2 = request.getParameter("fax").replace("<", "&gt;");
           fax2.replace("'", "&uu");
                    fax2.replace(">", "&gt;");
                      String houseno2 = request.getParameter("houseno").replace("<", "&gt;");
           houseno2.replace("'", "&uu");
                    houseno2.replace(">", "&gt;");
                     String zone2 =request.getParameter("zone").replace("<", "&gt;");
           zone2.replace("'", "&uu");
                    zone2.replace(">", "&gt;");


       String ss="";
                    request.setAttribute("projecttranid", projectId2);
                    request.setAttribute("projecttranName", projectname2);
                    // request.setAttribute("pmanid", request.getParameter("pmanid"));
                    // request.setAttribute("userrid", request.getParameter("userrid"));


                    ThemeDisplay xxxxx = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
                    String sd = request.getParameter("dp") + "/" + request.getParameter("mp") + "/" + request.getParameter("yp");
                    String ed = request.getParameter("dep") + "/" + request.getParameter("mep") + "/" + request.getParameter("yep");
                    request.setAttribute("StartDate", sd);
                    request.setAttribute("EndDate", ed);
                    int orgid=0;
if ((val.validateUser(pmanid2)) && (val.validateUser(projectId2)) && (val.validateUser(projectname2)) &&  (val.validateUser(request.getParameter("userrid"))) &&  (val.validateUser(organName2))  && (val.validateUseremail(email2))) {

                        customer cust = customerLocalServiceUtil.createcustomer(0);
                        cust.setOrganName(organName2);
                            if(sector2!=null &&  ( !( sector2.equalsIgnoreCase("--Select--")) )){
cust.setSector(sector2);}else{
cust.setSector(ss);}
                        cust.setEmail(email2);
                        cust.setFixedtele(ftele2);
                        cust.setMobiletele(mtele2);
                        cust.setFax(fax2);
                            if(region2!=null &&  ( !( region2.equalsIgnoreCase("--Select--")) )){
 cust.setRegion(region2);}else{
cust.setSector(ss);}

                        cust.setZone(zone2);
                        cust.setWoreda(woreda2);
                        cust.setKebele(kebele2);
                        cust.setHouseno(houseno2);
                        cust.setUserIId(request.getParameter("userrid"));
                        customerLocalServiceUtil.addcustomer(cust);
                        customerUtil.clearCache();


                        for(int i=0;i<customerLocalServiceUtil.getcustomersCount();i++)
                        {
                            if(organName2.trim().equalsIgnoreCase(customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(i).getOrganName()))
                            {
                               orgid=customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(i).getId();
                            }
                        }
                        customerproject proj = customerprojectLocalServiceUtil.createcustomerproject(0);
                        proj.setProjectId(projectId2);
                        proj.setProjectName(projectname2);
                        proj.setStartDate(sd);
                        proj.setEndDate(ed);
                        proj.setUserIId(request.getParameter("userrid"));
                        proj.setProjectManagerId(pmanid2);
                        proj.setOrgid(orgid);
                        customerprojectLocalServiceUtil.addcustomerproject(proj);

                        request.setAttribute("msg", "Successfully saved");
                      
                        request.setAttribute("one", "");
                request.setAttribute("two", "");
                request.setAttribute("three", "");
                request.setAttribute("four", "");
                 request.setAttribute("five", "");
                  request.setAttribute("six", "");


                        if ((request.getParameter("chk").equalsIgnoreCase("chk"))) {
                            request.setAttribute("msg", " have a sub project");
                            request.setAttribute("page", "/WEB-INF/jsp/SubProjectInsertion.jsp");

                        } else {

                            request.setAttribute("msg", "don't Have a sub project");
                            request.setAttribute("page", "/WEB-INF/jsp/projectActivity.jsp");

                        }
                    }
else {
 if (!(val.validateUser(pmanid2))) {
request.setAttribute("mg", "Please select Project Manager");
request.setAttribute("one", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }
                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        }

                    else if (!(val.validateUser(projectId2)) ) {


                            request.setAttribute("mg", "Please write valid projectId  ");
  request.setAttribute("two", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }

                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        }
                    else if ( !(val.validateUser(projectname2)) ) {

                            request.setAttribute("mg", "Please write projectname  ");
                          request.setAttribute("three", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }
                             request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        }
                       else if (!(val.validateUser(request.getParameter("userrid"))) ) {

                            request.setAttribute("mg", "Please select Responsible Project Follower");
                          request.setAttribute("four", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }

                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");

                        }


                    else if (!(val.validateUser(organName2)) ) {

                            request.setAttribute("mg", "Please write Organization Name ");

                             request.setAttribute("five", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }
                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");

                        }
                    else if (!(val.validateUseremail(email2)) ) {

                            request.setAttribute("mg", "Please write Valid Email  ");
                            request.setAttribute("six", "red");
 if(sector2!=null){
                            request.setAttribute("sector2", sector2);
                            }
 if(pmanid2!=null){
                            request.setAttribute("pmanid2", pmanid2);
                            }
 if(projectId2!=null){
                            request.setAttribute("projectId2", projectId2);
                            }
 if(projectname2!=null){
                            request.setAttribute("projectname2", projectname2);
                            }
 if(organName2!=null){
                            request.setAttribute("organName2", organName2);
                            }
 if(email2!=null){
                            request.setAttribute("email2", email2);
                            }
 if(ftele2!=null){
                            request.setAttribute("ftele2", ftele2);
                            }
 if(woreda2!=null){
                            request.setAttribute("woreda2", woreda2);
                            }
 if(region2!=null){
                            request.setAttribute("region2", region2);
                            }
 if(mtele2!=null){
                            request.setAttribute("mtele2", mtele2);
                            }
 if(kebele2!=null){
                            request.setAttribute("kebele2", kebele2);
                            }
 if(fax2!=null){
                            request.setAttribute("fax2", fax2);
                            }
 if(houseno2!=null){
                            request.setAttribute("houseno2", houseno2);
                            }
if(zone2!=null){
                            request.setAttribute("zone2", zone2);
                            }
if( request.getParameter("userrid")!=null){
                            request.setAttribute("userrid2",  request.getParameter("userrid"));
                            }
                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");

                        }
                    else if ((!(val.validateUser(organName2))) && (!val.validateUseremail(email2)) && (!val.validateUser(pmanid2)) && (!val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(request.getParameter("userrid")))) {
                            request.setAttribute("mg", "Please write atleast Project Manager, Project Id, Project Name,Responsible Project Follower, Organization Name and Valid Email ");
                            request.setAttribute("sector", sector2);
                            request.setAttribute("ftele ", request.getParameter("ftele "));
                            request.setAttribute("mtele", mtele2);
                            request.setAttribute("fax ", request.getParameter("fax "));
                            request.setAttribute("region ", request.getParameter("region "));
                            request.setAttribute("zone", zone2);
                            request.setAttribute("woreda ", request.getParameter("woreda "));
                            request.setAttribute("kebele", kebele2);
                            request.setAttribute("houseno ", request.getParameter("houseno "));




                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        }  else if ((val.validateUser(organName2)) && (val.validateUseremail(email2)) && (val.validateUser(pmanid2)) && (val.validateUser(projectId2)) && (val.validateUser(projectname2)) && (!val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please select Responsible Project Follower");

                            request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        } else if ((val.validateUser(organName2)) && (val.validateUseremail(email2)) && (!val.validateUser(pmanid2)) && (!val.validateUser(projectId2)) && (val.validateUser(projectname2)) && (val.validateUser(request.getParameter("userrid")))) {
                            request.setAttribute("mg", "Please write  Project Id");
                            request.setAttribute("sector", sector2);
                            request.setAttribute("ftele ", request.getParameter("ftele "));
                            request.setAttribute("mtele", mtele2);
                            request.setAttribute("fax ", request.getParameter("fax "));
                            request.setAttribute("region ", request.getParameter("region "));
                            request.setAttribute("zone", zone2);
                            request.setAttribute("woreda ", request.getParameter("woreda "));
                            request.setAttribute("kebele", kebele2);
                            request.setAttribute("houseno ", request.getParameter("houseno "));
                        }   else if ((val.validateUser(organName2)) && (val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write Project Id	and Project Name   ");
                        } else if ((!val.validateUser(organName2)) && (val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write organName and Project Id ");
                        } else if ((!val.validateUser(organName2)) && (val.validateUseremail(email2)) && (val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write organName and Project Name");
                        } else if ((!val.validateUser(organName2)) && (val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write organName , Project Id and Project Name ");
                        } else if ((val.validateUser(organName2)) && (!val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write Project Id and Email ");
                        } else if ((val.validateUser(organName2)) && (!val.validateUseremail(email2)) && (val.validateUser(projectId2))) {

                            request.setAttribute("mg", "Please write Project Name and Email");
                        } else if ((val.validateUser(organName2)) && (!val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write Project Id , Project Name and Email ");
                        } else if ((!val.validateUser(organName2)) && (!val.validateUseremail(email2)) && (!val.validateUser(projectId2)) && (val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write Project Id , organName  and Email ");
                        } else if ((!val.validateUser(organName2)) && (!val.validateUseremail(email2)) && (val.validateUser(projectId2)) && (!val.validateUser(projectname2)) && (val.validateUser(pmanid2)) && (val.validateUser(request.getParameter("userrid")))) {

                            request.setAttribute("mg", "Please write Project Name, organName and valid Email ");
                        }
                        else{
                             request.setAttribute("mg", "Please write atleast Project Manager, Project Id, Project Name,Responsible Project Follower, Organization Name and Valid Email ");
                        request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
                        }

                        }
                   request.setAttribute("prowithsub", "displaySubSelam");

         } catch (Exception io) {
                    request.setAttribute("msg", "not possible");

                }
            }
        else if (request.getParameter("addsub") != null && request.getParameter("addsub").equalsIgnoreCase("addsub")) {

                request.setAttribute("projecttranid", request.getParameter("disptranproid"));
                request.setAttribute("projecttranName", request.getParameter("disptranproname"));
                request.setAttribute("page", "/WEB-INF/jsp/SubProjectInsertion.jsp");

            } else if (request.getParameter("disped") != null && request.getParameter("disped").equalsIgnoreCase("disped")) {
                
                    String tempproid = "";
                     try
                    {
                                                                  
                            for (int z = 0; z < subprojectLocalServiceUtil.getsubprojectsCount(); z++) {

                                if(request.getParameter("dissubid").equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id()))
                                {

                                    tempproid = subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId();
                                    request.setAttribute("subproid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id());
                                    request.setAttribute("suid",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getId());
                                    request.setAttribute("subproname", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_name());
                                    request.setAttribute("subsd", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getStart_date());
                                    request.setAttribute("subed", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getEnd_date());
                                    break;
                                }
                            }
                            for (int w = 0; w < customerprojectLocalServiceUtil.getcustomerprojectsCount(); w++) {
                                if (tempproid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectId())) {
                                    request.setAttribute("proid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectId());
                                    request.setAttribute("pid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getId());
                                    request.setAttribute("proname", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectName());
                                    request.setAttribute("prosd", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getStartDate());
                                    request.setAttribute("proed", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getEndDate());
                                    request.setAttribute("proman", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getProjectManagerId());
                                    request.setAttribute("profollower", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(w).getUserIId());
                                    // request.setAttribute("editdiplayy","block");
                                    //request.setAttribute("page", "/WEB-INF/jsp/editProject.jsp");
                                    break;
                                }
                            }
                            request.setAttribute("updatedisplay", "displayUpdate");
                            request.setAttribute("page", "/WEB-INF/jsp/editProject.jsp");
                     }catch(Exception io)
                     {

                     }



            }
            else if (request.getParameter("disupdate") != null && request.getParameter("disupdate").equalsIgnoreCase("disupdate")) {

             try {

                    int piid=Integer.valueOf(request.getParameter("pid"));
                    int siid = Integer.valueOf(request.getParameter("suid"));

                    customerproject custpro =customerprojectLocalServiceUtil.createcustomerproject(piid);
                    custpro.setProjectId(request.getParameter("projId"));
                    custpro.setProjectName(request.getParameter("projName"));
                    custpro.setProjectManagerId(request.getParameter("projMng"));
                    custpro.setUserIId(request.getParameter("projUserid"));
                    custpro.setStartDate(request.getParameter("projSd"));
                    custpro.setEndDate(request.getParameter("projEd"));
                    customerprojectLocalServiceUtil.updatecustomerproject(custpro);

                    subproject proj = subprojectLocalServiceUtil.createsubproject(siid);
                    proj.setSubproject_id(request.getParameter("subproject_id").trim());
                    proj.setSubproject_name(request.getParameter("subproname"));
                    proj.setStart_date(request.getParameter("subsd").trim());
                    proj.setEnd_date(request.getParameter("subed").trim());
                    proj.setProjectId(request.getParameter("projId").trim());
                    subprojectLocalServiceUtil.updatesubproject(proj);
                    request.setAttribute("msg", "Successfully Updated");
                    request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_view.jsp");



                } catch (Exception io) {

                    request.setAttribute("msg", io);


                }





            } else if (request.getParameter("nosubpro") != null && request.getParameter("nosubpro").equalsIgnoreCase("nosubpro")) {

                request.setAttribute("msg", " It's getting here");
                request.setAttribute("page", "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp");
            }

         // try {
            





else if (request.getParameter("subpro") != null && request.getParameter("subpro").equalsIgnoreCase("subpro")) {

                if ((!val.validateUser(request.getParameter("subproject_id"))) && (!val.validateUser(request.getParameter("subproject_name")))) {
                    request.setAttribute("mg", "Please write atleast Sub Project Id  and SubProject Name  ");
                    request.setAttribute("page", "/WEB-INF/jsp/SubProjectInsertion.jsp");
                } else {


                    String prrrrrid = "";int orggid = 0;


                    prrrrrid = request.getParameter("projectsubtranId");




                    try {
                        String SSD = request.getParameter("dsp") + "/" + request.getParameter("msp") + "/" + request.getParameter("ysp");
                        String SED = request.getParameter("dep") + "/" + request.getParameter("mep") + "/" + request.getParameter("yep");


                        subproject subpr = subprojectLocalServiceUtil.createsubproject(0);
                        subpr.setSubproject_id(request.getParameter("subproject_id"));
                        subpr.setSubproject_name(request.getParameter("subProject_name"));
                        subpr.setProjectId(request.getParameter("projectsubtranId"));
                        subpr.setStart_date(SSD);
                        subpr.setEnd_date(SED);
                        subprojectLocalServiceUtil.addsubproject(subpr);
                        request.setAttribute("msg", projectactivityLocalServiceUtil.getprojectactivitiesCount());
                        String SDPP = request.getParameter("dspp") + "/" + request.getParameter("mspp") + "/" + request.getParameter("yspp");
                        String SDR = request.getParameter("dsrp") + "/" + request.getParameter("msrp") + "/" + request.getParameter("ysrp");
                        String SDS = request.getParameter("dssp") + "/" + request.getParameter("mssp") + "/" + request.getParameter("yssp");
                        String SDI = request.getParameter("dsip") + "/" + request.getParameter("msip") + "/" + request.getParameter("ysip");
                        String SDT = request.getParameter("dstp") + "/" + request.getParameter("mstp") + "/" + request.getParameter("ystp");
                        String SDD = request.getParameter("dsdp") + "/" + request.getParameter("msdp") + "/" + request.getParameter("ysdp");
                        String SDM = request.getParameter("dsmp") + "/" + request.getParameter("msmp") + "/" + request.getParameter("ysmp");
                        String SDTR = request.getParameter("dstrp") + "/" + request.getParameter("mstrp") + "/" + request.getParameter("ystrp");

                        String EDPP = request.getParameter("depp") + "/" + request.getParameter("mepp") + "/" + request.getParameter("yepp");
                        String EDR = request.getParameter("derp") + "/" + request.getParameter("merp") + "/" + request.getParameter("yerp");
                        String EDS = request.getParameter("desp") + "/" + request.getParameter("mesp") + "/" + request.getParameter("yesp");
                        String EDI = request.getParameter("deip") + "/" + request.getParameter("meip") + "/" + request.getParameter("yeip");
                        String EDT = request.getParameter("detp") + "/" + request.getParameter("metp") + "/" + request.getParameter("yetp");
                        String EDD = request.getParameter("dedp") + "/" + request.getParameter("medp") + "/" + request.getParameter("yedp");
                        String EDM = request.getParameter("demp") + "/" + request.getParameter("memp") + "/" + request.getParameter("yemp");
                        String EDTR = request.getParameter("detrp") + "/" + request.getParameter("metrp") + "/" + request.getParameter("yetrp");

                        for (int i = 1; i <= projectactivityLocalServiceUtil.getprojectactivitiesCount(); i++) {
                            subprojectactivityplan subplan = subprojectactivityplanLocalServiceUtil.createsubprojectactivityplan(0);
                            subplan.setActivityType(Integer.toString(i));
                            subplan.setSubprojectId(request.getParameter("subproject_id"));
                            subplan.setDuration("ccccc");
                            if (i == 1) {
                                subplan.setStart_date(SDPP);
                                subplan.setEnd_date(EDPP);
                            } else if (i == 2) {
                                subplan.setStart_date(SDR);
                                subplan.setEnd_date(EDR);
                            } else if (i == 3) {
                                subplan.setStart_date(SDS);
                                subplan.setEnd_date(EDS);
                            } else if (i == 4) {
                                subplan.setStart_date(SDI);
                                subplan.setEnd_date(EDI);
                            } else if (i == 5) {
                                subplan.setStart_date(SDT);
                                subplan.setEnd_date(EDT);
                            } else if (i == 6) {
                                subplan.setStart_date(SDD);
                                subplan.setEnd_date(EDD);
                            } else if (i == 7) {
                                subplan.setStart_date(SDM);
                                subplan.setEnd_date(EDM);
                            } else {
                                subplan.setStart_date(SDTR);
                                subplan.setEnd_date(EDTR);
                            }
                            subprojectactivityplanLocalServiceUtil.addsubprojectactivityplan(subplan);

                        }
                    } catch (Exception io) {
                        request.setAttribute("msg", "Trail Insertion Errrrrrooorrrrr");
                    }


                    try { String promanid="", prod="";
                        for (int z = 0; z < customerprojectLocalServiceUtil.getcustomerprojectsCount(); z++) {
                            if (prrrrrid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId())) {
                                request.setAttribute("prodispid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId());
                                request.setAttribute("prodispname", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName());
                                prod= customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getUserIId();
                                orggid = customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getOrgid();
                               promanid= customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectManagerId();
                                request.setAttribute("prodispStartDate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getStartDate());
                                request.setAttribute("prodispEnddate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getEndDate());
                                break;
                            }
                        }
                         for(int i=0;i<user_LocalServiceUtil.getuser_sCount();i++){
                         if(Integer.parseInt(promanid)==(user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getUserId()))
                         {
                                 request.setAttribute("prodispmanid",user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getScreenName());
                         }
                          if(Integer.parseInt(prod)==(user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getUserId()))
                         {       request.setAttribute("prodispuserid",user_LocalServiceUtil.getuser_s(0,user_LocalServiceUtil.getuser_sCount()).get(i).getScreenName());

                         }
                    }
                        for (int s = 0; s < customerLocalServiceUtil.getcustomersCount(); s++) {
                            if (orggid==customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getId()) {
                                request.setAttribute("prodisporgname", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getOrganName());
                                request.setAttribute("prodispsector", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getSector());
                                request.setAttribute("prodisporgemail", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getEmail());
                                request.setAttribute("prodisporgftele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFixedtele());
                                request.setAttribute("prodisporgmtele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getMobiletele());
                                request.setAttribute("prodisporgfax", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFax());

                                 request.setAttribute("prodisporgregion", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getRegion());
                                 request.setAttribute("prodisporgzone", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getZone());
                                 request.setAttribute("prodisporgworeda", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getWoreda());
                                 request.setAttribute("prodisporgkebele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getKebele());
                                 request.setAttribute("prodisporghouseno", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getHouseno());
                                break;
                            }
                        }
                        for (int z = 0; z < subprojectLocalServiceUtil.getsubprojectsCount(); z++) {
                            if (prrrrrid.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId())) {
                                request.setAttribute("subproid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id());
                                request.setAttribute("subproname", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_name());
                                request.setAttribute("subprosd", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getStart_date());
                                request.setAttribute("subproed", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getEnd_date());
                                request.setAttribute("proid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getProjectId());

                                break;
                            }
                        }
                    } catch (Exception io) {
                        request.setAttribute("msg", "Trail Fetching Errrrrrooorrrrr");
                    }
                    request.setAttribute("page", "/WEB-INF/jsp/display.jsp");
  nams=1;
                }
            }
 else if (request.getParameter("proact") != null && request.getParameter("proact").equalsIgnoreCase("proact")) {
                String prrrrrid = request.getParameter("proactid");
                int orgidd =0;
                try {


                    request.setAttribute("msg", "I'm getin");
                    String SDPP = request.getParameter("dspp") + "/" + request.getParameter("mspp") + "/" + request.getParameter("yspp");
                    String SDR = request.getParameter("dsrp") + "/" + request.getParameter("msrp") + "/" + request.getParameter("ysrp");
                    String SDS = request.getParameter("dssp") + "/" + request.getParameter("mssp") + "/" + request.getParameter("yssp");
                    String SDI = request.getParameter("dsip") + "/" + request.getParameter("msip") + "/" + request.getParameter("ysip");
                    String SDT = request.getParameter("dstp") + "/" + request.getParameter("mstp") + "/" + request.getParameter("ystp");
                    String SDD = request.getParameter("dsdp") + "/" + request.getParameter("msdp") + "/" + request.getParameter("ysdp");
                    String SDM = request.getParameter("dsmp") + "/" + request.getParameter("msmp") + "/" + request.getParameter("ysmp");
                    String SDTR = request.getParameter("dstrp") + "/" + request.getParameter("mstrp") + "/" + request.getParameter("ystrp");

                    String EDPP = request.getParameter("depp") + "/" + request.getParameter("mepp") + "/" + request.getParameter("yepp");
                    String EDR = request.getParameter("derp") + "/" + request.getParameter("merp") + "/" + request.getParameter("yerp");
                    String EDS = request.getParameter("desp") + "/" + request.getParameter("mesp") + "/" + request.getParameter("yesp");
                    String EDI = request.getParameter("deip") + "/" + request.getParameter("meip") + "/" + request.getParameter("yeip");
                    String EDT = request.getParameter("detp") + "/" + request.getParameter("metp") + "/" + request.getParameter("yetp");
                    String EDD = request.getParameter("dedp") + "/" + request.getParameter("medp") + "/" + request.getParameter("yedp");
                    String EDM = request.getParameter("demp") + "/" + request.getParameter("memp") + "/" + request.getParameter("yemp");
                    String EDTR = request.getParameter("detrp") + "/" + request.getParameter("metrp") + "/" + request.getParameter("yetrp");

                    for (int i = 1; i <= projectactivityLocalServiceUtil.getprojectactivitiesCount(); i++) {
                        subprojectactivityplan subplan = subprojectactivityplanLocalServiceUtil.createsubprojectactivityplan(0);
                        subplan.setActivityType(Integer.toString(i));
                        subplan.setSubprojectId(request.getParameter("proid"));
                        subplan.setDuration("ccccc");
                        if (i == 1) {
                            subplan.setStart_date(SDPP);
                            subplan.setEnd_date(EDPP);
                        } else if (i == 2) {
                            subplan.setStart_date(SDR);
                            subplan.setEnd_date(EDR);
                        } else if (i == 3) {
                            subplan.setStart_date(SDS);
                            subplan.setEnd_date(EDS);
                        } else if (i == 4) {
                            subplan.setStart_date(SDI);
                            subplan.setEnd_date(EDI);
                        } else if (i == 5) {
                            subplan.setStart_date(SDT);
                            subplan.setEnd_date(EDT);
                        } else if (i == 6) {
                            subplan.setStart_date(SDD);
                            subplan.setEnd_date(EDD);
                        } else if (i == 7) {
                            subplan.setStart_date(SDM);
                            subplan.setEnd_date(EDM);
                        } else {
                            subplan.setStart_date(SDTR);
                            subplan.setEnd_date(EDTR);
                        }
                        subprojectactivityplanLocalServiceUtil.addsubprojectactivityplan(subplan);
                        request.setAttribute("msg", "Successfully Saved");
                    }
                    for (int z = 0; z < customerprojectLocalServiceUtil.getcustomerprojectsCount(); z++) {
                        if (prrrrrid.equalsIgnoreCase(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId())) {
                            request.setAttribute("prodispid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectId());
                            request.setAttribute("prodispname", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectName());
                            request.setAttribute("prodispuserid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getUserIId());
                            orgidd = customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getOrgid();
                            request.setAttribute("prodispmanid", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getProjectManagerId());
                            request.setAttribute("prodispStartDate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getStartDate());
                            request.setAttribute("prodispEnddate", customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(z).getEndDate());
                            break;
                        }
                    }
                    for (int s = 0; s < customerLocalServiceUtil.getcustomersCount(); s++) {
                        if (orgidd==customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getId()) {
                            request.setAttribute("prodisporgname", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getOrganName());
                            request.setAttribute("prodispsector", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getSector());
                            request.setAttribute("prodisporgemail", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getEmail());
                            request.setAttribute("prodisporgftele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFixedtele());
                            request.setAttribute("prodisporgmtele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getMobiletele());
                            request.setAttribute("prodisporgfax", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getFax());
                            request.setAttribute("prodisporgregion", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getRegion());
                            request.setAttribute("prodisporgwereda", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getWoreda());
                            request.setAttribute("prodisporgzone", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getZone());
                            request.setAttribute("prodisporgkebele", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getKebele());
                            request.setAttribute("prodisporghouseno", customerLocalServiceUtil.getcustomers(0, customerLocalServiceUtil.getcustomersCount()).get(s).getHouseno());

                            break;
                        }
                    }

                    request.setAttribute("page", "/WEB-INF/jsp/display.jsp");

                } catch (Exception io) {
                    request.setAttribute("msg", io.getMessage());
                }

            } /*else{
            request.setAttribute("mg", "test");
           request.setAttribute("page", "/WEB-INF/jsp/display.jsp;
            }*/


        }


    }

    public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {

    System.out.println(request.getAttribute("page"));

        String callPage = "/WEB-INF/jsp/INSAOnlineCustomerService_view.jsp";
        if (request.getAttribute("page") != null) {
            callPage = request.getAttribute("page").toString();
        }
        response.setContentType("text/html");
        PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }

    public void doEdit(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        String callPage = "/WEB-INF/jsp/INSAOnlineCustomerService_edit.jsp";
        if (request.getAttribute("page") != null) {
            callPage = request.getAttribute("page").toString();
        }
        response.setContentType("text/html");
        PortletRequestDispatcher dispatcher =
                getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
}