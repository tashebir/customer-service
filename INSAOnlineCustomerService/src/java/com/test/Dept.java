package com.test;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.example.service.service.detailinfoLocalServiceUtil;
import com.example.service.model.detailinfo;

 // for upload
import com.liferay.portal.kernel.upload.UploadPortletRequest; // for the action requestes
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

// for delete

import com.example.service.service.*;
import com.example.service.model.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
//import com.liferay.portal.kernel.util.File;
import com.liferay.portal.util.PortalUtil;
import java.io.*;

import java.io.FileOutputStream;

/**
 * Dept Portlet Class
 */
public class Dept extends GenericPortlet {

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {
        
         if(response.getPortletMode().toString().equalsIgnoreCase("edit"))
         {
               UploadPortletRequest uploadRequest=PortalUtil.getUploadPortletRequest(request);
             if (uploadRequest.getParameter("storedd") != null)
             {
             try
             {

                              File hfile = new File(uploadRequest.getFile("uplfile").getPath());
                                    FileInputStream in = new FileInputStream(hfile.getAbsoluteFile());
                                    byte dataBytes[] = new byte[Integer.parseInt(Long.toString(hfile.length()))];
                                    int byteRead = 0;
                                    int totalBytesRead = 0;
                                    while (totalBytesRead < Integer.parseInt(Long.toString(hfile.length()))) {
                                    byteRead = in.read(dataBytes, totalBytesRead, Integer.parseInt(Long.toString(hfile.length())));
                                    totalBytesRead += byteRead;
                                    }
                                    String fty = uploadRequest.getParameter("upf").toString();
                                    File file1 = new File("../webapps/imagefile");
                                    if (file1.exists()) {
                                    } else {
                                    file1.mkdir();
                                    }
                                    File fileu = new File("../webapps/imagefile/" + uploadRequest.getParameter("ti").toString() +"." +fty);
                                   
                                    String pp= "/imagefile/"+uploadRequest.getParameter("ti").toString() +"." +fty;
                                    if (!fileu.exists()) {
                                        FileOutputStream fileOut = new FileOutputStream("..\\webapps\\imagefile\\" + uploadRequest.getParameter("ti").toString() +"."+ fty);
                                        fileOut.write(dataBytes);
                                        fileOut.flush();
                                        fileOut.close();
                                        }
                                    else
                                    {
                                            request.setAttribute("msg", "give different name");
                                    }     
                uploadRequest.setAttribute("msg", "try until u die");
                     detailinfo detinfo=detailinfoLocalServiceUtil.createdetailinfo(0);
                     detinfo.setTitle(uploadRequest.getParameter("ti"));
                     detinfo.setDname(uploadRequest.getParameter("deptname"));
                     detinfo.setDescirption(uploadRequest.getParameter("desc"));
                     detinfo.setImagepath(pp);
                     detailinfoLocalServiceUtil.adddetailinfo(detinfo);
             uploadRequest.setAttribute("msg","Successfully Saved");
             }
             catch(Exception io)
             {
             uploadRequest.setAttribute("msg", "reeeeee");
             }           }
         
        
         }
         else
         {
               if (request.getParameter("nextimage") != null)
             try
             {
                
                    for(int i=0;i<detailinfoLocalServiceUtil.getdetailinfosCount();i++)
                    {
                        if(request.getParameter("changeimage").equalsIgnoreCase(detailinfoLocalServiceUtil.getdetailinfos(0,detailinfoLocalServiceUtil.getdetailinfosCount()).get(i).getImagepath()))
                        {
                        }
                        else
                        {request.setAttribute("imagepath", detailinfoLocalServiceUtil.getdetailinfos(0,detailinfoLocalServiceUtil.getdetailinfosCount()).get(i).getImagepath());

                        }
                       
                    }
                 
             }
             catch(Exception io)
             {
                 
             }
         }


    }
 public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher("/WEB-INF/jsp/Dept_view.jsp");
        dispatcher.include(request, response);
    }
    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
            response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher("/WEB-INF/jsp/Dept_edit.jsp");
        dispatcher.include(request, response);
    }
}