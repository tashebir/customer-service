package com.test;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.example.service.service.*;
import com.example.service.model.*;
import com.example.service.service.persistence.customercomplainUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
/**
 * customerComplain Portlet Class
 */
public class customerComplain extends GenericPortlet {

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {


        if(response.getPortletMode().toString().equalsIgnoreCase("view"))
        {
            if(request.getParameter("solRecomplain")!=null &&request.getParameter("solRecomplain").toString().equalsIgnoreCase("solRecomplain") )
            {
                   request.setAttribute("page", "/WEB-INF/jsp/addComplain.jsp");
            }
            else if(request.getParameter("compppppp")!=null&&request.getParameter("compppppp").toString().equalsIgnoreCase("compppppp"))
            {
                        request.setAttribute("page", "/WEB-INF/jsp/CustomerComplain_edit.jsp");
            }
            else if(request.getParameter("addrecomp")!=null&&request.getParameter("addrecomp").toString().equalsIgnoreCase("addrecomp"))
            {
                try
                {
                    Date now = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
                    String dd = ft.format(now);
                    int comdetid=0;

                    customercomplain custco=customercomplainLocalServiceUtil.createcustomercomplain(0);
          Validation val = new Validation();
		  if( val.validateUser(request.getParameter("compsubject")) &&  val.validateUser(request.getParameter("compdetail"))){
      
      custco.setSubprojectId(request.getParameter("subid"));
                    custco.setComplainSubject(request.getParameter("compsubject"));
                    custco.setComplainDetail(request.getParameter("compdetail"));
                    custco.setStatus("new");
					custco.setComplainDate(dd);
                    customercomplainLocalServiceUtil.addcustomercomplain(custco);

                    customercomplainUtil.clearCache();
                    for(int z=0;z<customercomplainLocalServiceUtil.getcustomercomplainsCount();z++)
                    {
                        if((request.getParameter("compsubject").toString().equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainSubject()))&&(request.getParameter("subid").toString().equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getSubprojectId())))
                        {
                            comdetid=customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getId();
                            
                        }
                    }
                    complainReport comprep=complainReportLocalServiceUtil.createcomplainReport(0);
                    comprep.setComplainId(String.valueOf(comdetid));
                    comprep.setComplainType("new");
                    comprep.setRemark(dd);
                    complainReportLocalServiceUtil.addcomplainReport(comprep);

                    request.setAttribute("msg","Successful");
 } 
          else
 {
					 if( (!val.validateUser(request.getParameter("compsubject"))) &&  val.validateUser(request.getParameter("compdetail")))
					{
					 request.setAttribute("mg", "Please Write Complain On(subjcet) ");
					}
					else if( val.validateUser(request.getParameter("compsubject")) && (!val.validateUser(request.getParameter("compdetail"))))
					{
					request.setAttribute("mg", "Please write What are the tasks not done at all according to your interest?");
    
					}
					else if( (!val.validateUser(request.getParameter("compsubject"))) && (!val.validateUser(request.getParameter("compdetail"))))
					{
					request.setAttribute("mg", "Please write atleast Complain On(subjcet) and What are the tasks not done at all according to your interest?");
    
					}
               

                }
                }
                catch(Exception io)
               {
                    request.setAttribute("msg",io);
               }
            }
         }
        else
        {
            customercomplainUtil.clearCache();

            if((request.getParameter("report")!=null) && request.getParameter("report").trim().equalsIgnoreCase("report"))
            {
                request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");
            }


            else if((request.getParameter("catagoryText")!=null) && request.getParameter("catagoryText").trim().equalsIgnoreCase("catagoryDate"))
            {

                request.setAttribute("msg","Date Successful "+ request.getParameter("selecteddate") +request.getParameter("selectedcomptype") );
                request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");
            }
            else if((request.getParameter("catagoryText")!=null) && request.getParameter("catagoryText").trim().equalsIgnoreCase("catagoryPromanager"))
            {
                int usId=0;String ProId="",subidd="", compSub="",comptype=request.getParameter("comp");
                    try
                    {
                                  String[] compiid = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsub = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compass = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compstat = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsubpro = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compdate = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] vieew = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];



                            for( int x=0;x<user_LocalServiceUtil.getuser_sCount();x++)
                            {
                                 if(request.getParameter("selectedProman").equalsIgnoreCase(user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(x).getScreenName()))
                                 {
                                     usId=user_LocalServiceUtil.getuser_s(0, user_LocalServiceUtil.getuser_sCount()).get(x).getUserId();
                                     break;
                                 }
                            }
                            for(int i=0;i<customerprojectLocalServiceUtil.getcustomerprojectsCount();i++)
                            {
                                if(usId==Integer.valueOf(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectManagerId()))
                                {
                                   ProId=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(i).getProjectId();
                                   for(int a=0;a<subprojectLocalServiceUtil.getsubprojectsCount();a++)
                                   {
                                       if(ProId.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getProjectId()))
                                       {
                                           subidd=subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getSubproject_id();
                                           for(int c=0;c<customercomplainLocalServiceUtil.getcustomercomplainsCount();c++)
                                           {
                                               if((subidd.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()))&&(comptype.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus())))
                                               {
                                                    compSub=customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject();
                                                    compiid[c]="<tr><td id='ff'><input type='checkbox' name='ss" + c + "' value=\""+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId() +"\"></td><td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId()+"</td>";
                                                    compsub[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject()+"</td>";
                                                    compass[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getAssignedTo()+"</td>";
                                                    compstat[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus()+"</td>";
                                                    compsubpro[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()+"</td>";
                                                    compdate[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainDate()+"</td>";
                                                    vieew[c]=String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId());
                                                    
                                               }
                                               
                                           }
                                           

                                       }
                                   }
                                   break;
                                }
                            }
                                         request.setAttribute("res0", compiid);
                                         request.setAttribute("res1", compsub);
                                         request.setAttribute("res2", compass);
                                         request.setAttribute("res3", compstat);
                                         request.setAttribute("res4", compsubpro);
                                         request.setAttribute("res5", compdate);
                                         request.setAttribute("res6",vieew);

                                         request.setAttribute("msg","Manager Successful"+usId+ProId+subidd+" MA" +comptype);
                                         request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");


                    }
                    catch(Exception io)
                    {
                            request.setAttribute("msg", io);
                    }

                
            }
            else if((request.getParameter("catagoryText")!=null) && request.getParameter("catagoryText").trim().equalsIgnoreCase("catagoryProtitle"))
            {


                String ProId=request.getParameter("projectIId"),subidd="", compSub="",comptype=request.getParameter("comp");
                    try
                    {
                                  String[] compiid = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsub = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compass = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compstat = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsubpro = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compdate = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] vieew = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];



                            
                          
                                
                                   for(int a=0;a<subprojectLocalServiceUtil.getsubprojectsCount();a++)
                                   {
                                       if(ProId.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getProjectId()))
                                       {
                                           subidd=subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getSubproject_id();
                                           for(int c=0;c<customercomplainLocalServiceUtil.getcustomercomplainsCount();c++)
                                           {
                                               if((subidd.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()))&&(comptype.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus())))
                                               {
                                                    compSub=customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject();
                                                    compiid[c]="<tr><td id='ff'><input type='checkbox' name='ss" + c + "' value=\""+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId() +"\"></td><td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId()+"</td>";
                                                    compsub[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject()+"</td>";
                                                    compass[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getAssignedTo()+"</td>";
                                                    compstat[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus()+"</td>";
                                                    compsubpro[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()+"</td>";
                                                    compdate[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainDate()+"</td>";
                                                    vieew[c]=String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId());

                                               }

                                           }


                                   
                                }
                            }
                                         request.setAttribute("res0", compiid);
                                         request.setAttribute("res1", compsub);
                                         request.setAttribute("res2", compass);
                                         request.setAttribute("res3", compstat);
                                         request.setAttribute("res4", compsubpro);
                                         request.setAttribute("res5", compdate);
                                         request.setAttribute("res6",vieew);

                                         request.setAttribute("msg","Project Title Successful"+ProId+subidd+" MA" +comptype);
                                         request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");


                    }
                    catch(Exception io)
                    {
                            request.setAttribute("msg", io);
                    }


               
            }
            else if((request.getParameter("catagoryText")!=null) && request.getParameter("catagoryText").trim().equalsIgnoreCase("catagoryOrganization"))
            {
                 String ProId="",subidd="", orgname=request.getParameter("orgname"),compSub="",comptype=request.getParameter("comp");
                    try
                    {
                                  String[] compiid = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsub = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compass = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compstat = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsubpro = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compdate = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] vieew = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];



                                  for(int x=0;x<customerLocalServiceUtil.getcustomersCount();x++)
                                  {
                                      if(orgname.equalsIgnoreCase(String.valueOf(customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(x).getOrgid())))
                                      {
                                          ProId=customerprojectLocalServiceUtil.getcustomerprojects(0, customerprojectLocalServiceUtil.getcustomerprojectsCount()).get(x).getProjectId();
                                          break;

                                      }
                                  }



                                   for(int a=0;a<subprojectLocalServiceUtil.getsubprojectsCount();a++)
                                   {
                                       if(ProId.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getProjectId()))
                                       {
                                           subidd=subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(a).getSubproject_id();
                                           for(int c=0;c<customercomplainLocalServiceUtil.getcustomercomplainsCount();c++)
                                           {
                                               if((subidd.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()))&&(comptype.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus())))
                                               {
                                                    compSub=customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject();
                                                    compiid[c]="<tr><td id='ff'><input type='checkbox' name='ss" + c + "' value=\""+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId() +"\"></td><td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId()+"</td>";
                                                    compsub[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainSubject()+"</td>";
                                                    compass[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getAssignedTo()+"</td>";
                                                    compstat[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getStatus()+"</td>";
                                                    compsubpro[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getSubprojectId()+"</td>";
                                                    compdate[c]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getComplainDate()+"</td>";
                                                    vieew[c]=String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(c).getId());

                                               }

                                           }



                                }
                            }
                                         request.setAttribute("res0", compiid);
                                         request.setAttribute("res1", compsub);
                                         request.setAttribute("res2", compass);
                                         request.setAttribute("res3", compstat);
                                         request.setAttribute("res4", compsubpro);
                                         request.setAttribute("res5", compdate);
                                         request.setAttribute("res6",vieew);

                                          request.setAttribute("msg","Organization Successful" + request.getParameter("selectedorgname")+request.getParameter("selectedcomptype"));
                                          request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");


                    }
                    catch(Exception io)
                    {
                            request.setAttribute("msg", io);
                    }



              
            }
            else if((request.getParameter("catagoryText")!=null) && request.getParameter("catagoryText").trim().equalsIgnoreCase("catagoryAll"))
            {
                request.setAttribute("msg","All Successful" + request.getParameter("selectedorgname")+request.getParameter("selectedProtitle"));
                request.setAttribute("page","/WEB-INF/jsp/complainReport.jsp");
            }


            else if((request.getParameter("soso")!=null) && request.getParameter("soso").trim().equalsIgnoreCase("soso"))
            {
                 request.setAttribute("tracompsub", request.getParameter("subcomp"));
                 request.setAttribute("tracompprimid", request.getParameter("pricodetid"));
                 request.setAttribute("tracompdate", request.getParameter("datcomp"));
                 request.setAttribute("tracompsubproid", request.getParameter("subproid"));
                 request.setAttribute("tracompdetail", request.getParameter("compdetail"));
                 request.setAttribute("msg","Successful");
                 request.setAttribute("page","/WEB-INF/jsp/solution.jsp");
            }
            else if((request.getParameter("transfer")!=null)&& request.getParameter("transfer").trim().equalsIgnoreCase("transfer"))
             {
                 try
                 {
                        int codet= Integer.parseInt(request.getParameter("pricodetid"));
                        customercomplain custcomp =customercomplainLocalServiceUtil.createcustomercomplain(codet);
                        custcomp.setComplainSubject(request.getParameter("subcomp"));
                        custcomp.setComplainDate(request.getParameter("datcomp"));
                        custcomp.setComplainDetail(request.getParameter("compdetail"));
                        custcomp.setStatus("transfered");
                        custcomp.setSubprojectId(request.getParameter("subproid"));
                        customercomplainLocalServiceUtil.updatecustomercomplain(custcomp);
                        request.setAttribute("page","/WEB-INF/jsp/CustomerComplain_edit.jsp");
                 }
                 catch(Exception io)
                 {
                      request.setAttribute("msg","not transferd");
                 }

             }
            else if(request.getParameter("assigned")!=null&&request.getParameter("assigned").trim().equalsIgnoreCase("assigned"))
            {
                 int x=0;
                 String assig=request.getParameter("assigned");
                
                try
                 {
                            for(int k=0; k<customercomplainLocalServiceUtil.getcustomercomplainsCount();k++) 
                            {
                                        x= Integer.parseInt(request.getParameter("cc"+k));
                                     if (request.getParameter("cc" + k) != null && request.getParameter("cc" + k).equalsIgnoreCase(String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(k).getId())))
                                     {
                                                 
                                          customercomplain custu=customercomplainLocalServiceUtil.createcustomercomplain(x);
                                          custu.setStatus("assigned");
                                          custu.setAssignedTo(assig);
                                          customercomplainLocalServiceUtil.updatecustomercomplain(custu);
                                          break;       
                                     }
                             }
                             request.setAttribute("msg"," successfully assigned");
                             request.setAttribute("page","/WEB-INF/jsp/CustomerComplain_edit.jsp");

                 }
                 catch(Exception io)
                 {
                     
                 }
             }
             else if(request.getParameter("repply")!=null && request.getParameter("repply").trim().equalsIgnoreCase("repply"))
            {
                //int x=1;

                int x=1;

                try
                {
                           int id=Integer.parseInt(request.getParameter("comppriid"));

                         
                                        Date now = new Date();
                                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
                                        String dd = ft.format(now);
                                        complainSolution soln = complainSolutionLocalServiceUtil.createcomplainSolution(0);
                                        soln.setSolutionSubject(request.getParameter("solnsubject"));
                                        soln.setSubprojectId(request.getParameter("solnsubproid"));
                                        soln.setSolutionDetail(request.getParameter("solndetail"));
                                        soln.setComplainId(request.getParameter("comppriid"));
                                        soln.setSolutionDate(dd);
                                        complainSolutionLocalServiceUtil.addcomplainSolution(soln);


                                        customercomplain custco=customercomplainLocalServiceUtil.createcustomercomplain(id);
                                        //custco.setComplainId(request.getParameter("compid"));
                                        custco.setComplainDetail(request.getParameter("compdet"));
                                        custco.setComplainDate(request.getParameter("compdate"));
                                        custco.setStatus("answered");
                                        custco.setComplainSubject(request.getParameter("compsubject"));
                                        custco.setSubprojectId(request.getParameter("solnsubproid"));
                                        custco.setLastanswerDate(dd);
                                        customercomplainLocalServiceUtil.updatecustomercomplain(custco);

                                        request.setAttribute("msg","Successfully Saved in replyyyyyy");
                               
                              

                }
                catch (Exception io)
                {
                         request.setAttribute("msg","ERRRRRRROrr in replyyyyyy");
                }

            }
           /*else if(request.getParameter("assigned")!=null)
            {

            }*/
            else if((request.getParameter("compstatus")!=null) &&(request.getParameter("compstatus").trim().equalsIgnoreCase("new")||request.getParameter("compstatus").trim().equalsIgnoreCase("answered")||request.getParameter("compstatus").trim().equalsIgnoreCase("completed")||request.getParameter("compstatus").trim().equalsIgnoreCase("onprocess")||request.getParameter("compstatus").trim().equalsIgnoreCase("transfered")||request.getParameter("compstatus").trim().equalsIgnoreCase("all")||request.getParameter("compstatus").trim().equalsIgnoreCase("recomplain")||request.getParameter("compstatus").trim().equalsIgnoreCase("assigned")) )
            {


                
                 String stat=request.getParameter("compstatus");



                try {

                                  String[] compiid = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsub = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compass = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compstat = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compsubpro = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] compdate = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];
                                  String[] vieew = new String[customercomplainLocalServiceUtil.getcustomercomplainsCount()];

                           
                     if(stat.equalsIgnoreCase("all"))
                     {
                             
                         for(int zz=0;zz<customercomplainLocalServiceUtil.getcustomercomplainsCount();zz++)
                            {
                                     
                                          compiid[zz]="<tr><td id='ff'><input type='checkbox' name='ss" + zz + "' value=\""+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId() +"\"></td><td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId()+"</td>";
                                          compsub[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getComplainSubject()+"</td>";
                                          compass[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getAssignedTo()+"</td>";
                                          compstat[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getStatus()+"</td>";
                                          compsubpro[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getSubprojectId()+"</td>";
                                          compdate[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getComplainDate()+"</td>";
                                          vieew[zz]=String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId());
                                  
                            }
                     }
                     else
                     {
                             for(int zz=0;zz<customercomplainLocalServiceUtil.getcustomercomplainsCount();zz++)
                            {
                                  if (stat.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getStatus()))
                                  {
                                     
                                          compiid[zz]="<tr><td id='ff'><input type='checkbox' name='cc" + zz + "' value=\""+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId() +"\"></td><td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId()+"</td>";
                                          compsub[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getComplainSubject()+"</td>";
                                          compass[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getAssignedTo()+"</td>";
                                          compstat[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getStatus()+"</td>";
                                   
                                          compsubpro[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getSubprojectId()+"</td>";
                                          compdate[zz]="<td id='ff'>"+customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getComplainDate()+"</td>";
                                          vieew[zz]=  String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(zz).getId());
                                  }
                            }
                      }
                                         request.setAttribute("res0", compiid);
                                         request.setAttribute("res1", compsub);
                                         request.setAttribute("res2", compass);
                                         request.setAttribute("res3", compstat);
                                         request.setAttribute("res4", compsubpro);
                                         request.setAttribute("res5", compdate);
                                         request.setAttribute("res6",vieew);

                    }
                    catch(Exception io)
                    {

                    }

            }
             
           
            else if(request.getParameter("soln")!=null && request.getParameter("soln").trim().equalsIgnoreCase("soln"))
            {

                try
                {
                   
                         
                    for (int y = 0; y < customercomplainLocalServiceUtil.getcustomercomplainsCount(); y++)
                    {
                        if (request.getParameter("cc" + y) != null && request.getParameter("cc" + y).equalsIgnoreCase(String.valueOf(customercomplainLocalServiceUtil.getcustomercomplains(0,customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getId())))
                        {
                               //request.setAttribute("msg","r u in addis");
                               request.setAttribute("tracompdate", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getComplainDate());
                               request.setAttribute("tracompsub", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getComplainSubject());
                               request.setAttribute("tracompsubproid", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getSubprojectId());
                               request.setAttribute("tracompprimid", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getId());
                              // request.setAttribute("tracompid", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getComplainId());
                               request.setAttribute("tracompstatus", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getStatus());
                               request.setAttribute("tracompdetail", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(y).getComplainDetail());
                               break;
                        }
                    }
                    request.setAttribute("page","/WEB-INF/jsp/solution.jsp");
                   
            
                }

                catch(Exception io)
                {
                     request.setAttribute("msg","please2");
                }
                //request.setAttribute("page","/WEB-INF/jsp/complainSolutionform.jsp");
            }
            else if(request.getParameter("rep")!=null &&  request.getParameter("rep").trim().equalsIgnoreCase("rep"))
           {
                request.setAttribute("msg","REEEEEEEEEEEEEp");
               try {

              /* Date now = new Date();
               SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
               String dd = ft.format(now);
               complainSolution soln = complainSolutionLocalServiceUtil.createcomplainSolution(0);
               soln.setSolutionSubject(request.getParameter("solnsubject"));
               soln.setSubprojectId(request.getParameter("solnsubproid"));
               soln.setSolutionDetail(request.getParameter("solndetail"));
               soln.setSolutionDate(dd);
               complainSolutionLocalServiceUtil.addcomplainSolution(soln);

               int id=Integer.parseInt(request.getParameter("comppriid"));
               customercomplain custco=customercomplainLocalServiceUtil.createcustomercomplain(id);
               custco.setComplainId(request.getParameter("compid"));
               custco.setComplainDetail(request.getParameter("compdet"));
               custco.setComplainDate(request.getParameter("compdate"));
               custco.setStatus("answered");
               custco.setComplainSubject(request.getParameter("compsubject"));
               custco.setSubprojectId(request.getParameter("solnsubproid"));
               customercomplainLocalServiceUtil.updatecustomercomplain(custco);



               request.setAttribute("msg", "successfully Saved");*/
              }
              catch(Exception io)
              {

              }
            }
            else if(request.getParameter("compdetail")!=null)
            {

                String codetail=request.getParameter("compdetail");
                 int codetailid =Integer.parseInt(codetail);
                 request.setAttribute("codetailid", codetailid);

                  try
                  {
                         for(int z=0;z<customercomplainLocalServiceUtil.getcustomercomplainsCount();z++)
                         {
                                if(codetailid==customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getId())
                                {
                                       // request.setAttribute("complainIId", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainId());
                                         request.setAttribute("complainSub", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainSubject());
                                          request.setAttribute("complainDate", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainDate());
                                           request.setAttribute("complainIStatus", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getStatus());

                                            request.setAttribute("complainDetail", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainDetail());
                                             request.setAttribute("subprojectId", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getSubprojectId());
                                             break;
                                }
                         }

                          request.setAttribute("page", "/WEB-INF/jsp/viewCompdetail.jsp");
                                    /*if(request.getParameter("scal").equalsIgnoreCase("scal"))
                      {
                      request.setAttribute("resultsolution","none");
                      request.setAttribute("resultcomplain","block");
                      }*/
                                   
                    }catch(Exception io)
                    {

                   }


            }
            else
            {
                  request.setAttribute("msg", "not foundddddddddd..........."+request.getParameter("compdetail"));
            }
         /*  else if(request.getParameter("compdetail")!=null)
           {

                try{
                       

                     for (int y=0; y<customercomplainLocalServiceUtil.getcustomercomplainsCount();y++)
                     {

                     }
                     request.setAttribute("selectedCompdate", request.getParameter("subcomp"));
                     request.setAttribute("selectedCompsub",request.getParameter("datcomp"));
                     request.setAttribute("page","/WEB-INF/jsp/solution.jsp");
                     request.setAttribute("msg", "please");
                }catch(Exception io)
                 {

                 }
           }*/
        }
}
 public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
       String callPage="/WEB-INF/jsp/CustomerComplain_view.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
          String callPage="/WEB-INF/jsp/CustomerComplain_edit.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
            response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
}