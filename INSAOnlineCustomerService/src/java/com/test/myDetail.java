package com.test;
import com.example.service.service.*;
import com.example.service.model.*;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import javax.portlet.PortletException;

import javax.portlet.PortletRequestDispatcher;



import com.liferay.portal.kernel.upload.UploadPortletRequest; // for the action requestes
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

// for delete


import java.text.SimpleDateFormat;
import java.util.Date;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
//import com.liferay.portal.kernel.util.File;
import com.liferay.portal.util.PortalUtil;
import java.io.*;

import java.io.FileOutputStream;

/**
 * myDetail Portlet Class
 */
public class myDetail extends GenericPortlet {

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {


        if(response.getPortletMode().toString().equalsIgnoreCase("view"))
        {
            if (request.getParameter("ed") != null)
            {
                  try
                 {
                      String xx=request.getParameter("prosub").trim();
                      int x=Integer.parseInt(xx);
                      user_ uss=user_LocalServiceUtil.createuser_(x);
                      uss.setScreenName(request.getParameter("scrname"));
                      uss.setEmailAddress(request.getParameter("emai"));
                      user_LocalServiceUtil.updateuser_(uss);
                      request.setAttribute("msg", "Successfully Updated");

                 }
                 catch(Exception io)
                 {
                        request.setAttribute("msg", io);
                 }
            }
        }
        else
        {
           
        }

       

    }
    
    public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
          String callPage="/WEB-INF/jsp/myDetail_view.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
            response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher("/WEB-INF/jsp/myDetail_edit.jsp");
        dispatcher.include(request, response);
    }
}