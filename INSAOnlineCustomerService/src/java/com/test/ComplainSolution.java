package com.test;
import com.liferay.portal.SystemException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.IOException;
import javax.portlet.PortletRequestDispatcher;
import com.example.service.service.*;
import com.example.service.model.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
/**
 * ComplainSolution Portlet Class
 */
public class ComplainSolution extends GenericPortlet {

    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {

    
       //request.setAttribute("msg","proId"+request.getParameter("subid")+"Endashaw");
         if(response.getPortletMode().toString().equalsIgnoreCase("view"))
        {
              
             if((request.getParameter("solAccept")!=null)&& request.getParameter("solAccept").equalsIgnoreCase("solAccept"))
             {
                 try
                 {
                        String proooid=request.getParameter("solnproid");
                        for(int i=0;i<customercomplainLocalServiceUtil.getcustomercomplainsCount();i++)
                        {
                             if(Integer.parseInt(proooid)==customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getId())
                             {
                       
                                 int x=   customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getId();
                                 customercomplain cucost = customercomplainLocalServiceUtil.createcustomercomplain(x);
                                 cucost.setComplainDate(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getComplainDate());
                                 cucost.setComplainSubject(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getComplainSubject());
                                 //cuco.setComplainId(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getComplainId());
                                 cucost.setComplainDetail(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getComplainDetail());
                                   request.setAttribute("solutionDetail",customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getComplainDetail());
                                 cucost.setSubprojectId(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(i).getSubprojectId());
                                 cucost.setStatus("completed");
                                
                                 customercomplainLocalServiceUtil.updatecustomercomplain(cucost);

                                 request.setAttribute("msg","Successfullyyyyyy Updated");
                                 request.setAttribute("page", "/WEB-INF/jsp/viewSdetail.jsp");
                                

                                 break;
                             }

                        }

                 }
                 catch(Exception io)
                 {
                  System.out.println("checkkkkk");
                 }
             }    
             else if(request.getParameter("addrecomp")!=null &&  request.getParameter("addrecomp").equalsIgnoreCase("addrecomp"))
             {
                    try
                    {
                            Date now = new Date();
                            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
                            String dd = ft.format(now);
                            customercomplain custco=customercomplainLocalServiceUtil.createcustomercomplain(0);
                            Validation val = new Validation();
                            if( val.validateUser(request.getParameter("compsubject")) &&  val.validateUser(request.getParameter("compdetail"))){
                                  custco.setSubprojectId(request.getParameter("subid"));
                                  custco.setComplainSubject(request.getParameter("compsubject"));
                                  custco.setComplainDetail(request.getParameter("compdetail"));
                                  custco.setStatus("recomplain");
                                  custco.setComplainDate(dd);
                                  custco.setReIdOf(Integer.valueOf(request.getParameter("compidrecomp")));
                            //    custco.setComplainDate();
                                  customercomplainLocalServiceUtil.addcustomercomplain(custco);
                                  request.setAttribute("msg","Successful");
                              }
                              else
                              {
                                    if( (!val.validateUser(request.getParameter("compsubject"))) &&  val.validateUser(request.getParameter("compdetail")))
                                    {
                                        request.setAttribute("mg", "Please Write Complain On(subjcet) ");
                                    }
                                     else if( val.validateUser(request.getParameter("compsubject")) && (!val.validateUser(request.getParameter("compdetail"))))
                                    {
                                            request.setAttribute("mg", "Please write What are the tasks not done at all according to your interest?");
                             		}
                                    else if( (!val.validateUser(request.getParameter("compsubject"))) && (!val.validateUser(request.getParameter("compdetail"))))
                                    {
                                            request.setAttribute("mg", "Please write atleast Complain On(subjcet) and What are the tasks not done at all according to your interest?");
                					}
                               }
                           }
                            catch(Exception io)
                            {
                                    request.setAttribute("msg",io);
                            }

              }
              else if((request.getParameter("solRecomplain")!=null)&& request.getParameter("solRecomplain").equalsIgnoreCase("solRecomplain"))
              {
                    request.setAttribute("compidforRee", request.getParameter("compidForRecomplain"));

                    request.setAttribute("page", "/WEB-INF/jsp/addComplain.jsp");
              }
              else if(request.getParameter("Qn")!=null && request.getParameter("Qn").equalsIgnoreCase("Qn"))
              {
                        request.setAttribute("msg","proId"+request.getParameter("subid")+"u r in Qn");
              }
              else if(request.getParameter("subid")!=null)
              {
                              request.setAttribute("msg","proId"+request.getParameter("subid")+"Displaying");
                              String Selected=request.getParameter("subid");
                          try
                          {
                                      for(int z=0;z<customercomplainLocalServiceUtil.getcustomercomplainsCount();z++)
                                      {
                                            if(Selected.equalsIgnoreCase(customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getSubprojectId()))
                                            {
                                                    request.setAttribute("selectedCompdate", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainDate());
                                                    request.setAttribute("selectedCompsub", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainSubject());
                                                    request.setAttribute("selectedCompdetail", customercomplainLocalServiceUtil.getcustomercomplains(0, customercomplainLocalServiceUtil.getcustomercomplainsCount()).get(z).getComplainDetail());
                                                    break;
                                            }
                                       }
                           }
                           catch(Exception io)
                           {

                           }
                }
                else if(request.getParameter("det")!=null)
                {
                            String viewed=request.getParameter("det");
                            request.setAttribute("compidForRe",request.getParameter("compidforRe"));

                          int viewdId =Integer.parseInt(viewed);

                         try
                         {
                                    for(int z=0;z<complainSolutionLocalServiceUtil.getcomplainSolutionsCount();z++)
                                     {
                                            if(viewdId==complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getSolutionId())
                                            {
                                                        request.setAttribute("solutionId", complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getSolutionId());
                                                        request.setAttribute("solutionSubject", complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getSolutionSubject());
                                                        request.setAttribute("solutionDetail",complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getSolutionDetail());
                                                        request.setAttribute("solutionDate",complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getSolutionDate());
                                                        request.setAttribute("projectId",complainSolutionLocalServiceUtil.getcomplainSolutions(0, complainSolutionLocalServiceUtil.getcomplainSolutionsCount()).get(z).getComplainId());
                                                        break;
                                            }
                                    }
                                    request.setAttribute("page", "/WEB-INF/jsp/viewSdetail.jsp");
                             /*if(request.getParameter("scal").equalsIgnoreCase("scal"))
                  {
                  request.setAttribute("resultsolution","block");
                  request.setAttribute("resultcomplain","none");
                  }*/
                           
                         }
                         catch(Exception io)
                         {

                         }

                 }
                 else
                 {
                            request.setAttribute("msg"," U didn't get it");
                 }
          }
}
    
    public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
         String callPage="/WEB-INF/jsp/ComplainSolution_view.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }

        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
         String callPage="/WEB-INF/jsp/ComplainSolution_edit.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
            response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
}