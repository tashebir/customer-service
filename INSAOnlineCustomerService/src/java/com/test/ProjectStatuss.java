package com.test;
import javax.portlet.GenericPortlet;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.PortletException;
import java.io.*;
import javax.portlet.PortletRequestDispatcher;


 // for upload


import com.liferay.portal.kernel.upload.UploadPortletRequest; // for the action requestes
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

// for delete





import com.example.service.service.*;
import com.example.service.model.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
//import com.liferay.portal.kernel.util.File;
import com.liferay.portal.util.PortalUtil;
import java.io.*;

import java.io.FileOutputStream;

/**
 * ProjectStatuss Portlet Class
 */
public class ProjectStatuss extends GenericPortlet {
public static Integer couuntt=0;
 public static  Integer chh=0;
    public void processAction(ActionRequest request, ActionResponse response) throws PortletException,IOException {
        UploadPortletRequest uploadRequest=PortalUtil.getUploadPortletRequest(request);
        
          if(response.getPortletMode().toString().equalsIgnoreCase("edit"))
        {
               
              
                  if((uploadRequest.getParameter("ps")!=null)&&(uploadRequest.getParameter("ps").trim().equalsIgnoreCase("ps")))
               {
                      Date now = new Date();
                      SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
                      String dd = ft.format(now);
                      try
                     {

                            Validation val = new Validation();
                            if( (val.validateUser(uploadRequest.getParameter("projectId")))&& (val.validateUser(uploadRequest.getParameter("activitytype"))) &&  (val.validateUser(uploadRequest.getParameter("selectedperco"))))
                            {
                                    File hfile = new File(uploadRequest.getFile("uplfile").getPath());
                                    FileInputStream in = new FileInputStream(hfile.getAbsoluteFile());
                                    byte dataBytes[] = new byte[Integer.parseInt(Long.toString(hfile.length()))];
                                    int byteRead = 0;
                                    int totalBytesRead = 0;
                                    while (totalBytesRead < Integer.parseInt(Long.toString(hfile.length()))) {
                                    byteRead = in.read(dataBytes, totalBytesRead, Integer.parseInt(Long.toString(hfile.length())));
                                    totalBytesRead += byteRead;
                                    }
                                    String fty = uploadRequest.getParameter("upf").toString();
                                    File file1 = new File("../webapps/uplofile");
                                    if (file1.exists()) {
                                    } else {
                                    file1.mkdir();
                                    }
                                    File fileu = new File("../webapps/uplofile/" + uploadRequest.getParameter("selectedat").toString() +"." +fty);
                                    String path= "../webapps/uplofile/" + uploadRequest.getParameter("selectedat").toString() +"." +fty;
                                    if (!fileu.exists()) {
                                        FileOutputStream fileOut = new FileOutputStream("..\\webapps\\uplofile\\" + uploadRequest.getParameter("selectedat").toString() +"."+ fty);
                                        fileOut.write(dataBytes);
                                        fileOut.flush();
                                        fileOut.close();
                                        }
                                    else
                                    {
                                            request.setAttribute("msg", "give different name");
                                    }
                                    projectstatus projstat=projectstatusLocalServiceUtil.createprojectstatus(0) ;
                                    projstat.setSubprojectId(uploadRequest.getParameter("projectId"));
                                    projstat.setActivityType(uploadRequest.getParameter("activitytype"));
                                    projstat.setPercentageCompleted(uploadRequest.getParameter("selectedperco"));
                                    projstat.setRemaining(uploadRequest.getParameter("remain"));
                                    projstat.setStatusfilldate(dd);
                                    projstat.setUploaddd(path);
                                    projectstatusLocalServiceUtil.addprojectstatus(projstat);

                                    request.setAttribute("msg", "Successfully Registered!!!");
                              }
                              else
                             {
                                        request.setAttribute("mg", "Please you must select atleast  Sub Project Name,Activity Type and Percentage Completed ");
                                        request.setAttribute("page","/WEB-INF/jsp/ProjectStatuss_edit.jsp");
                             }
                          }
                          catch(Exception io)
                          {
                                  request.setAttribute("msg",io);
                          }
                     }
              else if(uploadRequest.getParameter("projectIId")!=null)
               {



                         uploadRequest.setAttribute("projectNa",uploadRequest.getParameter("projectIId"));
                        uploadRequest.setAttribute("projectName",uploadRequest.getParameter("selectedproname"));



               }
                    
               
              }
              else if(response.getPortletMode().toString().equalsIgnoreCase("view"))
             {
  
                     if(request.getParameter("del")!=null && request.getParameter("del").equalsIgnoreCase("del"))
                    {
                            int x=0;String pathh="";
                            try
                            {
                                 for (int y = 0; y < projectstatusLocalServiceUtil.getprojectstatusesCount(); y++)
                                 {
                                        if (request.getParameter("ss" + y) != null)
                                        {
                                                x=Integer.parseInt(request.getParameter("ss" + y));
                                                pathh=projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getUploaddd();
                                                break;
                                        }
                                 }
                                 File f = new File(pathh);
                                 if(f.exists()){
                                     // Attempt to delete it
                                  f.delete();}

                                projectstatusLocalServiceUtil.deleteprojectstatus(x);
                                request.setAttribute("msg", "Successfully Deleted");
                                request.setAttribute("page", "/WEB-INF/jsp/ProjectStatuss_view.jsp");
                            }
                            catch(Exception io)
                            {
                                     request.setAttribute("msg", io);
                            }

            }
            else if(request.getParameter("editprostat")!=null && request.getParameter("editprostat").equalsIgnoreCase("editprostat"))
            {
                    int activ=0;String subproje="";
                   try {

                              for (int y = 0; y < projectstatusLocalServiceUtil.getprojectstatusesCount(); y++)
                              {
                                     if (request.getParameter("ss" + y) != null)
                                     {
                                                                               
                                            subproje= projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getSubprojectId();
                                            activ= Integer.parseInt(projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getActivityType());
                                            request.setAttribute("percentcompleted", projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getPercentageCompleted());
                                            request.setAttribute("remaining", projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getRemaining());
                                            request.setAttribute("filleddate", projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(y).getStatusfilldate());
                                            break;
                                      }
                              }
                              for(int v=0;v<projectactivityLocalServiceUtil.getprojectactivitiesCount();v++)
                              {
                                  if(activ==projectactivityLocalServiceUtil.getprojectactivities(0, projectactivityLocalServiceUtil.getprojectactivitiesCount()).get(v).getId())
                                  {
                                       request.setAttribute("activity",projectactivityLocalServiceUtil.getprojectactivities(0, projectactivityLocalServiceUtil.getprojectactivitiesCount()).get(v).getActivity());
                                  }
                              }

                             for (int z = 0; z < subprojectLocalServiceUtil.getsubprojectsCount(); z++)
                            {

                                if (subproje.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id()))
                                {
                                    request.setAttribute("Subprojectiid", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_id());
                                    request.setAttribute("Subprojectnamme", subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getSubproject_name());
                                    request.setAttribute("StartDate",  subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getStart_date());
                                     request.setAttribute("EndDate",  subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(z).getEnd_date());
                                    break;
                                 }
                              }

                            request.setAttribute("page", "/WEB-INF/jsp/EditProjectStatus.jsp");
                        
                         }
                         catch(Exception io)
                         {
                             
                         }

            }
            else if(request.getParameter("newprostat")!=null && request.getParameter("newprostat").equalsIgnoreCase("newprostat"))
            {

            }
            
              else if(request.getParameter("subresh")!=null)
             {
                    request.setAttribute("msg",request.getParameter("subresh"));
                try
                {


                 String [] suid=new String[projectstatusLocalServiceUtil.getprojectstatusesCount()];
                 String [] suna=new  String[projectstatusLocalServiceUtil.getprojectstatusesCount()];
                 String [] susd=new  String[projectstatusLocalServiceUtil.getprojectstatusesCount()];
                 String [] sued=new  String[projectstatusLocalServiceUtil.getprojectstatusesCount()];
                 String [] Proid=new  String[projectstatusLocalServiceUtil.getprojectstatusesCount()];
                 String [] vieww=new  String[projectstatusLocalServiceUtil.getprojectstatusesCount()];

                 try
                 {
                 for (int i = 0; i <  projectstatusLocalServiceUtil.getprojectstatusesCount(); i++)
                 {
                       request.setAttribute("vv",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id());
                   if(projectstatusLocalServiceUtil.getprojectstatuses(0,projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getSubprojectId().equalsIgnoreCase(request.getParameter("subresh")))
                       {

                        
                           suid[i]="<tr><td id='ff'><input type='checkbox'name='ss" + i + "' value=\"" + projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getId()+ "\"></td><td id='ff'>" + projectstatusLocalServiceUtil.getprojectstatuses(0,projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getSubprojectId() + "</td>";
                           suna[i]="<td id='ff'>" + projectstatusLocalServiceUtil.getprojectstatuses(0,projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getActivityType() + "</td>";
                           susd[i]="<td id='ff'>" + projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getPercentageCompleted() + "</td>";
                           sued[i]="<td id='ff'>" + projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getRemaining() + "</td>";
                           Proid[i]="<td id='ff'>" + projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getStatusfilldate()+ "</td>";
                          vieww[i]= projectstatusLocalServiceUtil.getprojectstatuses(0,projectstatusLocalServiceUtil.getprojectstatusesCount()).get(i).getSubprojectId();
                          break;
                     }
                }

               try
               {
                

                     request.setAttribute("res0",suid);
                    request.setAttribute("res1",suna);
                     request.setAttribute("res2",susd);
                     request.setAttribute("res3",sued);
                     request.setAttribute("res4",Proid);
                     request.setAttribute("res5",vieww);

               }
                catch(Exception io)
                {request.setAttribute("msg","Fi Error..");
                }
                   
 }
                 catch(Exception io)
                 {request.setAttribute("msg","Assignment Error..");
                 }
             }catch(Exception io)
             {

                  request.setAttribute("msg","Error..");

             }

          }
          else if(request.getParameter("selst1")!=null)
             {


                 String   subid=request.getParameter("selst1");
                 try
                 {
                            for(int i=0;i<subprojectLocalServiceUtil.getsubprojectsCount();i++)
                            {
                               if(subid.equalsIgnoreCase(subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id()))
                               {
                                   request.setAttribute("Subprojectiid",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_id());
                                   request.setAttribute("Subprojectnamme",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getSubproject_name());
                                   request.setAttribute("StartDate",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getStart_date());
                                   request.setAttribute("EndDate",subprojectLocalServiceUtil.getsubprojects(0, subprojectLocalServiceUtil.getsubprojectsCount()).get(i).getEnd_date());
                                   break;
                               }
                             }
                 }
                 catch(Exception io)
                 {
                 }
                 try
                 {
                              for(int j=0;j<projectstatusLocalServiceUtil.getprojectstatusesCount();j++)
                              {

                                   if(subid.equalsIgnoreCase(projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(j).getSubprojectId()))
                                   {
                                        request.setAttribute("PercentCompleted",projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(j).getPercentageCompleted());
                                        request.setAttribute("PercentRemainning",projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(j).getRemaining());
                                        request.setAttribute("ActivityTyppe",projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(j).getActivityType());
                                        request.setAttribute("PostDate",projectstatusLocalServiceUtil.getprojectstatuses(0, projectstatusLocalServiceUtil.getprojectstatusesCount()).get(j).getStatusfilldate());
                                        break;
                                   }


                               }
                                 request.setAttribute("page", "/WEB-INF/jsp/viewStatusDetail.jsp");
                 }
                 catch(Exception io)
                 {

                 }


           }
        
         
          }

    }
    public void doView(RenderRequest request,RenderResponse response) throws PortletException,IOException {
           String callPage="/WEB-INF/jsp/ProjectStatuss_view.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
        response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }

    public void doEdit(RenderRequest request,RenderResponse response) throws PortletException,IOException {
           String callPage="/WEB-INF/jsp/ProjectStatuss_edit.jsp";
        if(request.getAttribute("page")!=null)
        {
            callPage=request.getAttribute("page").toString();
        }
            response.setContentType("text/html");        
        PortletRequestDispatcher dispatcher =
        getPortletContext().getRequestDispatcher(callPage);
        dispatcher.include(request, response);
    }
}